<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Slider;
use Mediapress\Modules\Content\Models\Sitemap;
use Mediapress\Http\Controllers\BaseController;

class HomepageController extends BaseController
{

    public function SitemapDetail(Mediapress $mediapress)
    {
        $mediapress->data['mainslider'] = Slider::find(1)->build();

        $about = Sitemap::with('detail')->where('id', 5)->first();
        $mediapress->data['about'] = [
            'name' => explode(' ', strip_tags($about->detail->name)),
            'desc' => word_limit($about->detail->detail, 21) . '...',
            'img' => $about->f_cover,
            'url' => $about->detail->url
        ];

        return $this->sitemapDetailFunc([]);
    }
}
