<?php

namespace App\Modules\Content\Website1\Http\Controllers\Web;

use Illuminate\Http\Request;
use Mediapress\Foundation\Mediapress;
use Mediapress\Http\Controllers\BaseController;
use Mediapress\Modules\Heraldist\Models\Form;

class ContactController extends BaseController
{
    public function SitemapDetail(Mediapress $mediapress)
    {
        $mediapress->data['form'] = Form::where('slug', 'contactform')->first();
        return $this->sitemapDetailFunc([]);
    }
}
