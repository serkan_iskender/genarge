<?php

use Mediapress\Foundation\Mediapress;
use Mediapress\Modules\Content\Models\Page;

function getMenuLink($menu)
{
    if ($menu['out_link']) {
        return $menu['out_link'];
    } else if ($menu['url']) {
        return $menu['url']->url;
    } else {
        return 'javascript:';
    }
}

function getPageList($pageid)
{
    $pagesRaw =  Cache::remember(cache_key('ny' .$pageid), 24*60*60, function () use ($pageid) {
        return Page::where('sitemap_id', $pageid)->with('detail')->status(1)->orderBy('order')->get();
    });

    $pages = [];
    foreach ($pagesRaw as $key => $page) {
        $pages[$key]['name'] = $page->detail->name;
        $pages[$key]['img'] = $page->f_cover;
        $pages[$key]['desc'] =  word_limit($page->detail->detail, 10) . '...';
    }

    return $pages;
}
