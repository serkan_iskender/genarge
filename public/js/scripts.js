if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
    // Mobil degilse bunlari calistir.
    $(window).scroll(function() {
        $('*[data-aho-effect]').each(function() {
            var _this = $(this);
            var effect = $(_this).attr('data-aho-effect');
            if ($(_this).offset().top <= ($(window).scrollTop() + $(window).height())) {
                setTimeout(function() {
                    $(_this).removeAttr('style');
                    $(_this).removeAttr('data-aho-effect');
                    $(_this).addClass('animated');
                    $(_this).addClass(effect);
                }, 350);
            }
        });
    });
}

$(document).ready(function() {
    $('.mobil-menu-area ul a').click(function() {
        if ($(this).parent().find('ul').length) {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).find('i:first').addClass('fa-angle-down').removeClass('fa-angle-up');
            } else {
                $(this).addClass('active');
                $(this).find('i:first').removeClass('fa-angle-down').addClass('fa-angle-up');
            }
            $(this).parent().find('ul:first').stop().fadeToggle(300);
        }
    });

    $('header .mobile-bar, .mobil-menu-area ul li:last-child').click(function() {
        if ($('.mobil-menu-area').hasClass('active')) {
            $('.mobil-menu-area').removeClass('active');
            $('header .mobile-bar i').addClass('fa-bars').removeClass('fa-times');
        } else {
            $('.mobil-menu-area').addClass('active');
            $('header .mobile-bar i').removeClass('fa-bars').addClass('fa-times');
        }
    });

    if (!/iPhone|iPad|iPod|Android/i.test(navigator.userAgent)) {
        // Mobil degilse bunlari calistir.
        $('*[data-aho-effect]').each(function() {
            $(this).attr('style', 'opacity: 0');
        });
    }
    $('*[data-kesfet]').click(function() {
        var nereye = $(this).attr('data-kesfet');
        $('html, body').animate({
            scrollTop: -$('header').height() + $(nereye).offset().top
        }, 1000);
        return false;
    });
    $('.form-group input[type=text],.form-group input[type=email],.form-group textarea').click(function() {
        $(this).parent().addClass('focus');
    });
});

$('.mobil-menu-area').on('click', 'a', function() {
    if ($(window).width() < 992) {
        if ($(this).next('ul').length > 0) {
            return false;
        }
    }
});