@extends('web.inc.app')

@php
$sitemap = $mediapress->data['sitemap'];
$page = $mediapress->data['page'];
@endphp

@section('content')
    <div class="page" id="page-subpage">
        <h1 class="page-title">{!! $page->detail->name !!}</h1>
        @include('web.inc.breadcrumb',['mediapress'=>$mediapress])
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="texts">
                            <p><b><span>{!! $page->detail->slogan1 !!}</span>{!! $page->detail->summary !!}</b></p>
                            <div class="desc">
                                {!! $page->detail->detail !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="img sticky">
                            <img src="{!! $page->f_ !!}" alt="{!! strip_tags($page->detail->name) !!}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
