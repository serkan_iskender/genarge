@extends('web.inc.app')

@php
$sitemap = $mediapress->data['sitemap'];
$mainSlider = $mediapress->data['mainslider']->slides;
@endphp

@section('content')

    <div class="page" id="page-home">
        <div class="main-slider">
            @foreach ($mainSlider as $slide)
                <div class="container">
                    <div class="texts">
                        <span class="animated fadeInLeft">
                            <p>{!! $slide->text(1) !!}</p>
                        </span>
                        <b class="animated fadeInRight">
                            <p>{!! $slide->text(2) !!}</p>
                        </b>
                    </div>
                    <div class="img">
                        <div class=" animated fadeInUp">
                            <img src="{!! $slide->image()->originalUrl !!} " alt="" class="ccm-svg ccm-image-block img-responsive bID-35">
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="mouse" data-kesfet=".whatwedo"></div>
        </div>

        <div class="whatwedo">
            <div class="container">
                <h1 class="title" data-aho-effect="bounceIn">{!! langPart('main.whatareyoudoing', 'Neler Yapıyoruz?') !!}</h1>
                <div class="boxes row">
                    @foreach (getPageList(7) as $key => $page)
                        <div class="col-12 col-lg-8 {!! $key % 2 == 1 ? 'offset-lg-4' : '' !!}">
                            <div class="box">
                                <div class="img" data-aho-effect="bounceIn">
                                    <img src="{!! $page['img'] !!}" alt="{!! strip_tags($page['name']) !!}">
                                </div>
                                <div class="texts" data-aho-effect="bounceIn">
                                    <h2>{!! $page['name'] !!}</h2>
                                    <p>{!! $page['desc'] !!}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="about-us">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <div class="texts">
                            <b data-aho-effect="bounceIn">
                                <span>{!! $mediapress->data['about']['name'][0] !!}</span>{!! @$mediapress->data['about']['name'][1] !!}
                            </b>
                            <div class="desc" data-aho-effect="bounceIn">
                                <p>{!! $mediapress->data['about']['desc'] !!}</p>

                                <div class="abtn" data-aho-effect="bounceIn">
                                    <a href="{!! $mediapress->data['about']['url'] !!}">{!! langPart('main.more', 'Devamını Gör') !!}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 offset-lg-2">
                        <div class="img" data-aho-effect="bounceIn">
                            <img src="{!! image($mediapress->data['about']['img']) !!}" alt="" class="ccm-svg ccm-image-block img-responsive bID-32">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
