@extends('web.inc.app')

@php
$sitemap = $mediapress->data['sitemap'];
@endphp

@section('content')
    <div class="page" id="page-subpage">
        <h1 class="page-title">{!! $sitemap->detail->name !!}</h1>
        @include('web.inc.breadcrumb',['mediapress'=>$mediapress])
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="texts">
                            <p><b>{!! $sitemap->detail->subtitle !!}</b></p>
                            <div class="desc">
                                {!! $sitemap->detail->detail !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="img">
                            <img src="{!! $sitemap->f_ !!}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
