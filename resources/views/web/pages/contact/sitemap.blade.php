@extends('web.inc.app')

@php
$sitemap = $mediapress->data['sitemap'];
@endphp

@section('content')
    {{-- @include('ContentWeb::default.sitemap_static.sitemap_detail') --}}
    <div class="page" id="page-subpage">
        <h1 class="page-title">{!! $sitemap->detail->name !!}</h1>
        @include('web.inc.breadcrumb',['mediapress'=>$mediapress])

        <div class="content contact-area">
            <div class="container">
                <div class="title text-center"><span></span></div>
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <div class="form">
                            <div class="ftitle">{!! langPart('contact.title', 'İletişim Formu') !!}</div>
                            {!! $mediapress->data['form']->view() !!}
                        </div>
                        <div class="logo">
                            <img src="{!! image(settingSun('logo.desktop_logo')) !!}" alt="{!! strip_tags(langPart('general.sitename')) !!}">
                        </div>
                    </div>
                    <div class="col-12 col-lg-7">
                        <div id="map"></div>
                        <div class="address">
                            {!! $sitemap->detail->detail !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCa9WrG968zb0WtQJucpz2MW18oK3Ctkfc&language=tr"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            var mapOptions = {
                zoom: 13,
                scrollwheel: true,
                center: new google.maps.LatLng(38.680597, 27.313992),
            };
            var mapElement = document.getElementById('map');
            var map = new google.maps.Map(mapElement, mapOptions);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(38.680597, 27.313992),
                map: map,
                icon: '{!! mp_asset('images/marker.svg') !!}',
                animation: google.maps.Animation.DROP
            });
        }
    </script>
@endpush
