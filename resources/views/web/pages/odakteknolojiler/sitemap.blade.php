@extends('web.inc.app')

@php
$sitemap = $mediapress->data['sitemap'];
@endphp

@section('content')
    <div class="page" id="page-subpage">
        <h1 class="page-title">{!! $sitemap->detail->name !!}</h1>
        @include('web.inc.breadcrumb',['mediapress'=>$mediapress])

        <div class="content">
            <div class="container">
                <div class="odak-boxes">
                    <div class="row">
                        @foreach ($sitemap->f_ as $image)
                            <div class="col-6 col-lg-3">
                                <div class="box">
                                    <img src="{!! image($image) !!}" alt="">
                                    <div class="title">{!! $image->detail()['title'] !!}</div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
