@section('footer')
    <footer>
        @if ($mediapress->sitemap->id != 10)
            <div class="help">
                <div class="container">
                    <div class="title">
                        <p data-aho-effect="bounceIn">{!! langPart('footer.slogan1', 'Size Nasıl Yardımcı') !!}</p>
                        <p data-aho-effect="bounceIn"><span>{!! langPart('footer.slogan2', 'olabiliriz?') !!}</span></p>
                    </div>
                    <div class="desc" data-aho-effect="bounceIn">
                        <p>{!! langPart('footer.address', 'Muradiye Mah. Celal Bayar Üniversitesi Kampüsü, Küme Evleri, Teknoloji Geliştirme (Bölgesi İçi) Merkezi No:22 / B206 Yunusemre / MANİSA') !!}</p>
                        <p><a href="tel:{!! strip_tags(str_replace(' ', '', langPart('footer.phone', '0850 811 51 21'))) !!}">{!! langPart('footer.phone', '0850 811 51 21') !!}</a></p>
                        <p><a href="mailto:{!! strip_tags(langPart('footer.email', 'info@genarge.com.tr')) !!}">{!! langPart('footer.email', 'info@genarge.com.tr') !!}</a></p>
                        <div class="abtn">
                            <a href="{!! getUrlBySitemapId(10) !!}">{!! langPart('footer.contact', 'İletişim') !!}</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="foot">
            <div class="logo" data-aho-effect="fadeInUp">
                <a href="/tr"><img src="{!! image(settingSun('logo.desktop_logo')) !!}" alt="{!! strip_tags(langPart('general.sitename')) !!}"></a>
            </div>
            <div class="social" data-aho-effect="fadeInUp">
                @foreach (socialMedia() as $media)
                    <a href="{!! $media['link'] !!}" target="_blank" rel="nofollow"><i class="fa fa-{!! $media['name'] == 'youtube' ? $media['name'] . '-play' : $media['name'] !!}"></i></a>
                @endforeach
            </div>
        </div>
        <div class="copy">
            <a href="https://www.mediaclick.com.tr" target="_blank">Web Tasarım</a>
            <a href="https://www.mediaclick.com.tr" target="_blank">MediaClick</a>
            <div class="desc">{!! langPart('footer.sign', '© 2020 Gen ARGE, Her hakkı saklıdır.') !!}</div>
        </div>
    </footer>
@endsection
