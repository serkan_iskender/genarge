@section('header')
    <header class="header">
        <div class="container">
            <div class="logo">
                <a href="{!! url($mediapress->homePageUrl->url) !!}">
                    <img src="{!! image(settingSun('logo.desktop_logo')) !!}" alt="{!! strip_tags(langPart('general.sitename')) !!}">
                </a>
            </div>
            <div class="menu">
                <ul>
                    @foreach ($mediapress->menu('header-menu') as $menu)
                        <li>
                            <a {!! $loop->last ? 'class="contact-btn"' : '' !!} href="{!! getMenuLink($menu) !!}">{!! $menu['name'] !!}</a>
                            @if (count($menu['children']))
                                <ul>
                                    @foreach ($menu['children'] as $submenu)
                                        <li>
                                            <a href="{!! getMenuLink($submenu) !!}">{!! $submenu['name'] !!}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="mobile-bar">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <div class="mobil-menu-area">
        <ul>
            @foreach ($mediapress->menu('header-menu') as $menu)
                <li>
                    <a {!! $loop->last ? 'class="contact-btn"' : '' !!} href="{!! getMenuLink($menu) !!}">{!! $menu['name'] !!}</a>
                    @if (count($menu['children']))
                        <ul>
                            @foreach ($menu['children'] as $submenu)
                                <li>
                                    <a href="{!! getMenuLink($submenu) !!}">{!! $submenu['name'] !!}</a>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach
            @if (count($mediapress->otherLanguages(1)) > 0)
                <li id="mobilelang">
                    <a class="active" href="javascript:;">{!! strtoupper($mediapress->activeLanguage->code) !!}</a>
                    @foreach ($mediapress->otherLanguages(1) as $otherLang)
                        <a href="{!! url($otherLang['url']->url) !!}">{!! strtoupper($otherLang['language_code']) !!}</a>
                    @endforeach
                </li>
            @endif
        </ul>
    </div>
    @if (count($mediapress->otherLanguages(1)) > 0)
        <ul id="langselect">
            <li><a class="active" href="javascript:;">{!! strtoupper($mediapress->activeLanguage->code) !!}</a></li>
            @foreach ($mediapress->otherLanguages(1) as $otherLang)
                <li><a href="{!! url($otherLang['url']->url) !!}">{!! strtoupper($otherLang['language_code']) !!}</a></li>
            @endforeach
        </ul>
    @endif
@endsection
