@php
$breadcrumb = $mediapress->breadcrumb();
@endphp
<div class="mcbread">
    @foreach ($breadcrumb as $bread)
        <a href="{!! $bread['url'] !!}">
            <span>{!! $bread['name'] !!}</span>
        </a>
    @endforeach
</div>
