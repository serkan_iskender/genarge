
@php
if(!isset($mediapress)){
$mediapress = mediapress();
}

@endphp
@extends('web.inc.app')

@section('content')

    <section>
        <div class="page-content p_b_0">
            <article>
                <div class="container-fluid">
                    <div class="a404 text-center">
                        <h2>Sayfa Bulunamadı</h2>
                        <span>Ulaşmaya çalıştığınız sayfa bulunmadı.</span>
                        <p>Lütfen ulaşmak istediğiniz sayfanın adresini doğru yazdığınızı kontrol edin.</p>
                        <p>Eğer doğru adresi yazdığınıza eminseniz, ulaşmak istediğiniz sayfa silinmiş olabilir.</p>
                        <a href="{!! $mediapress->homePageUrl !!}" class="btns">Ana Sayfaya Dön</a>
                    </div>
                </div>
            </article>
        </div>
    </section>
@endsection