-- --------------------------------------------------------
-- Sunucu:                       localhost
-- Sunucu sürümü:                8.0.23 - MySQL Community Server - GPL
-- Sunucu İşletim Sistemi:       Win64
-- HeidiSQL Sürüm:               11.2.0.6233
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- tablo yapısı dökülüyor genarge.abilities
CREATE TABLE IF NOT EXISTS `abilities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entity_id` int unsigned DEFAULT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `only_owned` tinyint(1) NOT NULL DEFAULT '0',
  `scope` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `abilities_scope_index` (`scope`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.abilities: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `abilities`;
/*!40000 ALTER TABLE `abilities` DISABLE KEYS */;
INSERT INTO `abilities` (`id`, `name`, `title`, `entity_id`, `entity_type`, `only_owned`, `scope`, `created_at`, `updated_at`) VALUES
	(1, '*', 'All Abilities', NULL, '*', 0, NULL, NULL, NULL);
/*!40000 ALTER TABLE `abilities` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.admins
CREATE TABLE IF NOT EXISTS `admins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int unsigned NOT NULL,
  `language_id` int unsigned DEFAULT '760',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google2fa` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google2fa_warning` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.admins: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `admins`;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` (`id`, `role_id`, `language_id`, `first_name`, `last_name`, `username`, `email`, `phone`, `password`, `remember_token`, `api_token`, `google2fa`, `google2fa_warning`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 2, 760, 'Serkan', 'Iskender', 'Serkan Iskender', 'serkan.iskender@mediaclick.com.tr', NULL, '$2y$10$1GxMVmWdwkCnOvuhdBsBsexAzE7Szc9Iez3MJj.nYH4GGrl.W0Z7y', 'VC6ycAsL1No5WT3QlsR9YSXDBiVoj2eahwh7OSClV40rTKxudhJYK0REZaK8', '605078f4a12ef', NULL, '1', '2021-03-16 09:23:00', '2021-03-16 09:23:00', NULL);
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.assigned_roles
CREATE TABLE IF NOT EXISTS `assigned_roles` (
  `role_id` int unsigned NOT NULL,
  `entity_id` int unsigned NOT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scope` int DEFAULT NULL,
  KEY `assigned_roles_entity_index` (`entity_id`,`entity_type`,`scope`),
  KEY `assigned_roles_role_id_index` (`role_id`),
  KEY `assigned_roles_scope_index` (`scope`),
  CONSTRAINT `assigned_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.assigned_roles: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `assigned_roles`;
/*!40000 ALTER TABLE `assigned_roles` DISABLE KEYS */;
INSERT INTO `assigned_roles` (`role_id`, `entity_id`, `entity_type`, `scope`) VALUES
	(2, 1, 'Mediapress\\Modules\\Auth\\Models\\Admin', NULL);
/*!40000 ALTER TABLE `assigned_roles` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int unsigned NOT NULL,
  `category_tag` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `depth` int unsigned DEFAULT NULL,
  `category_id` int unsigned DEFAULT NULL,
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `cdec_1` decimal(20,4) DEFAULT NULL,
  `cdec_2` decimal(20,4) DEFAULT NULL,
  `cdec_3` decimal(20,4) DEFAULT NULL,
  `cdec_4` decimal(20,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allowed_role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_sitemap_id_index` (`sitemap_id`),
  KEY `categories_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.categories: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `categories`;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_criteria
CREATE TABLE IF NOT EXISTS `category_criteria` (
  `category_id` int unsigned NOT NULL,
  `criteria_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_criteria: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_criteria`;
/*!40000 ALTER TABLE `category_criteria` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_criteria` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_details
CREATE TABLE IF NOT EXISTS `category_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  `country_group_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `search_text` text COLLATE utf8mb4_unicode_ci,
  `status` int NOT NULL DEFAULT '3',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `manual_meta` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `category_details_category_id_index` (`category_id`),
  KEY `category_details_language_id_index` (`language_id`),
  KEY `category_details_country_group_id_index` (`country_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_details`;
/*!40000 ALTER TABLE `category_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_detail_extras
CREATE TABLE IF NOT EXISTS `category_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category_detail_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_detail_extras_category_detail_id_index` (`category_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_detail_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_detail_extras`;
/*!40000 ALTER TABLE `category_detail_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_extras
CREATE TABLE IF NOT EXISTS `category_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category_extras_category_id_index` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_extras`;
/*!40000 ALTER TABLE `category_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_page
CREATE TABLE IF NOT EXISTS `category_page` (
  `category_id` int unsigned NOT NULL,
  `page_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_page: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_page`;
/*!40000 ALTER TABLE `category_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_page` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.category_property
CREATE TABLE IF NOT EXISTS `category_property` (
  `category_id` int unsigned NOT NULL,
  `property_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.category_property: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `category_property`;
/*!40000 ALTER TABLE `category_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `category_property` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.comments
CREATE TABLE IF NOT EXISTS `comments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `comment_structure_id` int NOT NULL,
  `parent_id` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '2',
  `model_class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int DEFAULT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` tinyint NOT NULL DEFAULT '2',
  `cvar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ctex` text COLLATE utf8mb4_unicode_ci,
  `cint` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.comments: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `comments`;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.comment_structures
CREATE TABLE IF NOT EXISTS `comment_structures` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `admin_id` int unsigned NOT NULL,
  `status` tinyint NOT NULL DEFAULT '2',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_time` tinyint NOT NULL DEFAULT '2',
  `condition` tinyint NOT NULL DEFAULT '2',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.comment_structures: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `comment_structures`;
/*!40000 ALTER TABLE `comment_structures` DISABLE KEYS */;
/*!40000 ALTER TABLE `comment_structures` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.countries
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `native` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tr` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `en` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.countries: ~248 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `countries`;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`id`, `code`, `native`, `tr`, `en`) VALUES
	(1, 'AD', 'Andorra', 'Andorra', 'Andorra'),
	(2, 'AE', 'United Arab Emirates', 'Birleşik Arap Emirlikleri', 'United Arab Emirates'),
	(3, 'AF', 'Afghanistan', 'Afganistan', 'Afghanistan'),
	(4, 'AG', 'Antigua and Barbuda', 'Antigua ve Barbuda', 'Antigua and Barbuda'),
	(5, 'AI', 'Anguilla', 'Anguilla', 'Anguilla'),
	(6, 'AL', 'Albania', 'Arnavutluk', 'Albania'),
	(7, 'AM', 'Armenia', 'Ermenistan', 'Armenia'),
	(8, 'AN', 'Netherlands Antilles', 'Hollanda Antilleri', 'Netherlands Antilles'),
	(9, 'AO', 'Angola', 'Angola', 'Angola'),
	(10, 'AQ', 'Antarctica', 'Antarktika', 'Antarctica'),
	(11, 'AR', 'Argentina', 'Arjantin', 'Argentina'),
	(12, 'AS', 'American Samoa', 'Amerikan Samoası', 'American Samoa'),
	(13, 'AT', 'Austria', 'Avusturya', 'Austria'),
	(14, 'AU', 'Australia', 'Avustralya', 'Australia'),
	(15, 'AW', 'Aruba', 'Aruba', 'Aruba'),
	(16, 'AX', 'Aland Islands', 'Aland Adaları', 'Aland Islands'),
	(17, 'AZ', 'Azerbaijan', 'Azerbaycan', 'Azerbaijan'),
	(18, 'BA', 'Bosnia and Herzegovina', 'Bosna Hersek', 'Bosnia and Herzegovina'),
	(19, 'BB', 'Barbados', 'Barbados', 'Barbados'),
	(20, 'BD', 'Bangladesh', 'Bangladeş', 'Bangladesh'),
	(21, 'BE', 'Belgium', 'Belçika', 'Belgium'),
	(22, 'BF', 'Burkina Faso', 'Burkina Faso', 'Burkina Faso'),
	(23, 'BG', 'Bulgaria', 'Bulgaristan', 'Bulgaria'),
	(24, 'BH', 'Bahrain', 'Bahreyn', 'Bahrain'),
	(25, 'BI', 'Burundi', 'Burundi', 'Burundi'),
	(26, 'BJ', 'Benin', 'Benin', 'Benin'),
	(27, 'BL', 'Saint Barthelemy', 'Saint Barthelemy', 'Saint Barthelemy'),
	(28, 'BM', 'Bermuda', 'Bermuda', 'Bermuda'),
	(29, 'BN', 'Brunei', 'Brunei', 'Brunei'),
	(30, 'BO', 'Bolivia', 'Bolivya', 'Bolivia'),
	(31, 'BR', 'Brazil', 'Brezilya', 'Brazil'),
	(32, 'BS', 'Bahamas', 'Bahamalar', 'Bahamas'),
	(33, 'BT', 'Bhutan', 'Butan', 'Bhutan'),
	(34, 'BV', 'Bouvet Island', 'Bouvet Adası', 'Bouvet Island'),
	(35, 'BW', 'Botswana', 'Botsvana', 'Botswana'),
	(36, 'BY', 'Belarus', 'Belarus', 'Belarus'),
	(37, 'BZ', 'Belize', 'Belize', 'Belize'),
	(38, 'CA', 'Canada', 'Kanada', 'Canada'),
	(39, 'CC', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands', 'Cocos (Keeling) Islands'),
	(40, 'CD', 'Congo Democratic Republic', 'Kongo Demokratik Cumhuriyeti', 'Congo Democratic Republic'),
	(41, 'CF', 'Central African Republic', 'Orta Afrika Cumhuriyeti', 'Central African Republic'),
	(42, 'CG', 'Congo', 'Kongo', 'Congo'),
	(43, 'CH', 'Switzerland', 'İsviçre', 'Switzerland'),
	(44, 'CI', 'Cote D\'Ivoire', 'Cote D\'Ivoire', 'Cote D\'Ivoire'),
	(45, 'CK', 'Cook Islands', 'Cook Adaları', 'Cook Islands'),
	(46, 'CL', 'Chile', 'Şili', 'Chile'),
	(47, 'CM', 'Cameroon', 'Kamerun', 'Cameroon'),
	(48, 'CN', 'China', 'Çin', 'China'),
	(49, 'CO', 'Colombia', 'Kolombiya', 'Colombia'),
	(50, 'CR', 'Costa Rica', 'Kosta Rika', 'Costa Rica'),
	(51, 'CT', 'North Cyprus Turkish Republic', 'Kuzey Kıbrıs Türk Cumhuriyeti', 'North Cyprus Turkish Republic'),
	(52, 'CU', 'Cuba', 'Küba', 'Cuba'),
	(53, 'CV', 'Cape Verde', 'Cape Verde', 'Cape Verde'),
	(54, 'CX', 'Christmas Island', 'Christmas Island', 'Christmas Island'),
	(55, 'CY', 'Cyprus', 'Kıbrıs', 'Cyprus'),
	(56, 'CZ', 'Czech Republic', 'Çek Cumhuriyeti', 'Czech Republic'),
	(57, 'DE', 'Germany', 'Almanya', 'Germany'),
	(58, 'DJ', 'Djibouti', 'Cibuti', 'Djibouti'),
	(59, 'DK', 'Denmark', 'Danimarka', 'Denmark'),
	(60, 'DM', 'Dominica', 'Dominika', 'Dominica'),
	(61, 'DO', 'Dominican Republic', 'Dominik Cumhuriyeti', 'Dominican Republic'),
	(62, 'DZ', 'Algeria', 'Cezayir', 'Algeria'),
	(63, 'EC', 'Ecuador', 'Ekvador', 'Ecuador'),
	(64, 'EE', 'Estonia', 'Estonya', 'Estonia'),
	(65, 'EG', 'Egypt', 'Mısır', 'Egypt'),
	(66, 'EH', 'West Sahara', 'Batı Sahra', 'West Sahara'),
	(67, 'ER', 'Eritrea', 'Eritre', 'Eritrea'),
	(68, 'ES', 'Spain', 'İspanya', 'Spain'),
	(69, 'ET', 'Ethiopia', 'Etiyopya', 'Ethiopia'),
	(70, 'FI', 'Finland', 'Finlandiya', 'Finland'),
	(71, 'FJ', 'Fiji', 'Fiji', 'Fiji'),
	(72, 'FK', 'Falkland Islands (Malvinas)', 'Falkland Adaları (Malvinas)', 'Falkland Islands (Malvinas)'),
	(73, 'FM', 'Micronesia', 'Mikronezya', 'Micronesia'),
	(74, 'FO', 'Faeroe Islands', 'Faroe Adaları', 'Faeroe Islands'),
	(75, 'FR', 'France', 'Fransa', 'France'),
	(76, 'GA', 'Gabon', 'Gabon', 'Gabon'),
	(77, 'GB', 'United Kingdom', 'Birleşik Krallık', 'United Kingdom'),
	(78, 'GD', 'Grenada', 'Grenada', 'Grenada'),
	(79, 'GE', 'Georgia', 'Gürcistan', 'Georgia'),
	(80, 'GF', 'French Guiana', 'Fransız Guyanası', 'French Guiana'),
	(81, 'GG', 'Guernsey', 'Guernsey', 'Guernsey'),
	(82, 'GH', 'Ghana', 'Gana', 'Ghana'),
	(83, 'GI', 'Gibraltar', 'Gibraltar', 'Gibraltar'),
	(84, 'GL', 'Greenland', 'Grönland', 'Greenland'),
	(85, 'GM', 'Gambia', 'Gambiya', 'Gambia'),
	(86, 'GN', 'Guinea', 'Gine', 'Guinea'),
	(87, 'GP', 'Guadeloupe', 'Guadeloupe', 'Guadeloupe'),
	(88, 'GQ', 'Equatorial Guinea', 'Equatorial Guinea', 'Equatorial Guinea'),
	(89, 'GR', 'Greece', 'Yunanistan', 'Greece'),
	(90, 'GS', 'South Georgia and Sandwich Islands', 'South Georgia ve Sandwich Adaları', 'South Georgia and Sandwich Islands'),
	(91, 'GT', 'Guatemala', 'Guatemala', 'Guatemala'),
	(92, 'GU', 'Guam', 'Guam', 'Guam'),
	(93, 'GW', 'Guinea-Bissau', 'Gine-Bissau', 'Guinea-Bissau'),
	(94, 'GY', 'Guyana', 'Guyana', 'Guyana'),
	(95, 'HK', 'Hong Kong', 'Hong Kong', 'Hong Kong'),
	(96, 'HM', 'Heard Island and McDonald Islands', 'Heard Adası ve McDonald Adaları', 'Heard Island and McDonald Islands'),
	(97, 'HN', 'Honduras', 'Honduras', 'Honduras'),
	(98, 'HR', 'Croatia', 'Hırvatistan', 'Croatia'),
	(99, 'HT', 'Haiti', 'Haiti', 'Haiti'),
	(100, 'HU', 'Hungary', 'Macaristan', 'Hungary'),
	(101, 'ID', 'Indonesia', 'Endonezya', 'Indonesia'),
	(102, 'IE', 'Ireland', 'İrlanda', 'Ireland'),
	(103, 'IL', 'Israel', 'İsrail', 'Israel'),
	(104, 'IM', 'Man Island', 'Man Adası', 'Man Island'),
	(105, 'IN', 'India', 'Hindistan', 'India'),
	(106, 'IO', 'British Indian Ocean Territory', 'Britanya Hint Okyanusu Toprakları', 'British Indian Ocean Territory'),
	(107, 'IQ', 'Iraq', 'Irak', 'Iraq'),
	(108, 'IR', 'Iran Islamic Republic', 'İran', 'Iran'),
	(109, 'IS', 'Iceland', 'İzlanda', 'Iceland'),
	(110, 'IT', 'Italy', 'İtalya', 'Italy'),
	(111, 'JE', 'Jersey', 'Jersey', 'Jersey'),
	(112, 'JM', 'Jamaica', 'Jamaika', 'Jamaica'),
	(113, 'JO', 'Jordan', 'Ürdün', 'Jordan'),
	(114, 'JP', 'Japan', 'Japonya', 'Japan'),
	(115, 'KE', 'Kenya', 'Kenya', 'Kenya'),
	(116, 'KG', 'Kyrgyzstan', 'Kırgızistan', 'Kyrgyzstan'),
	(117, 'KH', 'Cambodia', 'Kamboçya', 'Cambodia'),
	(118, 'KI', 'Kiribati', 'Kiribati', 'Kiribati'),
	(119, 'KM', 'Comoros', 'Comoros', 'Comoros'),
	(120, 'KN', 'Saint Kitts and Nevis', 'Saint Kitts ve Nevis', 'Saint Kitts and Nevis'),
	(121, 'KP', 'North Korea', 'Kuzey Kore', 'North Korea'),
	(122, 'KR', 'South Korea', 'Güney Kore', 'South Korea'),
	(123, 'KW', 'Kuwait', 'Kuveyt', 'Kuwait'),
	(124, 'KY', 'Cayman Islands', 'Cayman Adaları', 'Cayman Islands'),
	(125, 'KZ', 'Kazakhstan', 'Kazakistan', 'Kazakhstan'),
	(126, 'LA', 'Laos', 'Laos', 'Laos'),
	(127, 'LB', 'Lebanon', 'Lübnan', 'Lebanon'),
	(128, 'LC', 'Saint Lucia', 'Saint Lucia', 'Saint Lucia'),
	(129, 'LI', 'Liechtenstein', 'Lihtenştayn', 'Liechtenstein'),
	(130, 'LK', 'Sri Lanka', 'Sri Lanka', 'Sri Lanka'),
	(131, 'LR', 'Liberia', 'Liberya', 'Liberia'),
	(132, 'LS', 'Lesotho Kingdom', 'Lesotho Krallığı', 'Lesotho Kingdom'),
	(133, 'LT', 'Lithuania', 'Litvanya', 'Lithuania'),
	(134, 'LU', 'Luxembourg', 'Lüksemburg', 'Luxembourg'),
	(135, 'LV', 'Latvia', 'Letonya', 'Latvia'),
	(136, 'LY', 'Libya', 'Libya', 'Libya'),
	(137, 'MA', 'Morocco', 'Fas', 'Morocco'),
	(138, 'MC', 'Monaco', 'Monako', 'Monaco'),
	(139, 'MD', 'Moldova', 'Moldova', 'Moldova'),
	(140, 'ME', 'Montenegro', 'Karadağ', 'Montenegro'),
	(141, 'MF', 'Saint Martin', 'Saint Martin', 'Saint Martin'),
	(142, 'MG', 'Madagascar', 'Madagaskar', 'Madagascar'),
	(143, 'MH', 'Marshall Islands', 'Marshall Adaları', 'Marshall Islands'),
	(144, 'MK', 'Macedonia', 'Makedonya', 'Macedonia'),
	(145, 'ML', 'Mali', 'Mali', 'Mali'),
	(146, 'MM', 'Myanmar', 'Myanmar', 'Myanmar'),
	(147, 'MN', 'Mongolia', 'Moğolistan', 'Mongolia'),
	(148, 'MO', 'Macau', 'Mação', 'Macau'),
	(149, 'MP', 'Northern Marianas', 'Kuzey Mariana Adaları', 'Northern Marianas'),
	(150, 'MQ', 'Martinique', 'Martinique', 'Martinique'),
	(151, 'MR', 'Mauritania', 'Moritanya', 'Mauritania'),
	(152, 'MS', 'Montserrat', 'Montserrat', 'Montserrat'),
	(153, 'MT', 'Malta', 'Malta', 'Malta'),
	(154, 'MU', 'Mauritius', 'Mauritius', 'Mauritius'),
	(155, 'MV', 'Maldives', 'Maldivler', 'Maldives'),
	(156, 'MW', 'Malawi', 'Malavi', 'Malawi'),
	(157, 'MX', 'Mexico', 'Meksika', 'Mexico'),
	(158, 'MY', 'Malaysia', 'Malezya', 'Malaysia'),
	(159, 'MZ', 'Mozambique', 'Mozambik', 'Mozambique'),
	(160, 'NA', 'Namibia', 'Namibya', 'Namibia'),
	(161, 'NC', 'New Caledonia', 'Yeni Kaledonya', 'New Caledonia'),
	(162, 'NE', 'Niger', 'Nijer', 'Niger'),
	(163, 'NF', 'Norfolk Island', 'Norfolk Adası', 'Norfolk Island'),
	(164, 'NG', 'Nigeria', 'Nijerya', 'Nigeria'),
	(165, 'NI', 'Nicaragua', 'Nikaragua', 'Nicaragua'),
	(166, 'NL', 'Netherlands', 'Hollanda', 'Netherlands'),
	(167, 'NO', 'Norway', 'Norveç', 'Norway'),
	(168, 'NP', 'Nepal', 'Nepal', 'Nepal'),
	(169, 'NR', 'Nauru', 'Nauru', 'Nauru'),
	(170, 'NU', 'Niue', 'Niue', 'Niue'),
	(171, 'NZ', 'New Zealand', 'Yeni Zelanda', 'New Zealand'),
	(172, 'OM', 'Oman', 'Umman', 'Oman'),
	(173, 'PA', 'Panama', 'Panama', 'Panama'),
	(174, 'PE', 'Peru', 'Peru', 'Peru'),
	(175, 'PF', 'French Polynesia', 'Fransız Polinezyası', 'French Polynesia'),
	(176, 'PG', 'Papua New Guinea', 'Papua Yeni Gine', 'Papua New Guinea'),
	(177, 'PH', 'Philippines', 'Filipinler', 'Philippines'),
	(178, 'PK', 'Pakistan', 'Pakistan', 'Pakistan'),
	(179, 'PL', 'Poland', 'Polonya', 'Poland'),
	(180, 'PM', 'Saint Pierre and Miquelon', 'Saint Pierre ve Miquelon', 'Saint Pierre and Miquelon'),
	(181, 'PN', 'Pitcairn Islands', 'Pitcairn Adaları', 'Pitcairn Islands'),
	(182, 'PR', 'Puerto Rico', 'Porto Riko', 'Puerto Rico'),
	(183, 'PS', 'Palestine', 'Filistin', 'Palestine'),
	(184, 'PT', 'Portugal', 'Portekiz', 'Portugal'),
	(185, 'PW', 'Palau', 'Palau', 'Palau'),
	(186, 'PY', 'Paraguay', 'Paraguay', 'Paraguay'),
	(187, 'QA', 'Qatar', 'Katar', 'Qatar'),
	(188, 'RE', 'Reunion', 'Reunion', 'Reunion'),
	(189, 'RO', 'Romania', 'Romanya', 'Romania'),
	(190, 'RS', 'Serbia', 'Sırbistan', 'Serbia'),
	(191, 'RU', 'Russia', 'Rusya', 'Russia'),
	(192, 'RW', 'Ruanda', 'Ruanda', 'Ruanda'),
	(193, 'SA', 'Saudi Arabia', 'Suudi Arabistan', 'Saudi Arabia'),
	(194, 'SB', 'Solomon Islands', 'Solomon Adaları', 'Solomon Islands'),
	(195, 'SC', 'Seychelles', 'Seyşeller', 'Seychelles'),
	(196, 'SD', 'Sudan', 'Sudan', 'Sudan'),
	(197, 'SE', 'Sweden', 'İsveç', 'Sweden'),
	(198, 'SG', 'Singapore', 'Singapur', 'Singapore'),
	(199, 'SH', 'Saint Helena', 'Saint Helena', 'Saint Helena'),
	(200, 'SI', 'Slovenia', 'Slovenya', 'Slovenia'),
	(201, 'SJ', 'Svalbard and Jan Mayen', 'Svalbard ve Jan Mayen', 'Svalbard and Jan Mayen'),
	(202, 'SK', 'Slovakia', 'Slovakya', 'Slovakia'),
	(203, 'SL', 'Sierra Leone', 'Sierra Leone', 'Sierra Leone'),
	(204, 'SM', 'San Marino', 'San Marino', 'San Marino'),
	(205, 'SN', 'Senegal', 'Senegal', 'Senegal'),
	(206, 'SO', 'Somalia', 'Somali', 'Somalia'),
	(207, 'SR', 'Suriname', 'Surinam', 'Suriname'),
	(208, 'ST', 'Sao Tome ve Principe', 'Sao Tome ve Principe', 'Sao Tome ve Principe'),
	(209, 'SV', 'El Salvador', 'El Salvador', 'El Salvador'),
	(210, 'SY', 'Syria', 'Suriye', 'Syria'),
	(211, 'SZ', 'Swaziland', 'Svaziland', 'Swaziland'),
	(212, 'TC', 'Turks and Caicos Islands', 'Turks ve Caicos Adaları', 'Turks and Caicos Islands'),
	(213, 'TD', 'Chad', 'Çad', 'Chad'),
	(214, 'TF', 'French Southern Territories', 'French Southern Territories', 'French Southern Territories'),
	(215, 'TG', 'Togo', 'Togo', 'Togo'),
	(216, 'TH', 'Thailand', 'Tayland', 'Thailand'),
	(217, 'TJ', 'Tajikistan', 'Tacikistan', 'Tajikistan'),
	(218, 'TK', 'Tokelau', 'Tokelau', 'Tokelau'),
	(219, 'TL', 'Timor', 'Timor', 'Timor'),
	(220, 'TM', 'Turkmenistan', 'Türkmenistan', 'Turkmenistan'),
	(221, 'TN', 'Tunisia', 'Tunus', 'Tunisia'),
	(222, 'TO', 'Tonga', 'Tonga', 'Tonga'),
	(223, 'TR', 'Turkey', 'Türkiye', 'Turkey'),
	(224, 'TT', 'Trinidad and Tobago', 'Trinidad ve Tobago', 'Trinidad and Tobago'),
	(225, 'TV', 'Tuvalu', 'Tuvalu', 'Tuvalu'),
	(226, 'TW', 'Taiwan', 'Tayvan', 'Taiwan'),
	(227, 'TZ', 'Tanzania', 'Tanzanya', 'Tanzania'),
	(228, 'UA', 'Ukrayna', 'Ukrayna', 'Ukrayna'),
	(229, 'UG', 'Uganda', 'Uganda', 'Uganda'),
	(230, 'UM', 'United States Outlying Islands', 'United States Outlying Islands', 'United States Outlying Islands'),
	(231, 'US', 'US', 'Amerika Birleşik Devletleri', 'US'),
	(232, 'UY', 'Uruguay', 'Uruguay', 'Uruguay'),
	(233, 'UZ', 'Uzbekistan', 'Özbekistan', 'Uzbekistan'),
	(234, 'VA', 'Vatican', 'Vatikan', 'Vatican'),
	(235, 'VC', 'Saint Vincent and Grenadines', 'Saint Vincent ve Grenadines', 'Saint Vincent and Grenadines'),
	(236, 'VE', 'Venezuela', 'Venezuela', 'Venezuela'),
	(237, 'VG', 'Virgin Islands', 'Britanya Virjin Adaları', 'Virgin Islands'),
	(238, 'VI', 'Virgin Islands', 'Virjinya Adaları', 'Virgin Islands'),
	(239, 'VN', 'Vietnam', 'Vietnam', 'Vietnam'),
	(240, 'VU', 'Vanuatu', 'Vanuatu', 'Vanuatu'),
	(241, 'WF', 'Wallis and Futuna', 'Vallis ve Futuna', 'Wallis and Futuna'),
	(242, 'WS', 'Samoa', 'Samoa', 'Samoa'),
	(243, 'YE', 'Yemen', 'Yemen', 'Yemen'),
	(244, 'YT', 'Mayotte', 'Mayotte', 'Mayotte'),
	(245, 'ZA', 'South Africa', 'Güney Afrika', 'South Africa'),
	(246, 'ZM', 'Zambia', 'Zambiya', 'Zambia'),
	(247, 'ZW', 'Zimbabwe', 'Zimbabve', 'Zimbabwe'),
	(248, 'XK', 'Kosovo', 'Kosova', 'Kosovo');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.country_groups
CREATE TABLE IF NOT EXISTS `country_groups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `list_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_type` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `owner_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.country_groups: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `country_groups`;
/*!40000 ALTER TABLE `country_groups` DISABLE KEYS */;
INSERT INTO `country_groups` (`id`, `code`, `title`, `list_title`, `owner_type`, `owner_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'gl', 'Global', 'Global', 'Mediapress\\Modules\\Content\\Models\\Website', 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `country_groups` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.country_group_country
CREATE TABLE IF NOT EXISTS `country_group_country` (
  `country_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country_group_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.country_group_country: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `country_group_country`;
/*!40000 ALTER TABLE `country_group_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_group_country` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.country_group_language
CREATE TABLE IF NOT EXISTS `country_group_language` (
  `language_id` int unsigned NOT NULL,
  `country_group_id` int unsigned NOT NULL,
  `default` int unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.country_group_language: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `country_group_language`;
/*!40000 ALTER TABLE `country_group_language` DISABLE KEYS */;
INSERT INTO `country_group_language` (`language_id`, `country_group_id`, `default`) VALUES
	(760, 1, 1),
	(616, 1, 0);
/*!40000 ALTER TABLE `country_group_language` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.criterias
CREATE TABLE IF NOT EXISTS `criterias` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int unsigned NOT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `depth` int unsigned DEFAULT NULL,
  `criteria_id` int unsigned DEFAULT NULL,
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `cdec_1` decimal(20,4) DEFAULT NULL,
  `cdec_2` decimal(20,4) DEFAULT NULL,
  `cdec_3` decimal(20,4) DEFAULT NULL,
  `cdec_4` decimal(20,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `criterias_sitemap_id_index` (`sitemap_id`),
  KEY `criterias_criteria_id_index` (`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.criterias: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `criterias`;
/*!40000 ALTER TABLE `criterias` DISABLE KEYS */;
/*!40000 ALTER TABLE `criterias` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.criteria_details
CREATE TABLE IF NOT EXISTS `criteria_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `criteria_id` int unsigned NOT NULL,
  `country_group_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '3',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `criteria_details_criteria_id_index` (`criteria_id`),
  KEY `criteria_details_country_group_id_index` (`country_group_id`),
  KEY `criteria_details_language_id_index` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.criteria_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `criteria_details`;
/*!40000 ALTER TABLE `criteria_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `criteria_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.criteria_detail_extras
CREATE TABLE IF NOT EXISTS `criteria_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `criteria_detail_id` int unsigned DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `criteria_detail_extras_criteria_detail_id_index` (`criteria_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.criteria_detail_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `criteria_detail_extras`;
/*!40000 ALTER TABLE `criteria_detail_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `criteria_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.criteria_extras
CREATE TABLE IF NOT EXISTS `criteria_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `criteria_id` int unsigned DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `criteria_extras_criteria_id_index` (`criteria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.criteria_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `criteria_extras`;
/*!40000 ALTER TABLE `criteria_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `criteria_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.criteria_page
CREATE TABLE IF NOT EXISTS `criteria_page` (
  `criteria_id` int unsigned NOT NULL,
  `page_id` int unsigned NOT NULL,
  KEY `criteria_page_criteria_id_index` (`criteria_id`),
  KEY `criteria_page_page_id_index` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.criteria_page: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `criteria_page`;
/*!40000 ALTER TABLE `criteria_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `criteria_page` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.dynamic_urls
CREATE TABLE IF NOT EXISTS `dynamic_urls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `url_id` int unsigned NOT NULL,
  `regex` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_id` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.dynamic_urls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `dynamic_urls`;
/*!40000 ALTER TABLE `dynamic_urls` DISABLE KEYS */;
/*!40000 ALTER TABLE `dynamic_urls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.email_sender
CREATE TABLE IF NOT EXISTS `email_sender` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `maillist_id` int NOT NULL,
  `mailtemplate_id` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.email_sender: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `email_sender`;
/*!40000 ALTER TABLE `email_sender` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_sender` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.entities
CREATE TABLE IF NOT EXISTS `entities` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.entities: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `entities`;
/*!40000 ALTER TABLE `entities` DISABLE KEYS */;
/*!40000 ALTER TABLE `entities` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.entitylist
CREATE TABLE IF NOT EXISTS `entitylist` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `list_type` enum('standard','core') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.entitylist: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `entitylist`;
/*!40000 ALTER TABLE `entitylist` DISABLE KEYS */;
/*!40000 ALTER TABLE `entitylist` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.entitylist_pivot
CREATE TABLE IF NOT EXISTS `entitylist_pivot` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `entitylist_id` int NOT NULL,
  `model_id` int NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.entitylist_pivot: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `entitylist_pivot`;
/*!40000 ALTER TABLE `entitylist_pivot` DISABLE KEYS */;
/*!40000 ALTER TABLE `entitylist_pivot` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.entity_list_website
CREATE TABLE IF NOT EXISTS `entity_list_website` (
  `entity_list_id` int NOT NULL,
  `website_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.entity_list_website: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `entity_list_website`;
/*!40000 ALTER TABLE `entity_list_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `entity_list_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.extras
CREATE TABLE IF NOT EXISTS `extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `extras_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `extras`;
/*!40000 ALTER TABLE `extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.e_bulletins
CREATE TABLE IF NOT EXISTS `e_bulletins` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activated` int NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.e_bulletins: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `e_bulletins`;
/*!40000 ALTER TABLE `e_bulletins` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_bulletins` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.failed_jobs: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `failed_jobs`;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.forms
CREATE TABLE IF NOT EXISTS `forms` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `formbuilder_json` longtext COLLATE utf8mb4_unicode_ci,
  `smtp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `port` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `security` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `send_user` tinyint NOT NULL DEFAULT '2',
  `receiver` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `success` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `error` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `captcha` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `captcha_site_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `captcha_secret` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.forms: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `forms`;
/*!40000 ALTER TABLE `forms` DISABLE KEYS */;
INSERT INTO `forms` (`id`, `user_id`, `name`, `slug`, `sender`, `password`, `formbuilder_json`, `smtp`, `port`, `security`, `send_user`, `receiver`, `success`, `error`, `button`, `captcha`, `captcha_site_key`, `captcha_secret`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 0, 'İletişim', 'contactform', NULL, NULL, '{"id":"22629962-c707-4913-a1d1-98ece215e6ef","stages":{"fdd52357-7487-4c2c-b9f1-4887f409fa07":{"children":["2c1622b2-b309-4f95-9334-8aee354312f2","dccc78e9-dc8e-4d03-91dc-77e5ff6e10ee","97dc92ff-edd1-4a0e-8a67-41d8207ca8e8","e47245a3-a6fb-4b0f-8e52-b5b944294bf7","39b7947b-2294-4d3e-b5df-6bafff7f89f8"],"id":"fdd52357-7487-4c2c-b9f1-4887f409fa07"}},"rows":{"2c1622b2-b309-4f95-9334-8aee354312f2":{"config":{"fieldset":false,"legend":"","inputGroup":false},"children":["53ba79e1-0d25-4ac5-be40-37c567a19aa4"],"className":["f-row"],"id":"2c1622b2-b309-4f95-9334-8aee354312f2"},"dccc78e9-dc8e-4d03-91dc-77e5ff6e10ee":{"config":{"fieldset":false,"legend":"","inputGroup":false},"children":["42184e16-f135-48e6-9eb1-df66afdd64b7"],"className":["f-row"],"id":"dccc78e9-dc8e-4d03-91dc-77e5ff6e10ee"},"97dc92ff-edd1-4a0e-8a67-41d8207ca8e8":{"config":{"fieldset":false,"legend":"","inputGroup":false},"children":["0db216bd-2cd1-43bd-922b-d116dcb6d1c3"],"className":["f-row"],"id":"97dc92ff-edd1-4a0e-8a67-41d8207ca8e8"},"39b7947b-2294-4d3e-b5df-6bafff7f89f8":{"config":{"fieldset":false,"legend":"","inputGroup":false},"children":["3620e630-9e8f-4303-9372-48929a3d50b8"],"className":["f-row"],"id":"39b7947b-2294-4d3e-b5df-6bafff7f89f8"},"e47245a3-a6fb-4b0f-8e52-b5b944294bf7":{"config":{"fieldset":false,"legend":"","inputGroup":false},"children":["1ab7029d-f422-4e8a-b621-b43e620dd7aa"],"className":["f-row"],"id":"e47245a3-a6fb-4b0f-8e52-b5b944294bf7"}},"columns":{"53ba79e1-0d25-4ac5-be40-37c567a19aa4":{"config":{"width":"100%"},"children":["9463985a-e027-4389-b732-d2c68a06f047"],"className":"f-column","id":"53ba79e1-0d25-4ac5-be40-37c567a19aa4"},"42184e16-f135-48e6-9eb1-df66afdd64b7":{"config":{"width":"100%"},"children":["76f3bb56-239d-43c4-b9d4-75af40210180"],"className":"f-column","id":"42184e16-f135-48e6-9eb1-df66afdd64b7"},"0db216bd-2cd1-43bd-922b-d116dcb6d1c3":{"config":{"width":"100%"},"children":["470e6965-c8b0-469f-83f5-d040c63ca899"],"className":"f-column","id":"0db216bd-2cd1-43bd-922b-d116dcb6d1c3"},"3620e630-9e8f-4303-9372-48929a3d50b8":{"config":{"width":"100%"},"children":["834a7a2f-73ae-4961-bb2e-b5007ed274ae"],"className":"f-column","id":"3620e630-9e8f-4303-9372-48929a3d50b8"},"1ab7029d-f422-4e8a-b621-b43e620dd7aa":{"config":{"width":"100%"},"children":["84f274d9-abed-4fc0-b348-37d8c273541d"],"className":"f-column","id":"1ab7029d-f422-4e8a-b621-b43e620dd7aa"}},"fields":{"9463985a-e027-4389-b732-d2c68a06f047":{"conditions":[{"if":[{"source":"","sourceProperty":"value","comparison":"equals","target":"","targetProperty":"value"}],"then":[{"target":"","targetProperty":"value","assignment":"equals","value":""}]}],"tag":"input","attrs":{"required":false,"type":"text","className":"","data-validation":"length","data-validation-length":"3-12","data-validation-error-msg":"form.input.required"},"config":{"label":"contactform.name"},"meta":{"group":"common","icon":"text-input","id":"text-input"},"id":"9463985a-e027-4389-b732-d2c68a06f047"},"76f3bb56-239d-43c4-b9d4-75af40210180":{"conditions":[{"if":[{"source":"","sourceProperty":"value","comparison":"equals","target":"","targetProperty":"value"}],"then":[{"target":"","targetProperty":"value","assignment":"equals","value":""}]}],"tag":"input","attrs":{"required":false,"type":"text","className":"","data-validation":"length","data-validation-length":"3-12","data-validation-error-msg":"form.input.required"},"config":{"label":"contactform.surname"},"meta":{"group":"common","icon":"text-input","id":"text-input"},"id":"76f3bb56-239d-43c4-b9d4-75af40210180"},"470e6965-c8b0-469f-83f5-d040c63ca899":{"conditions":[{"if":[{"source":"","sourceProperty":"value","comparison":"equals","target":"","targetProperty":"value"}],"then":[{"target":"","targetProperty":"value","assignment":"equals","value":""}]}],"tag":"input","config":{"label":"contactform.phone","disabledAttrs":["type"],"lockedAttrs":["required","className","data-validation","data-validation-length","data-validation-error-msg"]},"meta":{"group":"common","id":"phone","icon":"+"},"attrs":{"className":"telmask","type":"phone","required":true,"data-validation":"length","data-validation-length":"1-100","data-validation-error-msg":"form.mobile.error"},"id":"470e6965-c8b0-469f-83f5-d040c63ca899"},"834a7a2f-73ae-4961-bb2e-b5007ed274ae":{"conditions":[{"if":[{"source":"","sourceProperty":"value","comparison":"equals","target":"","targetProperty":"value"}],"then":[{"target":"","targetProperty":"value","assignment":"equals","value":""}]}],"tag":"textarea","config":{"label":"contactform.message"},"meta":{"group":"common","icon":"textarea","id":"textarea"},"attrs":{"required":false},"id":"834a7a2f-73ae-4961-bb2e-b5007ed274ae"},"84f274d9-abed-4fc0-b348-37d8c273541d":{"conditions":[{"if":[{"source":"","sourceProperty":"value","comparison":"equals","target":"","targetProperty":"value"}],"then":[{"target":"","targetProperty":"value","assignment":"equals","value":""}]}],"tag":"input","config":{"label":"contactform.mail","disabledAttrs":["type"],"lockedAttrs":["required","className","data-validation","data-validation-error-msg"]},"meta":{"group":"common","id":"email","icon":"@"},"attrs":{"className":"custom-email","type":"email","required":true,"data-validation":"email","data-validation-error-msg":"form.email.error"},"id":"84f274d9-abed-4fc0-b348-37d8c273541d"}}}', NULL, NULL, NULL, 2, 'serkan.iskender@mediaclick.com.tr', 'contactform.success', 'contactform.error', 'contact.send', NULL, NULL, NULL, '2021-03-16 14:24:29', '2021-03-16 15:08:08', NULL);
/*!40000 ALTER TABLE `forms` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.form_details
CREATE TABLE IF NOT EXISTS `form_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rules` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `where` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int NOT NULL DEFAULT '1',
  `values` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `list_option` int DEFAULT NULL,
  `select` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `div_attr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `region_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datasource` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.form_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `form_details`;
/*!40000 ALTER TABLE `form_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `form_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.form_website
CREATE TABLE IF NOT EXISTS `form_website` (
  `form_id` int NOT NULL,
  `website_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.form_website: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `form_website`;
/*!40000 ALTER TABLE `form_website` DISABLE KEYS */;
INSERT INTO `form_website` (`form_id`, `website_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `form_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.instagram_albums
CREATE TABLE IF NOT EXISTS `instagram_albums` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `sync_type` int DEFAULT NULL,
  `sync_data` text COLLATE utf8mb4_unicode_ci,
  `sync_minutes` int DEFAULT NULL,
  `sync_last` time DEFAULT NULL,
  `sync_limit` int DEFAULT NULL,
  `sync_priority` text COLLATE utf8mb4_unicode_ci,
  `last_posts_datas` json DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.instagram_albums: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `instagram_albums`;
/*!40000 ALTER TABLE `instagram_albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `instagram_albums` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.instagram_posts
CREATE TABLE IF NOT EXISTS `instagram_posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `instagram_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_datas` json NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.instagram_posts: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `instagram_posts`;
/*!40000 ALTER TABLE `instagram_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `instagram_posts` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.instagram_post_album
CREATE TABLE IF NOT EXISTS `instagram_post_album` (
  `album_id` int NOT NULL,
  `post_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.instagram_post_album: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `instagram_post_album`;
/*!40000 ALTER TABLE `instagram_post_album` DISABLE KEYS */;
/*!40000 ALTER TABLE `instagram_post_album` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.jobs
CREATE TABLE IF NOT EXISTS `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.jobs: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `jobs`;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.languages
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alphabet` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `native` varchar(190) COLLATE utf8mb4_unicode_ci NOT NULL,
  `regional` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `flag` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fontColor` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hexColor` varchar(190) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `default` tinyint(1) NOT NULL,
  `sort` int unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=869 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.languages: ~285 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `languages`;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`id`, `code`, `name`, `alphabet`, `native`, `regional`, `flag`, `fontColor`, `hexColor`, `direction`, `active`, `default`, `sort`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(580, 'ace', 'Achinese', 'Latn', 'Aceh', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(581, 'af', 'Afrikaans', 'Latn', 'Afrikaans', 'af_ZA', 'AF.png', '#BF0000', '#009900', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(582, 'agq', 'Aghem', 'Latn', 'Aghem', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(583, 'ak', 'Akan', 'Latn', 'Akan', 'ak_GH', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(584, 'an', 'Aragonese', 'Latn', 'aragonés', 'an_ES', 'AN.png', '#000079', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(585, 'cch', 'Atsam', 'Latn', 'Atsam', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(586, 'gn', 'Guaraní', 'Latn', 'Avañe’ẽ', '', 'GN.png', '#CE1126', '#009460', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(587, 'ae', 'Avestan', 'Latn', 'avesta', '', 'AE.png', '#009A00', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(588, 'ay', 'Aymara', 'Latn', 'aymar aru', 'ay_PE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(589, 'az', 'Azerbaijani (Latin)', 'Latn', 'azərbaycanca', 'az_AZ', 'AZ.png', '#00AE68', '#0092C7', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(590, 'id', 'Indonesian', 'Latn', 'Bahasa Indonesia', 'id_ID', 'ID.png', '#CE1126', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(591, 'ms', 'Malay', 'Latn', 'Bahasa Melayu', 'ms_MY', 'MS.png', '#000067', '#010066', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(592, 'bm', 'Bambara', 'Latn', 'bamanakan', '', 'BM.png', '#CC0000', '#CC0001', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(593, 'jv', 'Javanese (Latin)', 'Latn', 'Basa Jawa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(594, 'su', 'Sundanese', 'Latn', 'Basa Sunda', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(595, 'bh', 'Bihari', 'Latn', 'Bihari', '', 'BH.png', '#FFFFFF', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(596, 'bi', 'Bislama', 'Latn', 'Bislama', '', 'BI.png', '#1EB53A', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(597, 'nb', 'Norwegian Bokmål', 'Latn', 'Bokmål', 'nb_NO', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(598, 'bs', 'Bosnian', 'Latn', 'bosanski', 'bs_BA', 'BS.png', '#FAE042', '#00ABC9', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(599, 'br', 'Breton', 'Latn', 'brezhoneg', 'br_FR', 'BR.png', '#00933E', '#00923F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(600, 'ca', 'Catalan', 'Latn', 'català', 'ca_ES', 'CA.png', '#FFFFFF', '#FF0000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(601, 'ch', 'Chamorro', 'Latn', 'Chamoru', '', 'CH.png', '#FFFFFF', '#D81E05', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(602, 'ny', 'Chewa', 'Latn', 'chiCheŵa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(603, 'kde', 'Makonde', 'Latn', 'Chimakonde', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(604, 'sn', 'Shona', 'Latn', 'chiShona', '', 'SN.png', '#00853F', '#E31B23', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(605, 'co', 'Corsican', 'Latn', 'corsu', '', 'CO.png', '#CE1126', '#FCD116', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(606, 'cy', 'Welsh', 'Latn', 'Cymraeg', 'cy_GB', 'CY.png', '#EA6A00', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(607, 'da', 'Danish', 'Latn', 'dansk', 'da_DK', '', NULL, NULL, 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(608, 'se', 'Northern Sami', 'Latn', 'davvisámegiella', 'se_NO', 'SE.png', '#FFC000', '#0B5089', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(609, 'de', 'German', 'Latn', 'Deutsch', 'de_DE', 'DE.png', '#D80000', '#000000', 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(610, 'luo', 'Luo', 'Latn', 'Dholuo', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(611, 'nv', 'Navajo', 'Latn', 'Diné bizaad', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(612, 'dua', 'Duala', 'Latn', 'duálá', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(613, 'et', 'Estonian', 'Latn', 'eesti', 'et_EE', 'ET.png', '#E23D28', '#006B3F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(614, 'na', 'Nauru', 'Latn', 'Ekakairũ Naoero', '', 'NA.png', '#003580', '#009543', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(615, 'guz', 'Ekegusii', 'Latn', 'Ekegusii', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(616, 'en', 'English', 'Latn', 'English', 'en_GB', 'US.png', '#FFFFFF', '#3C3B6E', 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(620, 'es', 'Spanish', 'Latn', 'español', 'es_ES', 'ES.png', '#FFC400', '#C60B1E', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(621, 'eo', 'Esperanto', 'Latn', 'esperanto', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(622, 'eu', 'Basque', 'Latn', 'euskara', 'eu_ES', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(623, 'ewo', 'Ewondo', 'Latn', 'ewondo', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(624, 'ee', 'Ewe', 'Latn', 'eʋegbe', '', 'EE.png', '#3A75C4', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(625, 'fil', 'Filipino', 'Latn', 'Filipino', 'fil_PH', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(626, 'fr', 'French', 'Latn', 'français', 'fr_FR', 'FR.png', '#FFFFFF', '#F42A41', 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(627, 'fr-CA', 'Canadian French', 'Latn', 'français canadien', 'fr_CA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(628, 'fy', 'Western Frisian', 'Latn', 'frysk', 'fy_DE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(629, 'fur', 'Friulian', 'Latn', 'furlan', 'fur_IT', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(630, 'fo', 'Faroese', 'Latn', 'føroyskt', 'fo_FO', 'FO.png', '#D20014', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(631, 'gaa', 'Ga', 'Latn', 'Ga', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(632, 'ga', 'Irish', 'Latn', 'Gaeilge', 'ga_IE', 'GA.png', '#FCD116', '#009E60', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(633, 'gv', 'Manx', 'Latn', 'Gaelg', 'gv_GB', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(634, 'sm', 'Samoan', 'Latn', 'Gagana fa’a Sāmoa', '', 'SM.png', '#0082D1', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(635, 'gl', 'Galician', 'Latn', 'galego', 'gl_ES', 'GL.png', '#DE2918', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(636, 'ki', 'Kikuyu', 'Latn', 'Gikuyu', '', 'KI.png', '#CE1126', '#003F87', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(637, 'gd', 'Scottish Gaelic', 'Latn', 'Gàidhlig', 'gd_GB', 'GD.png', '#007A5E', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(638, 'ha', 'Hausa', 'Latn', 'Hausa', 'ha_NG', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(639, 'bez', 'Bena', 'Latn', 'Hibena', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(640, 'ho', 'Hiri Motu', 'Latn', 'Hiri Motu', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(641, 'hr', 'Croatian', 'Latn', 'hrvatski', 'hr_HR', 'HR.png', '#021272', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(642, 'bem', 'Bemba', 'Latn', 'Ichibemba', 'bem_ZM', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(643, 'io', 'Ido', 'Latn', 'Ido', '', 'IO.png', '#FFFFFE', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(644, 'ig', 'Igbo', 'Latn', 'Igbo', 'ig_NG', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(645, 'rn', 'Rundi', 'Latn', 'Ikirundi', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(646, 'ia', 'Interlingua', 'Latn', 'interlingua', 'ia_FR', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(647, 'iu-Latn', 'Inuktitut (Latin)', 'Latn', 'Inuktitut', 'iu_CA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(648, 'sbp', 'Sileibi', 'Latn', 'Ishisangu', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(649, 'nd', 'North Ndebele', 'Latn', 'isiNdebele', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(650, 'nr', 'South Ndebele', 'Latn', 'isiNdebele', 'nr_ZA', 'NR.png', '#002682', '#002B7F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(651, 'xh', 'Xhosa', 'Latn', 'isiXhosa', 'xh_ZA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(652, 'zu', 'Zulu', 'Latn', 'isiZulu', 'zu_ZA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(653, 'it', 'Italian', 'Latn', 'italiano', 'it_IT', 'IT.png', '#CE2B37', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(654, 'ik', 'Inupiaq', 'Latn', 'Iñupiaq', 'ik_CA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(655, 'dyo', 'Jola-Fonyi', 'Latn', 'joola', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(656, 'kea', 'Kabuverdianu', 'Latn', 'kabuverdianu', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(657, 'kaj', 'Jju', 'Latn', 'Kaje', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(658, 'mh', 'Marshallese', 'Latn', 'Kajin M̧ajeļ', 'mh_MH', 'MH.png', '#FFFFFF', '#003893', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(659, 'kl', 'Kalaallisut', 'Latn', 'kalaallisut', 'kl_GL', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(660, 'kln', 'Kalenjin', 'Latn', 'Kalenjin', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(661, 'kr', 'Kanuri', 'Latn', 'Kanuri', '', 'KR.png', '#888888', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(662, 'kcg', 'Tyap', 'Latn', 'Katab', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(663, 'kw', 'Cornish', 'Latn', 'kernewek', 'kw_GB', 'KW.png', '#009977', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(664, 'naq', 'Nama', 'Latn', 'Khoekhoegowab', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(665, 'rof', 'Rombo', 'Latn', 'Kihorombo', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(666, 'kam', 'Kamba', 'Latn', 'Kikamba', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(667, 'kg', 'Kongo', 'Latn', 'Kikongo', '', 'KG.png', '#FC2050', '#FD2152', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(668, 'jmc', 'Machame', 'Latn', 'Kimachame', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(669, 'rw', 'Kinyarwanda', 'Latn', 'Kinyarwanda', 'rw_RW', 'RW.png', '#007C59', '#0084C9', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(670, 'asa', 'Kipare', 'Latn', 'Kipare', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(671, 'rwk', 'Rwa', 'Latn', 'Kiruwa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(672, 'saq', 'Samburu', 'Latn', 'Kisampur', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(673, 'ksb', 'Shambala', 'Latn', 'Kishambaa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(674, 'swc', 'Congo Swahili', 'Latn', 'Kiswahili ya Kongo', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(675, 'sw', 'Swahili', 'Latn', 'Kiswahili', 'sw_KE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(676, 'dav', 'Dawida', 'Latn', 'Kitaita', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(677, 'teo', 'Teso', 'Latn', 'Kiteso', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(678, 'khq', 'Koyra Chiini', 'Latn', 'Koyra ciini', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(679, 'ses', 'Songhay', 'Latn', 'Koyraboro senni', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(680, 'mfe', 'Morisyen', 'Latn', 'kreol morisien', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(681, 'ht', 'Haitian', 'Latn', 'Kreyòl ayisyen', 'ht_HT', 'HT.png', '#D21034', '#00209F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(682, 'kj', 'Kuanyama', 'Latn', 'Kwanyama', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(683, 'ksh', 'Kölsch', 'Latn', 'Kölsch', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(684, 'ebu', 'Kiembu', 'Latn', 'Kĩembu', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(685, 'mer', 'Kimîîru', 'Latn', 'Kĩmĩrũ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(686, 'lag', 'Langi', 'Latn', 'Kɨlaangi', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(687, 'lah', 'Lahnda', 'Latn', 'Lahnda', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(688, 'la', 'Latin', 'Latn', 'latine', '', 'LA.png', '#002868', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(689, 'lv', 'Latvian', 'Latn', 'latviešu', 'lv_LV', 'LV.png', '#FFFFFF', '#861623', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(690, 'to', 'Tongan', 'Latn', 'lea fakatonga', '', 'TO.png', '#FFFFFF', '#C10000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(691, 'lt', 'Lithuanian', 'Latn', 'lietuvių', 'lt_LT', 'LT.png', '#C1272D', '#FDB913', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(692, 'li', 'Limburgish', 'Latn', 'Limburgs', 'li_BE', 'LI.png', '#002B7F', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(693, 'ln', 'Lingala', 'Latn', 'lingála', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(694, 'lg', 'Ganda', 'Latn', 'Luganda', 'lg_UG', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(695, 'luy', 'Oluluyia', 'Latn', 'Luluhia', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(696, 'lb', 'Luxembourgish', 'Latn', 'Lëtzebuergesch', 'lb_LU', 'LB.png', '#FFFFFF', '#FD3118', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(697, 'hu', 'Hungarian', 'Latn', 'magyar', 'hu_HU', 'HU.png', '#008D46', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(698, 'mgh', 'Makhuwa-Meetto', 'Latn', 'Makua', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(699, 'mg', 'Malagasy', 'Latn', 'Malagasy', 'mg_MG', 'MG.png', '#00B760', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(700, 'mt', 'Maltese', 'Latn', 'Malti', 'mt_MT', 'MT.png', '#FFFFFF', '#CF142B', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(701, 'mtr', 'Mewari', 'Latn', 'Mewari', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(702, 'mua', 'Mundang', 'Latn', 'Mundang', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(703, 'mi', 'Māori', 'Latn', 'Māori', 'mi_NZ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(704, 'nl', 'Dutch', 'Latn', 'Nederlands', 'nl_NL', 'NL.png', '#21468B', '#AE1C28', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(705, 'nmg', 'Kwasio', 'Latn', 'ngumba', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(706, 'yav', 'Yangben', 'Latn', 'nuasue', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(707, 'nn', 'Norwegian Nynorsk', 'Latn', 'nynorsk', 'nn_NO', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(708, 'oc', 'Occitan', 'Latn', 'occitan', 'oc_FR', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(710, 'xog', 'Soga', 'Latn', 'Olusoga', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(711, 'om', 'Oromo', 'Latn', 'Oromoo', 'om_ET', 'OM.png', '#FFFFFF', '#FC3D32', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(712, 'ng', 'Ndonga', 'Latn', 'OshiNdonga', '', 'NG.png', '#FFFFFF', '#008751', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(713, 'hz', 'Herero', 'Latn', 'Otjiherero', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(714, 'uz-Latn', 'Uzbek (Latin)', 'Latn', 'oʼzbekcha', 'uz_UZ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(715, 'nds', 'Low German', 'Latn', 'Plattdüütsch', 'nds_DE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(716, 'pl', 'Polish', 'Latn', 'polski', 'pl_PL', 'PL.png', '#FFFFFF', '#DC143C', 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(717, 'pt', 'Portuguese', 'Latn', 'português', 'pt_PT', 'PT.png', '#006233', '#DC241F', 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(718, 'pt-BR', 'Brazilian Portuguese', 'Latn', 'português do Brasil', 'pt_BR', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(719, 'ff', 'Fulah', 'Latn', 'Pulaar', 'ff_SN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(720, 'pi', 'Pahari-Potwari', 'Latn', 'Pāli', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(721, 'aa', 'Afar', 'Latn', 'Qafar', 'aa_ER', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(722, 'ty', 'Tahitian', 'Latn', 'Reo Māohi', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(723, 'ksf', 'Bafia', 'Latn', 'rikpa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(724, 'ro', 'Romanian', 'Latn', 'română', 'ro_RO', 'RO.png', '#FCD116', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(725, 'cgg', 'Chiga', 'Latn', 'Rukiga', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(726, 'rm', 'Romansh', 'Latn', 'rumantsch', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(727, 'qu', 'Quechua', 'Latn', 'Runa Simi', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(728, 'nyn', 'Nyankole', 'Latn', 'Runyankore', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(729, 'ssy', 'Saho', 'Latn', 'Saho', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(730, 'sc', 'Sardinian', 'Latn', 'sardu', 'sc_IT', 'SC.png', '#003F87', '#D62828', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(731, 'de-CH', 'Swiss High German', 'Latn', 'Schweizer Hochdeutsch', 'de_CH', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(732, 'gsw', 'Swiss German', 'Latn', 'Schwiizertüütsch', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(733, 'trv', 'Taroko', 'Latn', 'Seediq', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(734, 'seh', 'Sena', 'Latn', 'sena', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(735, 'nso', 'Northern Sotho', 'Latn', 'Sesotho sa Leboa', 'nso_ZA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(736, 'st', 'Southern Sotho', 'Latn', 'Sesotho', 'st_ZA', 'ST.png', '#FFCE00', '#12AD2B', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(737, 'tn', 'Tswana', 'Latn', 'Setswana', 'tn_ZA', 'TN.png', '#FFFFFF', '#E70013', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(738, 'sq', 'Albanian', 'Latn', 'shqip', 'sq_AL', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(739, 'sid', 'Sidamo', 'Latn', 'Sidaamu Afo', 'sid_ET', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(740, 'ss', 'Swati', 'Latn', 'Siswati', 'ss_ZA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(741, 'sk', 'Slovak', 'Latn', 'slovenčina', 'sk_SK', 'SK.png', '#CE1126', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(742, 'sl', 'Slovene', 'Latn', 'slovenščina', 'sl_SI', 'SL.png', '#0072C6', '#1EB53A', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(743, 'so', 'Somali', 'Latn', 'Soomaali', 'so_SO', 'SO.png', '#438ADD', '#4189DD', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(744, 'sr-Latn', 'Serbian (Latin)', 'Latn', 'Srpski', 'sr_RS', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(745, 'sh', 'Serbo-Croatian', 'Latn', 'srpskohrvatski', '', 'SH.png', '#000066', '#010066', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(746, 'fi', 'Finnish', 'Latn', 'suomi', 'fi_FI', 'FI.png', '#003580', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(747, 'sv', 'Swedish', 'Latn', 'svenska', 'sv_FI', 'SV.png', '#FFFFFF', '#00209F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(748, 'sg', 'Sango', 'Latn', 'Sängö', '', 'SG.png', '#F42A41', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(749, 'tl', 'Tagalog', 'Latn', 'Tagalog', 'tl_PH', 'TL.png', '#DC261F', '#DC241F', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(750, 'tzm-Latn', 'Central Atlas Tamazight (Latin)', 'Latn', 'Tamazight', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(751, 'kab', 'Kabyle', 'Latn', 'Taqbaylit', 'kab_DZ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(752, 'twq', 'Tasawaq', 'Latn', 'Tasawaq senni', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(753, 'shi', 'Tachelhit (Latin)', 'Latn', 'Tashelhit', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(754, 'nus', 'Nuer', 'Latn', 'Thok Nath', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(755, 'vi', 'Vietnamese', 'Latn', 'Tiếng Việt', 'vi_VN', 'VI.png', '#FFFFFE', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(756, 'tg-Latn', 'Tajik (Latin)', 'Latn', 'tojikī', 'tg_TJ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(757, 'lu', 'Luba-Katanga', 'Latn', 'Tshiluba', 've_ZA', 'LU.png', '#00A3DD', '#EF2B2D', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(758, 've', 'Venda', 'Latn', 'Tshivenḓa', '', 'VE.png', '#F7D117', '#CF142B', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(759, 'tw', 'Twi', 'Latn', 'Twi', '', 'TW.png', '#000099', '#FF0000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(760, 'tr', 'Turkish', 'Latn', 'Türkçe', 'tr_TR', 'TR.png', '#FFFFFF', '#E30A17', 'ltr', 1, 1, 0, NULL, NULL, NULL),
	(761, 'ale', 'Aleut', 'Latn', 'Unangax tunuu', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(762, 'ca-valencia', 'Valencian', 'Latn', 'valencià', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(763, 'vai-Latn', 'Vai (Latin)', 'Latn', 'Viyamíĩ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(764, 'vo', 'Volapük', 'Latn', 'Volapük', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(765, 'fj', 'Fijian', 'Latn', 'vosa Vakaviti', '', 'FJ.png', '#5D96B0', '#5B97B1', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(766, 'wa', 'Walloon', 'Latn', 'Walon', 'wa_BE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(767, 'wae', 'Walser', 'Latn', 'Walser', 'wae_CH', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(768, 'wen', 'Sorbian', 'Latn', 'Wendic', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(769, 'wo', 'Wolof', 'Latn', 'Wolof', 'wo_SN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(770, 'ts', 'Tsonga', 'Latn', 'Xitsonga', 'ts_ZA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(771, 'dje', 'Zarma', 'Latn', 'Zarmaciine', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(772, 'yo', 'Yoruba', 'Latn', 'Èdè Yorùbá', 'yo_NG', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(773, 'de-AT', 'Austrian German', 'Latn', 'Österreichisches Deutsch', 'de_AT', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(774, 'is', 'Icelandic', 'Latn', 'íslenska', 'is_IS', 'IS.png', '#001484', '#003897', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(775, 'cs', 'Czech', 'Latn', 'čeština', 'cs_CZ', 'CS.png', '#DE0029', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(776, 'bas', 'Basa', 'Latn', 'Ɓàsàa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(777, 'mas', 'Masai', 'Latn', 'ɔl-Maa', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(778, 'haw', 'Hawaiian', 'Latn', 'ʻŌlelo Hawaiʻi', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(779, 'el', 'Greek', 'Grek', 'Ελληνικά', 'el_GR', '', NULL, NULL, 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(780, 'uz', 'Uzbek (Cyrillic)', 'Cyrl', 'Ўзбек', 'uz_UZ', 'UZ.png', '#1EB53A', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(781, 'az-Cyrl', 'Azerbaijani (Cyrillic)', 'Cyrl', 'Азәрбајҹан', 'uz_UZ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(782, 'ab', 'Abkhazian', 'Cyrl', 'Аҧсуа', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(783, 'os', 'Ossetic', 'Cyrl', 'Ирон', 'os_RU', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(784, 'ky', 'Kyrgyz', 'Cyrl', 'Кыргыз', 'ky_KG', 'KY.png', '#000098', '#000099', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(785, 'sr', 'Serbian (Cyrillic)', 'Cyrl', 'Српски', 'sr_RS', 'SR.png', '#D21034', '#006819', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(786, 'av', 'Avaric', 'Cyrl', 'авар мацӀ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(787, 'ady', 'Adyghe', 'Cyrl', 'адыгэбзэ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(788, 'ba', 'Bashkir', 'Cyrl', 'башҡорт теле', '', 'BA.png', '#F7D117', '#171796', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(789, 'be', 'Belarusian', 'Cyrl', 'беларуская', 'be_BY', 'BE.png', '#FFDE00', '#000000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(790, 'bg', 'Bulgarian', 'Cyrl', 'български', 'bg_BG', 'BG.png', '#00966E', '#D62612', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(791, 'kv', 'Komi', 'Cyrl', 'коми кыв', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(792, 'mk', 'Macedonian', 'Cyrl', 'македонски', 'mk_MK', 'MK.png', '#C6002B', '#CE2028', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(793, 'mn', 'Mongolian (Cyrillic)', 'Cyrl', 'монгол', 'mn_MN', 'MN.png', '#DC171C', '#47AECF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(794, 'ce', 'Chechen', 'Cyrl', 'нохчийн мотт', 'ce_RU', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(795, 'ru', 'Russian', 'Cyrl', 'русский', 'ru_RU', 'RU.png', '#FF0000', '#0000FF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(796, 'sah', 'Yakut', 'Cyrl', 'саха тыла', '', '', NULL, NULL, 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(797, 'tt', 'Tatar', 'Cyrl', 'татар теле', 'tt_RU', 'TT.png', '#FFFFFF', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(798, 'tg', 'Tajik (Cyrillic)', 'Cyrl', 'тоҷикӣ', 'tg_TJ', 'TG.png', '#FFD000', '#006A4E', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(799, 'tk', 'Turkmen', 'Cyrl', 'түркменче', 'tk_TM', 'TK.png', '#020266', '#010066', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(800, 'uk', 'Ukrainian', 'Cyrl', 'українська', 'uk_UA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(801, 'cv', 'Chuvash', 'Cyrl', 'чӑваш чӗлхи', 'cv_RU', 'CV.png', '#FAFFFF', '#003893', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(802, 'cu', 'Church Slavic', 'Cyrl', 'ѩзыкъ словѣньскъ', '', 'CU.png', '#001986', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(803, 'kk', 'Kazakh', 'Cyrl', 'қазақ тілі', 'kk_KZ', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(804, 'hy', 'Armenian', 'Armn', 'Հայերէն', 'hy_AM', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(805, 'yi', 'Yiddish', 'Hebr', 'ייִדיש', 'yi_US', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(806, 'he', 'Hebrew', 'Hebr', 'עברית', 'he_IL', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(807, 'ug', 'Uyghur', 'Arab', 'ئۇيغۇرچە', 'ug_CN', 'UG.png', '#D92209', '#FFFF00', 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(808, 'ur', 'Urdu', 'Arab', 'اردو', 'ur_PK', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(809, 'ar', 'Arabic', 'Arab', 'العربية', 'ar_AE', 'AR.png', '#FFFFFF', '#2B970A', 'rtl', 1, 0, 1, NULL, NULL, NULL),
	(810, 'uz-Arab', 'Uzbek (Arabic)', 'Arab', 'اۉزبېک', '', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(811, 'tg-Arab', 'Tajik (Arabic)', 'Arab', 'تاجیکی', 'tg_TJ', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(812, 'sd', 'Sindhi', 'Arab', 'سنڌي', 'sd_IN', 'SD.png', '#FFFFFF', '#000000', 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(813, 'fa', 'Persian', 'Arab', 'فارسی', 'fa_IR', '', NULL, NULL, 'rtl', 1, 0, 1, NULL, NULL, NULL),
	(814, 'pa-Arab', 'Punjabi (Arabic)', 'Arab', 'پنجاب', 'pa_IN', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(815, 'ps', 'Pashto', 'Arab', 'پښتو', 'ps_AF', 'PS.png', '#FFFFFF', '#000000', 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(816, 'ks', 'Kashmiri (Arabic)', 'Arab', 'کأشُر', 'ks_IN', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(817, 'ku', 'Kurdish', 'Arab', 'کوردی', 'ku_TR', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(818, 'dv', 'Divehi', 'Thaa', 'ދިވެހިބަސް', 'dv_MV', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(819, 'ks-Deva', 'Kashmiri (Devaganari)', 'Deva', 'कॉशुर', 'ks_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(820, 'kok', 'Konkani', 'Deva', 'कोंकणी', 'kok_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(821, 'doi', 'Dogri', 'Deva', 'डोगरी', 'doi_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(822, 'ne', 'Nepali', 'Deva', 'नेपाली', '', 'NE.png', '#0DB02B', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(823, 'pra', 'Prakrit', 'Deva', 'प्राकृत', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(824, 'brx', 'Bodo', 'Deva', 'बड़ो', 'brx_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(825, 'bra', 'Braj', 'Deva', 'ब्रज भाषा', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(826, 'mr', 'Marathi', 'Deva', 'मराठी', 'mr_IN', 'MR.png', '#1FB53A', '#1EB53A', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(827, 'mai', 'Maithili', 'Tirh', 'मैथिली', 'mai_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(828, 'raj', 'Rajasthani', 'Deva', 'राजस्थानी', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(829, 'sa', 'Sanskrit', 'Deva', 'संस्कृतम्', 'sa_IN', 'SA.png', '#00942E', '#009530', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(830, 'hi', 'Hindi', 'Deva', 'हिन्दी', 'hi_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(831, 'as', 'Assamese', 'Beng', 'অসমীয়া', 'as_IN', 'AS.png', '#003580', '#D21034', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(832, 'bn', 'Bengali', 'Beng', 'বাংলা', 'bn_BD', 'BN.png', '#FFFFFF', '#F7E017', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(833, 'mni', 'Manipuri', 'Beng', 'মৈতৈ', 'mni_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(834, 'pa', 'Punjabi (Gurmukhi)', 'Guru', 'ਪੰਜਾਬੀ', 'pa_IN', 'PA.png', '#0067C6', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(835, 'gu', 'Gujarati', 'Gujr', 'ગુજરાતી', 'gu_IN', 'GU.png', '#0055DE', '#0150C7', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(836, 'or', 'Oriya', 'Orya', 'ଓଡ଼ିଆ', 'or_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(837, 'ta', 'Tamil', 'Taml', 'தமிழ்', 'ta_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(838, 'te', 'Telugu', 'Telu', 'తెలుగు', 'te_IN', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(839, 'kn', 'Kannada', 'Knda', 'ಕನ್ನಡ', 'kn_IN', 'KN.png', '#BE0026', '#000000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(840, 'ml', 'Malayalam', 'Mlym', 'മലയാളം', 'ml_IN', 'ML.png', '#14B53A', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(841, 'si', 'Sinhala', 'Sinh', 'සිංහල', 'si_LK', 'SI.png', '#FF0C00', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(842, 'th', 'Thai', 'Thai', 'ไทย', 'th_TH', 'TH.png', '#00247D', '#DE0D15', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(843, 'lo', 'Lao', 'Laoo', 'ລາວ', 'lo_LA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(844, 'bo', 'Tibetan', 'Tibt', 'པོད་སྐད་', 'bo_IN', 'BO.png', '#007A3D', '#CE1126', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(845, 'dz', 'Dzongkha', 'Tibt', 'རྫོང་ཁ', 'dz_BT', 'DZ.png', '#FFFFFF', '#008638', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(846, 'my', 'Burmese', 'Mymr', 'မြန်မာဘာသာ', 'my_MM', 'MY.png', '#E55E5A', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(847, 'ka', 'Georgian', 'Geor', 'ქართული', 'ka_GE', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(848, 'byn', 'Blin', 'Ethi', 'ብሊን', 'byn_ER', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(849, 'tig', 'Tigre', 'Ethi', 'ትግረ', 'tig_ER', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(850, 'ti', 'Tigrinya', 'Ethi', 'ትግርኛ', 'ti_ET', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(851, 'am', 'Amharic', 'Ethi', 'አማርኛ', 'am_ET', 'AM.png', '#FF9900', '#FF0000', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(852, 'wal', 'Wolaytta', 'Ethi', 'ወላይታቱ', 'wal_ET', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(853, 'chr', 'Cherokee', 'Cher', 'ᏣᎳᎩ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(854, 'iu', 'Inuktitut (Canadian Aboriginal Syllabics)', 'Cans', 'ᐃᓄᒃᑎᑐᑦ', 'iu_CA', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(855, 'oj', 'Ojibwa', 'Cans', 'ᐊᓂᔑᓈᐯᒧᐎᓐ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(856, 'cr', 'Cree', 'Cans', 'ᓀᐦᐃᔭᐍᐏᐣ', '', 'CR.png', '#CE1126', '#001B75', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(857, 'km', 'Khmer', 'Khmr', 'ភាសាខ្មែរ', 'km_KH', 'KM.png', '#FFC61E', '#FFFFFF', 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(858, 'mn-Mong', 'Mongolian (Mongolian)', 'Mong', 'ᠮᠣᠨᠭᠭᠣᠯ ᠬᠡᠯᠡ', 'mn_MN', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(859, 'shi-Tfng', 'Tachelhit (Tifinagh)', 'Tfng', 'ⵜⴰⵎⴰⵣⵉⵖⵜ', '', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(860, 'tzm', 'Central Atlas Tamazight (Tifinagh)', 'Tfng', 'ⵜⴰⵎⴰⵣⵉⵖⵜ', '', '', NULL, NULL, 'rtl', 1, 0, 3, NULL, NULL, NULL),
	(861, 'yue', 'Yue', 'Hant', '廣州話', 'yue_HK', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(862, 'ja', 'Japanese', 'Jpan', '日本語', 'ja_JP', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(863, 'zh', 'Chinese (Simplified)', 'Hans', '简体中文', 'zh_CN', 'CN.png', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(864, 'zh-Hant', 'Chinese (Traditional)', 'Hant', '繁體中文', 'zh_CN', '', NULL, NULL, 'ltr', 1, 0, 1, NULL, NULL, NULL),
	(865, 'ii', 'Sichuan Yi', 'Yiii', 'ꆈꌠꉙ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(866, 'vai', 'Vai (Vai)', 'Vaii', 'ꕙꔤ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(867, 'jv-Java', 'Javanese (Javanese)', 'Java', 'ꦧꦱꦗꦮ', '', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL),
	(868, 'ko', 'Korean', 'Hang', '한국어', 'ko_KR', '', NULL, NULL, 'ltr', 1, 0, 3, NULL, NULL, NULL);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.language_parts
CREATE TABLE IF NOT EXISTS `language_parts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int unsigned DEFAULT NULL,
  `country_group_id` int unsigned DEFAULT NULL,
  `website_id` int unsigned DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.language_parts: ~38 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `language_parts`;
/*!40000 ALTER TABLE `language_parts` DISABLE KEYS */;
INSERT INTO `language_parts` (`id`, `language_id`, `country_group_id`, `website_id`, `key`, `value`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 760, 1, 1, 'general.sitename', 'Genarge', NULL, '2021-03-16 15:02:46', NULL),
	(2, 760, 1, 1, 'footer.slogan1', 'Size Nasıl Yardımcı', NULL, NULL, NULL),
	(3, 760, 1, 1, 'footer.slogan2', 'olabiliriz?', NULL, NULL, NULL),
	(4, 760, 1, 1, 'footer.address', 'Muradiye Mah. Celal Bayar Üniversitesi Kampüsü, Küme Evleri, Teknoloji Geliştirme (Bölgesi İçi) Merkezi No:22 / B206 Yunusemre / MANİSA', NULL, NULL, NULL),
	(5, 760, 1, 1, 'footer.phone', '0850 811 51 21', NULL, NULL, NULL),
	(6, 760, 1, 1, 'footer.email', 'info@genarge.com.tr', NULL, NULL, NULL),
	(7, 760, 1, 1, 'footer.contact', 'İletişim', NULL, NULL, NULL),
	(8, 760, 1, 1, 'footer.sign', '© 2021 Gen ARGE, Her hakkı saklıdır.', NULL, '2021-03-16 15:03:40', NULL),
	(9, 760, 1, 1, 'main.whatareyoudoing', 'Neler Yapıyoruz?', NULL, NULL, NULL),
	(10, 760, 1, 1, 'contactform.name', 'Adınız', NULL, '2021-03-16 15:04:05', NULL),
	(11, 760, 1, 1, 'contactform.surname', 'Soyadınız', NULL, '2021-03-16 15:04:25', NULL),
	(12, 760, 1, 1, 'contactform.phone', 'Telefon', NULL, '2021-03-16 15:04:43', NULL),
	(13, 760, 1, 1, 'contactform.message', 'Mesajınız', NULL, '2021-03-16 15:04:54', NULL),
	(14, 760, 1, 1, 'form.input.required', 'Gerekli', NULL, '2021-03-16 15:05:06', NULL),
	(15, 760, 1, 1, 'form.mobile.error', 'Geçerli bir telefon numarası yazmalısınız!', NULL, '2021-03-16 15:08:49', NULL),
	(16, 760, 1, 1, 'form.button.contact.send', 'Gönder', NULL, '2021-03-16 15:06:41', NULL),
	(17, 760, 1, 1, 'contact.title', 'İletişim Formu', NULL, NULL, NULL),
	(18, 616, 1, 1, 'general.sitename', 'Genarge', '2021-03-16 15:02:51', '2021-03-16 15:02:51', NULL),
	(19, 616, 1, 1, 'footer.slogan1', 'How to Help You', '2021-03-16 15:02:58', '2021-03-16 15:02:58', NULL),
	(20, 616, 1, 1, 'footer.slogan2', 'we can be?', '2021-03-16 15:03:06', '2021-03-16 15:03:06', NULL),
	(21, 616, 1, 1, 'footer.address', 'Muradiye Mah. Celal Bayar University Campus, Cluster Houses, Technology Development (In-Region) Center No: 22 / B206 Yunusemre / MANİSA', '2021-03-16 15:03:11', '2021-03-17 07:57:27', NULL),
	(22, 616, 1, 1, 'footer.phone', '0850 811 51 21', '2021-03-16 15:03:15', '2021-03-16 15:03:15', NULL),
	(23, 616, 1, 1, 'footer.email', 'info@genarge.com.tr', '2021-03-16 15:03:19', '2021-03-16 15:03:19', NULL),
	(24, 616, 1, 1, 'footer.contact', 'Contact', '2021-03-16 15:03:26', '2021-03-16 15:03:26', NULL),
	(25, 616, 1, 1, 'footer.sign', '© 2021 Gen ARGE, All rights reserved.', '2021-03-16 15:03:36', '2021-03-16 15:03:43', NULL),
	(26, 616, 1, 1, 'main.whatareyoudoing', 'What Are We Doing?', '2021-03-16 15:03:50', '2021-03-16 15:03:50', NULL),
	(27, 616, 1, 1, 'contactform.name', 'Your name', '2021-03-16 15:04:06', '2021-03-16 15:04:11', NULL),
	(28, 616, 1, 1, 'contactform.surname', 'Surname', '2021-03-16 15:04:32', '2021-03-16 15:04:32', NULL),
	(29, 616, 1, 1, 'contactform.phone', 'Telephone', '2021-03-16 15:04:46', '2021-03-16 15:04:46', NULL),
	(30, 616, 1, 1, 'contactform.message', 'Your message', '2021-03-16 15:04:59', '2021-03-16 15:04:59', NULL),
	(31, 616, 1, 1, 'form.input.required', 'Require', '2021-03-16 15:05:13', '2021-03-16 15:05:13', NULL),
	(32, 616, 1, 1, 'form.mobile.error', 'You must write a valid phone number!', '2021-03-16 15:05:52', '2021-03-16 15:08:54', NULL),
	(33, 616, 1, 1, 'form.button.contact.send', 'Send', '2021-03-16 15:06:45', '2021-03-16 15:06:45', NULL),
	(34, 616, 1, 1, 'contact.title', 'Contact Form', '2021-03-16 15:06:54', '2021-03-16 15:06:54', NULL),
	(35, 760, 1, 1, 'contactform.mail', 'E-Posta', NULL, '2021-03-16 15:09:20', NULL),
	(36, 760, 1, 1, 'form.email.error', 'Geçerli bir e-posta adresi yazmalısınız!', NULL, '2021-03-16 15:09:08', NULL),
	(37, 616, 1, 1, 'form.email.error', 'You must write a valid e-mail address!', '2021-03-16 15:09:12', '2021-03-16 15:09:12', NULL),
	(38, 616, 1, 1, 'contactform.mail', 'E-Mail', '2021-03-16 15:09:23', '2021-03-16 15:09:23', NULL),
	(39, 760, 1, 1, 'main.more', 'Devamını Gör', NULL, NULL, NULL),
	(40, 616, 1, 1, 'main.more', 'Devamını Gör', NULL, NULL, NULL);
/*!40000 ALTER TABLE `language_parts` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.language_website
CREATE TABLE IF NOT EXISTS `language_website` (
  `language_id` int NOT NULL,
  `website_id` int NOT NULL,
  `country_group_id` int DEFAULT NULL,
  `default` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.language_website: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `language_website`;
/*!40000 ALTER TABLE `language_website` DISABLE KEYS */;
INSERT INTO `language_website` (`language_id`, `website_id`, `country_group_id`, `default`) VALUES
	(760, 1, NULL, 1);
/*!40000 ALTER TABLE `language_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` tinyint unsigned NOT NULL DEFAULT '0',
  `expire_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.login_attempts: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `login_attempts`;
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.maillists
CREATE TABLE IF NOT EXISTS `maillists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `usernames` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `emails` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.maillists: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `maillists`;
/*!40000 ALTER TABLE `maillists` DISABLE KEYS */;
/*!40000 ALTER TABLE `maillists` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.maillist_website
CREATE TABLE IF NOT EXISTS `maillist_website` (
  `maillist_id` int NOT NULL,
  `website_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.maillist_website: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `maillist_website`;
/*!40000 ALTER TABLE `maillist_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `maillist_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mail_templates
CREATE TABLE IF NOT EXISTS `mail_templates` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mail_templates: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mail_templates`;
/*!40000 ALTER TABLE `mail_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_templates` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mail_template_website
CREATE TABLE IF NOT EXISTS `mail_template_website` (
  `mail_template_id` int NOT NULL,
  `website_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mail_template_website: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mail_template_website`;
/*!40000 ALTER TABLE `mail_template_website` DISABLE KEYS */;
/*!40000 ALTER TABLE `mail_template_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mdisks
CREATE TABLE IF NOT EXISTS `mdisks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `diskkey` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `driver` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `root` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mdisks_diskkey_unique` (`diskkey`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mdisks: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mdisks`;
/*!40000 ALTER TABLE `mdisks` DISABLE KEYS */;
INSERT INTO `mdisks` (`id`, `diskkey`, `name`, `driver`, `root`, `created_at`, `updated_at`) VALUES
	(1, 'local', 'uploads', 'local', 'uploads/', NULL, NULL);
/*!40000 ALTER TABLE `mdisks` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.menus
CREATE TABLE IF NOT EXISTS `menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int unsigned DEFAULT NULL,
  `sections` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.menus: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `menus`;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` (`id`, `website_id`, `slug`, `name`, `type`, `sections`, `admin_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'header-menu', 'Header Menu', NULL, NULL, 1, 1, NULL, NULL, NULL),
	(2, 1, 'footer-menu', 'Footer Menu', NULL, NULL, 1, 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.menu_details
CREATE TABLE IF NOT EXISTS `menu_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned NOT NULL,
  `language_id` int unsigned DEFAULT NULL,
  `country_group_id` int NOT NULL DEFAULT '1',
  `url_id` int unsigned DEFAULT NULL,
  `type` tinyint unsigned DEFAULT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` int unsigned DEFAULT NULL,
  `type_view` int unsigned DEFAULT NULL,
  `out_link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `depth` int unsigned DEFAULT NULL,
  `draft` tinyint NOT NULL DEFAULT '1',
  `status` tinyint NOT NULL DEFAULT '1',
  `file_id` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.menu_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `menu_details`;
/*!40000 ALTER TABLE `menu_details` DISABLE KEYS */;
INSERT INTO `menu_details` (`id`, `menu_id`, `language_id`, `country_group_id`, `url_id`, `type`, `target`, `name`, `parent`, `type_view`, `out_link`, `lft`, `rgt`, `depth`, `draft`, `status`, `file_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 760, 1, 1, 0, NULL, 'Ana Sayfa', 0, 2, NULL, 1, 2, 0, 0, 1, 0, '2021-03-16 09:26:07', '2021-03-16 09:28:36', NULL),
	(2, 1, 760, 1, NULL, 2, NULL, 'Bizi Tanıyın', 0, 0, NULL, 2, 6, 0, 0, 1, 0, '2021-03-16 09:26:22', '2021-03-16 10:09:26', NULL),
	(3, 1, 760, 1, 7, 0, NULL, 'Neler Yapıyoruz?', 0, 0, NULL, 7, 16, 0, 0, 1, NULL, '2021-03-16 09:26:33', '2021-03-17 07:04:06', NULL),
	(4, 1, 760, 1, NULL, 2, NULL, 'Projeler', 0, 0, NULL, 17, 22, 0, 0, 1, 0, '2021-03-16 09:26:47', '2021-03-16 14:07:19', NULL),
	(5, 1, 760, 1, 33, 0, '_self', 'İletişim', 0, 2, NULL, 23, 24, 0, 0, 1, NULL, '2021-03-16 09:27:01', '2021-03-16 14:29:36', NULL),
	(6, 1, 760, 1, 3, 0, '_self', 'Biz Kimiz?', 2, 2, NULL, 3, 4, 1, 0, 1, 0, '2021-03-16 09:46:28', '2021-03-16 09:46:35', NULL),
	(7, 1, 760, 1, 5, 0, '_self', 'Odak Teknolojiler', 2, 2, NULL, 4, 5, 1, 0, 1, 0, '2021-03-16 10:09:22', '2021-03-16 10:09:26', NULL),
	(8, 1, 760, 1, 9, 0, '_self', 'Özel Web Tabanlı Yazılım Geliştirme', 3, 2, NULL, 8, 9, 1, 0, 1, 0, '2021-03-16 13:34:01', '2021-03-16 13:36:38', NULL),
	(9, 1, 760, 1, 11, 0, '_self', 'Web Tasarım Projeleri', 3, 2, NULL, 9, 10, 1, 0, 1, 0, '2021-03-16 13:34:27', '2021-03-16 13:36:38', NULL),
	(10, 1, 760, 1, 13, 0, '_self', 'Start-Up Proje Danışmanlığı', 3, 2, NULL, 10, 11, 1, 0, 1, 0, '2021-03-16 13:34:50', '2021-03-16 13:36:38', NULL),
	(11, 1, 760, 1, 15, 0, '_self', 'Sunucu Bakımı ve Güvenliği', 3, 2, NULL, 11, 12, 1, 0, 1, 0, '2021-03-16 13:35:10', '2021-03-16 13:36:38', NULL),
	(12, 1, 760, 1, 17, 0, '_self', 'CRM Yazılımları', 3, 2, NULL, 12, 13, 1, 0, 1, 0, '2021-03-16 13:35:28', '2021-03-16 13:36:38', NULL),
	(13, 1, 760, 1, 19, 0, '_self', 'İş Süreci Yönetim Yazılımları', 3, 2, NULL, 13, 14, 1, 0, 1, 0, '2021-03-16 13:35:46', '2021-03-16 13:36:38', NULL),
	(14, 1, 760, 1, 21, 0, '_self', 'Blockchain Teknolojileri', 3, 2, NULL, 14, 15, 1, 0, 1, 0, '2021-03-16 13:36:12', '2021-03-16 13:36:38', NULL),
	(15, 1, 760, 1, 27, 0, '_self', 'MediaPress', 4, 2, NULL, 18, 19, 1, 0, 1, 0, '2021-03-16 14:06:18', '2021-03-16 14:07:19', NULL),
	(16, 1, 760, 1, 29, 0, '_self', 'Folm Universe (Blockchain Project)', 4, 2, NULL, 19, 20, 1, 0, 1, 0, '2021-03-16 14:06:39', '2021-03-16 14:07:19', NULL),
	(17, 1, 760, 1, 31, 0, '_blank', 'Eğitmen.tv (Start-Up)', 4, 2, NULL, 20, 21, 1, 0, 1, 0, '2021-03-16 14:06:57', '2021-03-16 14:07:19', NULL),
	(18, 1, 616, 1, 2, 0, '_self', 'Home Page', 0, 2, NULL, 1, 2, 0, 0, 1, NULL, '2021-03-16 09:26:07', '2021-03-17 07:53:26', NULL),
	(19, 1, 616, 1, NULL, 2, NULL, 'Know Us', 0, 0, NULL, 2, 6, 0, 0, 1, NULL, '2021-03-16 09:26:22', '2021-03-17 07:53:42', NULL),
	(20, 1, 616, 1, 8, 0, NULL, 'What Are We Doing?', 0, 0, NULL, 7, 16, 0, 0, 1, NULL, '2021-03-16 09:26:33', '2021-03-17 07:54:12', NULL),
	(21, 1, 616, 1, NULL, 2, NULL, 'Projects', 0, 0, NULL, 17, 22, 0, 0, 1, NULL, '2021-03-16 09:26:47', '2021-03-17 07:55:27', NULL),
	(22, 1, 616, 1, 34, 0, '_self', 'Contact', 0, 2, NULL, 23, 24, 0, 0, 1, NULL, '2021-03-16 09:27:01', '2021-03-17 07:55:40', NULL),
	(23, 1, 616, 1, 4, 0, '_self', 'Who are we?', 19, 2, NULL, 3, 4, 1, 0, 1, NULL, '2021-03-16 09:46:28', '2021-03-17 07:53:54', NULL),
	(24, 1, 616, 1, 6, 0, '_self', 'Focus Technologies', 19, 2, NULL, 4, 5, 1, 0, 1, NULL, '2021-03-16 10:09:22', '2021-03-17 07:54:01', NULL),
	(25, 1, 616, 1, 10, 0, '_self', 'Custom Web Based Software Development', 20, 2, NULL, 8, 9, 1, 0, 1, NULL, '2021-03-16 13:34:01', '2021-03-17 07:54:21', NULL),
	(26, 1, 616, 1, 12, 0, '_self', 'Web Design Projects', 20, 2, NULL, 9, 10, 1, 0, 1, NULL, '2021-03-16 13:34:27', '2021-03-17 07:54:29', NULL),
	(27, 1, 616, 1, 14, 0, '_self', 'Start-Up Project Consultancy', 20, 2, NULL, 10, 11, 1, 0, 1, NULL, '2021-03-16 13:34:50', '2021-03-17 07:54:37', NULL),
	(28, 1, 616, 1, 16, 0, '_self', 'Server Maintenance and Security', 20, 2, NULL, 11, 12, 1, 0, 1, NULL, '2021-03-16 13:35:10', '2021-03-17 07:54:45', NULL),
	(29, 1, 616, 1, 18, 0, '_self', 'CRM Software', 20, 2, NULL, 12, 13, 1, 0, 1, NULL, '2021-03-16 13:35:28', '2021-03-17 07:54:59', NULL),
	(30, 1, 616, 1, 20, 0, '_self', 'Business Process Management Software', 20, 2, NULL, 13, 14, 1, 0, 1, NULL, '2021-03-16 13:35:46', '2021-03-17 07:55:08', NULL),
	(31, 1, 616, 1, 22, 0, '_self', 'Blockchain Technologies', 20, 2, NULL, 14, 15, 1, 0, 1, NULL, '2021-03-16 13:36:12', '2021-03-17 07:55:20', NULL),
	(32, 1, 616, 1, 28, 0, '_self', 'MediaPress', 21, 2, NULL, 18, 19, 1, 0, 1, 0, '2021-03-16 14:06:18', '2021-03-16 14:07:19', NULL),
	(33, 1, 616, 1, 30, 0, '_self', 'Folm Universe (Blockchain Project)', 21, 2, NULL, 19, 20, 1, 0, 1, 0, '2021-03-16 14:06:39', '2021-03-16 14:07:19', NULL),
	(34, 1, 616, 1, 32, 0, '_blank', 'Eğitmen.tv (Start-Up)', 21, 2, NULL, 20, 21, 1, 0, 1, 0, '2021-03-16 14:06:57', '2021-03-16 14:07:19', NULL);
/*!40000 ALTER TABLE `menu_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.messages
CREATE TABLE IF NOT EXISTS `messages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `form_id` int unsigned DEFAULT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read` int NOT NULL DEFAULT '0',
  `file` int NOT NULL DEFAULT '0',
  `deleted` int NOT NULL DEFAULT '0',
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` int DEFAULT NULL,
  `cint_3` int DEFAULT NULL,
  `cint_4` int DEFAULT NULL,
  `cint_5` int DEFAULT NULL,
  `cdec_1` decimal(8,2) DEFAULT NULL,
  `cdec_2` decimal(8,2) DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `utm` text COLLATE utf8mb4_unicode_ci,
  `source` text COLLATE utf8mb4_unicode_ci,
  `agent` text COLLATE utf8mb4_unicode_ci,
  `visit_id` int DEFAULT NULL,
  `track_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.messages: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `messages`;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.metas
CREATE TABLE IF NOT EXISTS `metas` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `url_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_id` bigint unsigned NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `indexed` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'index,follow',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `desktop_enabled` tinyint(1) NOT NULL DEFAULT '1',
  `desktop_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `metas_url_type_url_id_index` (`url_type`,`url_id`),
  KEY `key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=181 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.metas: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `metas`;
/*!40000 ALTER TABLE `metas` DISABLE KEYS */;
INSERT INTO `metas` (`id`, `url_type`, `url_id`, `key`, `order`, `type`, `template`, `indexed`, `created_at`, `updated_at`, `deleted_at`, `desktop_enabled`, `desktop_value`) VALUES
	(1, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(2, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(3, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(4, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(5, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(6, 'Mediapress\\Modules\\MPCore\\Models\\Url', 3, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(7, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(8, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(9, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(10, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(11, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(12, 'Mediapress\\Modules\\MPCore\\Models\\Url', 4, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 09:36:16', '2021-03-16 09:40:08', NULL, 1, ''),
	(13, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(14, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(15, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(16, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(17, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(18, 'Mediapress\\Modules\\MPCore\\Models\\Url', 5, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(19, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(20, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(21, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(22, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(23, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(24, 'Mediapress\\Modules\\MPCore\\Models\\Url', 6, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 10:12:15', '2021-03-16 10:25:10', NULL, 1, ''),
	(25, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 10:37:38', '2021-03-16 13:37:34', NULL, 1, 'Özel ve Kurumsal Yazılım Çözümleri | Gen Arge San ve Tic A.Ş.'),
	(26, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 10:37:38', '2021-03-16 13:37:35', NULL, 1, 'Gen Arge, tamamen kendi bünyesinde bulunan yazılım ekibi ile şirketinize katma değerli yazılımlar geliştirmektedir.'),
	(27, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(28, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(29, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(30, 'Mediapress\\Modules\\MPCore\\Models\\Url', 1, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(31, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(32, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(33, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(34, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(35, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(36, 'Mediapress\\Modules\\MPCore\\Models\\Url', 2, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 11:49:47', '2021-03-16 11:49:47', NULL, 1, NULL),
	(37, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(38, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(39, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(40, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(41, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(42, 'Mediapress\\Modules\\MPCore\\Models\\Url', 9, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(43, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(44, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(45, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(46, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(47, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(48, 'Mediapress\\Modules\\MPCore\\Models\\Url', 10, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:23:26', '2021-03-16 12:23:58', NULL, 1, ''),
	(49, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(50, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(51, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(52, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(53, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(54, 'Mediapress\\Modules\\MPCore\\Models\\Url', 11, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(55, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(56, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(57, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(58, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(59, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(60, 'Mediapress\\Modules\\MPCore\\Models\\Url', 12, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL, 1, ''),
	(61, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(62, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(63, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(64, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(65, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(66, 'Mediapress\\Modules\\MPCore\\Models\\Url', 13, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(67, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(68, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(69, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(70, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(71, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(72, 'Mediapress\\Modules\\MPCore\\Models\\Url', 14, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, 1, ''),
	(73, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(74, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(75, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(76, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(77, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(78, 'Mediapress\\Modules\\MPCore\\Models\\Url', 15, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(79, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(80, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(81, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(82, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(83, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(84, 'Mediapress\\Modules\\MPCore\\Models\\Url', 16, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL, 1, ''),
	(85, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(86, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(87, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(88, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(89, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(90, 'Mediapress\\Modules\\MPCore\\Models\\Url', 17, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(91, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(92, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(93, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(94, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(95, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(96, 'Mediapress\\Modules\\MPCore\\Models\\Url', 18, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:27:36', '2021-03-16 13:28:43', NULL, 1, ''),
	(97, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(98, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(99, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(100, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(101, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(102, 'Mediapress\\Modules\\MPCore\\Models\\Url', 19, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(103, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(104, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(105, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(106, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(107, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(108, 'Mediapress\\Modules\\MPCore\\Models\\Url', 20, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:29:03', '2021-03-16 13:30:17', NULL, 1, ''),
	(109, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(110, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(111, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(112, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(113, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(114, 'Mediapress\\Modules\\MPCore\\Models\\Url', 21, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(115, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(116, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(117, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(118, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(119, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(120, 'Mediapress\\Modules\\MPCore\\Models\\Url', 22, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:30:58', '2021-03-16 13:32:14', NULL, 1, ''),
	(121, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(122, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(123, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(124, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(125, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(126, 'Mediapress\\Modules\\MPCore\\Models\\Url', 25, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(127, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(128, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(129, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(130, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(131, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(132, 'Mediapress\\Modules\\MPCore\\Models\\Url', 26, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:52:57', '2021-03-16 13:52:57', NULL, 1, NULL),
	(133, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(134, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(135, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(136, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(137, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(138, 'Mediapress\\Modules\\MPCore\\Models\\Url', 27, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, 1, ''),
	(139, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(140, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(141, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(142, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(143, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(144, 'Mediapress\\Modules\\MPCore\\Models\\Url', 28, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:54:33', '2021-03-16 13:57:17', NULL, 1, ''),
	(145, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(146, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(147, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(148, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(149, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(150, 'Mediapress\\Modules\\MPCore\\Models\\Url', 29, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(151, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(152, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(153, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(154, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(155, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(156, 'Mediapress\\Modules\\MPCore\\Models\\Url', 30, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 13:59:05', '2021-03-16 14:03:10', NULL, 1, ''),
	(157, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(158, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(159, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(160, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(161, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(162, 'Mediapress\\Modules\\MPCore\\Models\\Url', 31, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(163, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(164, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(165, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(166, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(167, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(168, 'Mediapress\\Modules\\MPCore\\Models\\Url', 32, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:03:38', '2021-03-16 14:05:30', NULL, 1, ''),
	(169, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(170, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(171, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(172, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(173, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(174, 'Mediapress\\Modules\\MPCore\\Models\\Url', 33, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(175, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'title', 0, 'description', '<$0>$1</$0>', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(176, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'description', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(177, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'keywords', 0, 'description', '<meta name="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(178, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'og:title', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(179, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'og:description', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, ''),
	(180, 'Mediapress\\Modules\\MPCore\\Models\\Url', 34, 'og:image', 0, 'description', '<meta property="$0" content="$1">', 'index,follow', '2021-03-16 14:27:54', '2021-03-16 14:43:24', NULL, 1, '');
/*!40000 ALTER TABLE `metas` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mfiles
CREATE TABLE IF NOT EXISTS `mfiles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int NOT NULL,
  `mdisk_id` int DEFAULT NULL,
  `mfolder_id` int DEFAULT NULL,
  `mfile_id` int DEFAULT NULL,
  `usecase` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'original',
  `uploadname` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `extension` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `process_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mimetype` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mimeclass` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `size` int NOT NULL DEFAULT '0',
  `tag` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mfiles: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mfiles`;
/*!40000 ALTER TABLE `mfiles` DISABLE KEYS */;
INSERT INTO `mfiles` (`id`, `admin_id`, `mdisk_id`, `mfolder_id`, `mfile_id`, `usecase`, `uploadname`, `fullname`, `filename`, `extension`, `process_id`, `mimetype`, `mimeclass`, `width`, `height`, `size`, `tag`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 1, 2, NULL, 'original', 'logo', 'logo.svg', 'logo', 'svg', '6050792ba36a3', 'image/svg+xml', 'image', NULL, NULL, 2835, NULL, '2021-03-16 09:23:55', '2021-03-16 09:23:55', NULL),
	(2, 1, 1, 2, NULL, 'original', 'favicon', 'favicon.png', 'favicon', 'png', '6050792bb0018', 'image/png', 'image', 256, 256, 9209, NULL, '2021-03-16 09:23:55', '2021-03-16 09:23:55', NULL),
	(3, 1, 1, 2, NULL, 'original', 'slide1', 'slide1.png', 'slide1', 'png', '1615886960864', 'image/png', 'image', 493, 388, 20333, NULL, '2021-03-16 09:29:28', '2021-03-16 09:29:28', NULL),
	(4, 1, 1, 2, NULL, 'original', '5e2166637fc56', '5e2166637fc56.svg', '5e2166637fc56', 'svg', '1615887591216', 'image/svg+xml', 'image', NULL, NULL, 7911, NULL, '2021-03-16 09:39:56', '2021-03-16 09:39:56', NULL),
	(5, 1, 1, 2, NULL, 'original', 'composer', 'composer.svg', 'composer', 'svg', '1615890286252', 'image/svg+xml', 'image', NULL, NULL, 127812, NULL, '2021-03-16 10:24:56', '2021-03-16 10:24:56', NULL),
	(6, 1, 1, 2, NULL, 'original', 'elastic', 'elastic.svg', 'elastic', 'svg', '1615890286252', 'image/svg+xml', 'image', NULL, NULL, 2640, NULL, '2021-03-16 10:24:57', '2021-03-16 10:24:57', NULL),
	(7, 1, 1, 2, NULL, 'original', 'css3', 'css3.svg', 'css3', 'svg', '1615890286252', 'image/svg+xml', 'image', NULL, NULL, 3468, NULL, '2021-03-16 10:25:00', '2021-03-16 10:25:00', NULL),
	(8, 1, 1, 2, NULL, 'original', 'flutter', 'flutter.png', 'flutter', 'png', '1615890986796', 'image/png', 'image', 400, 200, 10977, NULL, '2021-03-16 10:36:35', '2021-03-16 10:36:35', NULL),
	(9, 1, 1, 2, NULL, 'original', 'html5', 'html5.svg', 'html5', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 674, NULL, '2021-03-16 10:36:36', '2021-03-16 10:36:36', NULL),
	(10, 1, 1, 2, NULL, 'original', 'git', 'git.svg', 'git', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 2675, NULL, '2021-03-16 10:36:36', '2021-03-16 10:36:36', NULL),
	(11, 1, 1, 2, NULL, 'original', 'mongodb', 'mongodb.svg', 'mongodb', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 8673, NULL, '2021-03-16 10:36:37', '2021-03-16 10:36:37', NULL),
	(12, 1, 1, 2, NULL, 'original', 'laravel', 'laravel.svg', 'laravel', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 2087, NULL, '2021-03-16 10:36:38', '2021-03-16 10:36:38', NULL),
	(13, 1, 1, 2, NULL, 'original', 'php', 'php.svg', 'php', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 1948, NULL, '2021-03-16 10:36:38', '2021-03-16 10:36:38', NULL),
	(14, 1, 1, 2, NULL, 'original', 'nodejs', 'nodejs.svg', 'nodejs', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 5924, NULL, '2021-03-16 10:36:39', '2021-03-16 10:36:39', NULL),
	(15, 1, 1, 2, NULL, 'original', 'react', 'react.svg', 'react', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 3987, NULL, '2021-03-16 10:36:40', '2021-03-16 10:36:40', NULL),
	(16, 1, 1, 2, NULL, 'original', 'postgresql', 'postgresql.svg', 'postgresql', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 4461, NULL, '2021-03-16 10:36:40', '2021-03-16 10:36:40', NULL),
	(17, 1, 1, 2, NULL, 'original', 'vue', 'vue.svg', 'vue', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 467, NULL, '2021-03-16 10:36:41', '2021-03-16 10:36:41', NULL),
	(18, 1, 1, 2, NULL, 'original', 'redis', 'redis.svg', 'redis', 'svg', '1615890986796', 'image/svg+xml', 'image', NULL, NULL, 6424, NULL, '2021-03-16 10:36:41', '2021-03-16 10:36:41', NULL),
	(19, 1, 1, 2, NULL, 'original', 'neleryapiyoruz1', 'neleryapiyoruz1.svg', 'neleryapiyoruz1', 'svg', '1615897586587', 'image/svg+xml', 'image', NULL, NULL, 43771, NULL, '2021-03-16 12:26:59', '2021-03-16 12:26:59', NULL),
	(20, 1, 1, 2, NULL, 'original', 'neleryapiyoruz2', 'neleryapiyoruz2.svg', 'neleryapiyoruz2', 'svg', '1615897803923', 'image/svg+xml', 'image', NULL, NULL, 18013, NULL, '2021-03-16 12:30:08', '2021-03-16 12:30:08', NULL),
	(21, 1, 1, 2, NULL, 'original', 'neleryapiyoruz3', 'neleryapiyoruz3.svg', 'neleryapiyoruz3', 'svg', '1615900917704', 'image/svg+xml', 'image', NULL, NULL, 17778, NULL, '2021-03-16 13:22:03', '2021-03-16 13:22:03', NULL),
	(22, 1, 1, 2, NULL, 'original', 'neleryapiyoruz4', 'neleryapiyoruz4.svg', 'neleryapiyoruz4', 'svg', '1615901225632', 'image/svg+xml', 'image', NULL, NULL, 23105, NULL, '2021-03-16 13:27:11', '2021-03-16 13:27:11', NULL),
	(23, 1, 1, 2, NULL, 'original', 'neleryapiyoruz5', 'neleryapiyoruz5.svg', 'neleryapiyoruz5', 'svg', '1615901311119', 'image/svg+xml', 'image', NULL, NULL, 8025, NULL, '2021-03-16 13:28:35', '2021-03-16 13:28:35', NULL),
	(24, 1, 1, 2, NULL, 'original', 'neleryapiyoruz6', 'neleryapiyoruz6.svg', 'neleryapiyoruz6', 'svg', '1615901404143', 'image/svg+xml', 'image', NULL, NULL, 9474, NULL, '2021-03-16 13:30:08', '2021-03-16 13:30:08', NULL),
	(25, 1, 1, 2, NULL, 'original', 'neleryapiyoruz7', 'neleryapiyoruz7.svg', 'neleryapiyoruz7', 'svg', '1615901521783', 'image/svg+xml', 'image', NULL, NULL, 20496, NULL, '2021-03-16 13:32:06', '2021-03-16 13:32:06', NULL),
	(26, 1, 1, 2, NULL, 'original', 'projeler1', 'projeler1.svg', 'projeler1', 'svg', '1615903026294', 'image/svg+xml', 'image', NULL, NULL, 265125, NULL, '2021-03-16 13:57:10', '2021-03-16 13:57:10', NULL),
	(27, 1, 1, 2, NULL, 'original', 'projeler2', 'projeler2.svg', 'projeler2', 'svg', '1615903379767', 'image/svg+xml', 'image', NULL, NULL, 20496, NULL, '2021-03-16 14:03:03', '2021-03-16 14:03:03', NULL),
	(28, 1, 1, 2, NULL, 'original', 'projeler3', 'projeler3.svg', 'projeler3', 'svg', '1615903520494', 'image/svg+xml', 'image', NULL, NULL, 9474, NULL, '2021-03-16 14:05:24', '2021-03-16 14:05:24', NULL);
/*!40000 ALTER TABLE `mfiles` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mfile_general
CREATE TABLE IF NOT EXISTS `mfile_general` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mfile_id` int NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` int NOT NULL,
  `file_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordernum` int NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mfile_general: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mfile_general`;
/*!40000 ALTER TABLE `mfile_general` DISABLE KEYS */;
INSERT INTO `mfile_general` (`id`, `mfile_id`, `model_type`, `model_id`, `file_key`, `ordernum`, `details`) VALUES
	(2, 4, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 5, 'cover', 0, NULL),
	(23, 12, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 0, '{"616":{"title":"Laravel","tag":null},"760":{"title":"Laravel","tag":null}}'),
	(24, 9, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 1, '{"616":{"title":"HTML","tag":null},"760":{"title":"HTML","tag":null}}'),
	(25, 7, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 2, '{"616":{"title":"CSS","tag":null},"760":{"title":"CSS","tag":null}}'),
	(26, 13, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 3, '{"616":{"title":"PHP","tag":null},"760":{"title":"PHP","tag":null}}'),
	(27, 6, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 4, '{"616":{"title":"Elastic","caption":null,"tag":null},"760":{"title":"Elastic","caption":null,"tag":null}}'),
	(28, 17, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 5, '{"616":{"title":"Vue","tag":null},"760":{"title":"Vue","tag":null}}'),
	(29, 15, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 6, '{"616":{"title":"React","tag":null},"760":{"title":"React","tag":null}}'),
	(30, 10, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 7, '{"616":{"title":"Git","tag":null},"760":{"title":"Git","tag":null}}'),
	(31, 14, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 8, '{"616":{"title":"Node","tag":null},"760":{"title":"Node","tag":null}}'),
	(32, 18, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 9, '{"616":{"title":"Redis","tag":null},"760":{"title":"Redis","tag":null}}'),
	(33, 11, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 10, '{"616":{"title":"MongoDB","tag":null},"760":{"title":"MongoDB","tag":null}}'),
	(34, 5, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 11, '{"616":{"title":"Composer","caption":null,"tag":null},"760":{"title":"Composer","caption":null,"tag":null}}'),
	(35, 8, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 12, '{"616":{"title":"Flutter","tag":null},"760":{"title":"Flutter","tag":null}}'),
	(36, 16, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 6, 'cover', 13, '{"616":{"title":"PortreSQL","tag":null},"760":{"title":"PortreSQL","tag":null}}'),
	(39, 20, 'Mediapress\\Modules\\Content\\Models\\Page', 2, 'cover', 0, NULL),
	(41, 19, 'Mediapress\\Modules\\Content\\Models\\Page', 1, 'cover', 0, NULL),
	(42, 21, 'Mediapress\\Modules\\Content\\Models\\Page', 3, 'cover', 0, NULL),
	(43, 22, 'Mediapress\\Modules\\Content\\Models\\Page', 4, 'cover', 0, NULL),
	(44, 23, 'Mediapress\\Modules\\Content\\Models\\Page', 5, 'cover', 0, NULL),
	(45, 24, 'Mediapress\\Modules\\Content\\Models\\Page', 6, 'cover', 0, NULL),
	(46, 25, 'Mediapress\\Modules\\Content\\Models\\Page', 7, 'cover', 0, NULL),
	(47, 26, 'Mediapress\\Modules\\Content\\Models\\Page', 9, 'cover', 0, NULL),
	(48, 27, 'Mediapress\\Modules\\Content\\Models\\Page', 10, 'cover', 0, NULL),
	(49, 28, 'Mediapress\\Modules\\Content\\Models\\Page', 11, 'cover', 0, NULL);
/*!40000 ALTER TABLE `mfile_general` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.mfolders
CREATE TABLE IF NOT EXISTS `mfolders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `mdisk_id` int NOT NULL,
  `mfolder_id` int DEFAULT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.mfolders: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `mfolders`;
/*!40000 ALTER TABLE `mfolders` DISABLE KEYS */;
INSERT INTO `mfolders` (`id`, `mdisk_id`, `mfolder_id`, `path`, `created_at`, `updated_at`) VALUES
	(1, 1, NULL, '2021', '2021-03-16 09:23:55', '2021-03-16 09:23:55'),
	(2, 1, 1, '03', '2021-03-16 09:23:55', '2021-03-16 09:23:55');
/*!40000 ALTER TABLE `mfolders` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.migrations: ~95 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_mdisks_table', 1),
	(2, '2014_10_12_000000_create_mfiles_table', 1),
	(3, '2014_10_12_000000_create_mfolders_table', 1),
	(4, '2014_10_12_000000_create_users_table', 1),
	(5, '2014_10_12_000001_update_mfiles_table', 1),
	(6, '2014_10_12_000002_create_mfile_general_table', 1),
	(7, '2014_10_12_000002_update_mfiles_table_add_imaginal_columns', 1),
	(8, '2014_10_12_100000_create_password_resets_table', 1),
	(9, '2016_12_10_132541_create_admins_table', 1),
	(10, '2016_12_10_132541_create_bouncer_table', 1),
	(11, '2016_12_10_132541_create_categories_table', 1),
	(12, '2016_12_10_132541_create_category_criteria_table', 1),
	(13, '2016_12_10_132541_create_category_details_table', 1),
	(14, '2016_12_10_132541_create_category_page_table', 1),
	(15, '2016_12_10_132541_create_category_property_table', 1),
	(16, '2016_12_10_132541_create_criteria_detail_extras_table', 1),
	(17, '2016_12_10_132541_create_criteria_details_table', 1),
	(18, '2016_12_10_132541_create_criteria_extras_table', 1),
	(19, '2016_12_10_132541_create_criteria_page_table', 1),
	(20, '2016_12_10_132541_create_criterias_table', 1),
	(21, '2016_12_10_132541_create_panel_menu_details_table', 1),
	(22, '2016_12_10_132541_create_panel_menus_table', 1),
	(23, '2016_12_10_132541_create_properties_table', 1),
	(24, '2016_12_10_132541_create_property_detail_extras_table', 1),
	(25, '2016_12_10_132541_create_property_details_table', 1),
	(26, '2016_12_10_132541_create_property_extras_table', 1),
	(27, '2016_12_10_132541_create_property_page_table', 1),
	(28, '2016_12_10_132541_create_settings_table', 1),
	(29, '2016_12_10_132541_create_sitemap_details_table', 1),
	(30, '2016_12_10_132541_create_sitemap_extras_table', 1),
	(31, '2016_12_10_132542_update_sitemap_details_table_add_detail_column', 1),
	(32, '2016_12_10_132542_update_sitemap_details_table_add_meta_templates_column', 1),
	(33, '2017_02_01_073816_create_user_activations_table', 1),
	(34, '2017_02_01_073816_create_user_extras_table', 1),
	(35, '2017_02_01_073816_update_users_table', 1),
	(36, '2017_02_09_081705_create_social_media_table', 1),
	(37, '2017_02_10_080536_create_e_bulletin_users_table', 1),
	(38, '2017_04_24_152436_create_menu_details_table', 1),
	(39, '2017_05_03_143713_create_messages_table', 1),
	(40, '2017_05_03_143713_update_messages_table', 1),
	(41, '2017_08_03_135201_create_forms_table', 1),
	(42, '2018_02_27_141521_create_scripts_table', 1),
	(43, '2018_07_10_082611_create_maillists_table', 1),
	(44, '2018_07_10_083258_create_mail_templates_table', 1),
	(45, '2018_07_11_083820_create_module_configs_table', 1),
	(46, '2018_07_11_110851_create_modules_table', 1),
	(47, '2018_07_17_082549_create_email_sender_table', 1),
	(48, '2018_07_25_100046_create_form_details_table', 1),
	(49, '2018_07_27_100739_create_entities_table', 1),
	(50, '2018_07_27_123358_create_entitylists_table', 1),
	(51, '2018_07_27_123358_create_userlists_table', 1),
	(52, '2018_08_09_123218_create_setting_sun_table', 1),
	(53, '2018_08_13_132541_create_urls_table', 1),
	(54, '2018_08_13_132542_update_urls_table_drop_url_unique', 1),
	(55, '2018_08_29_065552_create_entitylist_pivot_table', 1),
	(56, '2018_08_29_065552_create_userlist_pivot_table', 1),
	(57, '2018_08_29_082056_update_admins_add_api_key_table', 1),
	(58, '2018_08_31_132541_create_language_website_table', 1),
	(59, '2018_08_31_132541_create_metas_table', 1),
	(60, '2018_08_31_132541_create_sitemaps_table', 1),
	(61, '2018_08_31_132541_create_websites_table', 1),
	(62, '2018_08_31_132542_create_website_details_table', 1),
	(63, '2018_08_31_132542_create_website_extras_table', 1),
	(64, '2018_08_31_132542_update_metas_table_divide_mobile_values', 1),
	(65, '2018_08_31_132543_create_website_detail_extras_table', 1),
	(66, '2018_09_07_112218_create_menus_table', 1),
	(67, '2018_09_14_102759_create_useraction_logs_table', 1),
	(68, '2018_10_10_121435_create_sitemap_details_extras_table', 1),
	(69, '2018_10_11_062141_create_extras_table', 1),
	(70, '2018_10_11_062141_create_page_details_table', 1),
	(71, '2018_10_11_062141_create_page_extras_table', 1),
	(72, '2018_10_11_062141_create_page_property_table', 1),
	(73, '2018_10_11_062529_create_page_detail_extras_table', 1),
	(74, '2018_10_11_062626_create_category_extras_table', 1),
	(75, '2018_10_11_062721_create_category_detail_extras_table', 1),
	(76, '2018_10_30_075720_create_pages_table', 1),
	(77, '2018_11_13_074500_create_countries_table', 1),
	(78, '2018_11_13_074500_create_country_groups_table', 1),
	(79, '2018_11_13_122424_create_country_group_country_table', 1),
	(80, '2018_11_13_122424_create_country_group_language_table', 1),
	(81, '2018_11_23_145126_create_slots_table', 1),
	(82, '2018_11_29_099999_create_sitemap_website_table', 1),
	(83, '2018_11_29_105745_create_sitemap_types_table', 1),
	(84, '2019_01_014_113615_create_language_parts_table', 1),
	(85, '2019_01_014_113615_create_languages_table', 1),
	(86, '2019_01_29_063538_create_jobs_table', 1),
	(87, '2019_03_28_000000_update_all_details_tables_add_manual_meta_column', 1),
	(88, '2019_03_28_000000_update_urlable_content_tables_add_protection_columns', 1),
	(89, '2019_03_29_099999_create_sliders_table', 1),
	(90, '2019_04_18_115909_update_urls_table_remove_password_column', 1),
	(91, '2019_04_18_132542_update_sliders_table', 1),
	(92, '2019_05_02_185930_create_sitemap_xmls_table', 1),
	(93, '2019_05_02_185931_create_sitemap_xml_blocks_table', 1),
	(94, '2019_05_02_185932_update_sitemap_xmls_table_add_renew_freq_column', 1),
	(95, '2019_05_02_185933_update_sitemap_xml_blocks_table_add_cf_columns', 1),
	(96, '2019_07_23_063538_create_search_content_view', 1),
	(97, '2019_08_19_000000_create_failed_jobs_table', 1),
	(98, '2019_08_20_115909_update_mfile_general_add_detail', 1),
	(99, '2019_10_01_123456_create_popups_table', 1),
	(100, '2019_10_01_132541_create_language_popup_table', 1),
	(101, '2019_10_01_132542_create_popup_urls_table', 1),
	(102, '2019_10_15_062141_update_scene_details_table', 1),
	(103, '2019_10_15_132542_update_menu_details_table', 1),
	(104, '2019_10_25_105500_update_sitemaps_table_add_sitemap_id', 1),
	(105, '2019_11_07_102046_add_website_id_to__sitemap_types', 1),
	(106, '2019_12_10_000000_create_instagram_albums_table', 1),
	(107, '2019_12_10_000000_create_instagram_post_album_table', 1),
	(108, '2019_12_10_000000_create_instagram_posts_table', 1),
	(109, '2019_12_15_132542_update_popup_languages_table', 1),
	(110, '2020_01_24_121805_add_file_id_column_to_menu_details_table', 1),
	(111, '2020_05_20_115909_update_mfiles_v4', 1),
	(112, '2020_06_10_132542_update_popup_table', 1),
	(113, '2020_07_03_115909_update_mfiles_add_tag', 1),
	(114, '2020_07_27_132542_update_popup_border_radius_table', 1),
	(115, '2020_07_28_132542_update_useraction_logs_ip_table', 1),
	(116, '2020_08_31_132542_update_scene_detail_video_url_table', 1),
	(117, '2020_10_13_000001_update_admin_google2fa_table', 1),
	(118, '2020_10_17_000001_update_sitemap_reserved_url_table', 1),
	(119, '2020_10_19_000001_update_sitemap_url_status_table', 1),
	(120, '2020_10_21_132542_update_meta_drop_mobile_table', 1),
	(121, '2020_10_26_000000_update_social_media_table_update_icon_column_type', 1),
	(122, '2020_11_04_000001_create_panel_language_parts_table', 1),
	(123, '2020_11_06_000001_update_admin_google2fa_warning_table', 1),
	(124, '2020_11_10_000001_create_seo_keywords_table', 1),
	(125, '2020_11_18_000001_create_login_attempts_table', 1),
	(126, '2020_11_19_000001_create_seo_contents_table', 1),
	(127, '2020_11_25_000001_update_popup_curtain_table', 1),
	(128, '2020_11_27_000001_update_forms_send_user_table', 1),
	(129, '2020_11_30_000001_update_country_groups_list_title_table', 1),
	(130, '2020_12_16_000001_update_popup_content_image_table', 1),
	(131, '2020_12_16_000002_create_popup_custom_urls_table', 1),
	(132, '2020_12_24_000001_update_popup_custom_css_table', 1),
	(133, '2020_99_99_063517_create_mp_failed_jobs_table', 1),
	(134, '2021_01_04_000001_create_comment_structures_table', 1),
	(135, '2021_01_04_000002_create_comments_table', 1),
	(136, '2021_01_05_000001_update_sitemap_auto_order_table', 1),
	(137, '2021_01_13_000001_update_sitemap_details_search_text_table', 1),
	(138, '2021_01_13_000002_update_page_details_search_text_table', 1),
	(139, '2021_01_13_000003_update_category_details_search_text_table', 1),
	(140, '2021_02_26_000001_update_sitemap_sub_page_order_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.modules
CREATE TABLE IF NOT EXISTS `modules` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `namespace` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slot_id` int DEFAULT NULL,
  `module_id` int DEFAULT NULL,
  `status` tinyint NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.modules: ~12 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `modules`;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` (`id`, `name`, `namespace`, `slot_id`, `module_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'Auth', 'Mediapress\\Modules\\Auth', NULL, NULL, 1, NULL, NULL, NULL),
	(2, 'Comment', 'Mediapress\\Modules\\Comment', NULL, NULL, 1, NULL, NULL, NULL),
	(3, 'Content', 'Mediapress\\Modules\\Content', NULL, NULL, 1, NULL, NULL, NULL),
	(4, 'Entity', 'Mediapress\\Modules\\Entity', NULL, NULL, 1, NULL, NULL, NULL),
	(5, 'Flow', 'Mediapress\\Modules\\Flow', NULL, NULL, 1, NULL, NULL, NULL),
	(6, 'GoogleTagManager', 'Mediapress\\Modules\\GoogleTagManager', NULL, NULL, 1, NULL, NULL, NULL),
	(7, 'Heraldist', 'Mediapress\\Modules\\Heraldist', NULL, NULL, 1, NULL, NULL, NULL),
	(8, 'Interaction', 'Mediapress\\Modules\\Interaction', NULL, NULL, 1, NULL, NULL, NULL),
	(9, 'Locale', 'Mediapress\\Modules\\Locale', NULL, NULL, 1, NULL, NULL, NULL),
	(10, 'MPCore', 'Mediapress\\Modules\\MPCore', NULL, NULL, 1, NULL, NULL, NULL),
	(11, 'RenderableEditor', 'Mediapress\\Modules\\RenderableEditor', NULL, NULL, 1, NULL, NULL, NULL),
	(12, 'Setting', 'Mediapress\\Modules\\Setting', NULL, NULL, 1, NULL, NULL, NULL);
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.module_configs
CREATE TABLE IF NOT EXISTS `module_configs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `module_id` int NOT NULL,
  `config_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.module_configs: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `module_configs`;
/*!40000 ALTER TABLE `module_configs` DISABLE KEYS */;
/*!40000 ALTER TABLE `module_configs` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.pages
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int unsigned NOT NULL,
  `page_id` int unsigned NOT NULL,
  `admin_id` int unsigned NOT NULL,
  `fb_template` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'AllBuilder template differer key',
  `order` int unsigned NOT NULL,
  `date` date DEFAULT NULL,
  `status` tinyint unsigned NOT NULL DEFAULT '3',
  `published_at` timestamp NOT NULL,
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `cdec_1` decimal(20,4) DEFAULT NULL,
  `cdec_2` decimal(20,4) DEFAULT NULL,
  `cdec_3` decimal(20,4) DEFAULT NULL,
  `cdec_4` decimal(20,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allowed_role_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.pages: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `pages`;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` (`id`, `sitemap_id`, `page_id`, `admin_id`, `fb_template`, `order`, `date`, `status`, `published_at`, `ctex_1`, `ctex_2`, `cvar_1`, `cvar_2`, `cvar_3`, `cvar_4`, `cvar_5`, `cint_1`, `cint_2`, `cint_3`, `cint_4`, `cint_5`, `cdat_1`, `cdat_2`, `cdec_1`, `cdec_2`, `cdec_3`, `cdec_4`, `created_at`, `updated_at`, `deleted_at`, `password`, `allowed_role_id`) VALUES
	(1, 7, 0, 1, NULL, 1, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:23:25', '2021-03-16 12:44:19', NULL, '', 0),
	(2, 7, 0, 1, NULL, 2, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:28:28', '2021-03-16 12:44:19', NULL, '', 0),
	(3, 7, 0, 1, NULL, 3, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL, '', 0),
	(4, 7, 0, 1, NULL, 4, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:22:33', '2021-03-16 13:27:21', NULL, '', 0),
	(5, 7, 0, 1, NULL, 5, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:27:35', '2021-03-16 13:28:43', NULL, '', 0),
	(6, 7, 0, 1, NULL, 6, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:29:02', '2021-03-16 13:30:16', NULL, '', 0),
	(7, 7, 0, 1, NULL, 7, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:30:57', '2021-03-16 13:32:14', NULL, '', 0),
	(9, 9, 0, 1, NULL, 1, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL, '', 0),
	(10, 9, 0, 1, NULL, 2, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:59:04', '2021-03-16 14:03:10', NULL, '', 0),
	(11, 9, 0, 1, NULL, 3, NULL, 1, '0000-00-00 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:03:37', '2021-03-16 14:05:30', NULL, '', 0);
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.page_details
CREATE TABLE IF NOT EXISTS `page_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int NOT NULL,
  `language_id` int unsigned DEFAULT NULL,
  `country_group_id` int unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `search_text` text COLLATE utf8mb4_unicode_ci,
  `status` int NOT NULL DEFAULT '3',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `manual_meta` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `page_details_page_id_index` (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.page_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `page_details`;
/*!40000 ALTER TABLE `page_details` DISABLE KEYS */;
INSERT INTO `page_details` (`id`, `page_id`, `language_id`, `country_group_id`, `name`, `slug`, `detail`, `search_text`, `status`, `deleted_at`, `manual_meta`) VALUES
	(1, 1, 760, 1, 'Özel Web Tabanlı Yazılım Geliştirme', '/ozel-web-tabanli-yazilim-gelistirme', '<p>Gen Arge ekibi, tamamen web tabanlı yazılımlar üzerine uzman olduğu için, bu konudaki en güncel teknolojileri takip eder. Proje yönetimi, versiyon yönetimi, sunucu yönetimi ve deployment süreçlerini uçtan uca yönetir. Böylece anahtar teslim ve sorunsuz bir projenin keyfini sürmek size kalır.&nbsp;</p>\r\n\r\n<p>Yazılım projelerinde kullandığımız teknolojileri görmek için <a href="/tr/odak-teknolojiler">tıklayınız</a></p>\r\n', '', 3, NULL, 0),
	(2, 1, 616, 1, 'Custom Web Based Software Development', '/custom-web-based-software-development', '<p>Since the Gen Arge team is fully expert on web-based software, it follows the latest technologies in this field. Manages project management, version management, server management and deployment processes from end to end. Thus, it is up to you to enjoy a turnkey and trouble-free project.</p>\r\n\r\n<p><a href="/en/focus-technologies">Click</a> to see the technologies we use in software projects.</p>\r\n', '', 3, NULL, 0),
	(3, 2, 760, 1, 'Web Tasarım Projeleri', '/web-tasarim-projeleri', '<p>Gen Arge, web tasarım projelerinde çözüm ortakları ile birlikte, görsel olarak dikkat çekici, kullanıcı deneyimi odaklı ve SEO uyumlu projeleri hayata geçirir. Tüm projelerinde MediaPress CMS nin gücünü kullanarak, güncellenebilir ve güvenli web siteleri üretir.</p>\r\n', '', 3, NULL, 0),
	(4, 2, 616, 1, 'Web Design Projects', '/web-design-projects', '<p>Gen Arge, together with its solution partners in web design projects, realizes visually striking, user experience-oriented and SEO compatible projects. It produces updatable and secure websites by using the power of MediaPress CMS in all its projects.</p>\r\n', '', 3, NULL, 0),
	(5, 3, 760, 1, 'Start-Up Proje Danışmanlığı', '/start-up-proje-danismanligi', '<p>Gen Arge, gerek kuruluşundan sonra ve gerekse kurucularının öncesinde edinmiş olduğu tecrübeler ile bir çok sturt-up projenin analizi, yazılım altyapısının kurulması ve iş planlarının hazırlanması süreçlerinde aktif olarak rol almıştır. Bu sebeple de, bu konularda ciddi bir bilgi birikimi ve deneyime sahiptir.</p>\r\n', '', 3, NULL, 0),
	(6, 3, 616, 1, 'Start-Up Project Consultancy', '/start-up-project-consultancy', '<p>The Gene R &amp; A has active in the process of the installation of the software infrastructure and the preparation of the software infrastructure, with the experiences of the R &amp; A prior to the establishment and the founders before the establishment of and before their founders. Therefore, there is a serious knowledge and experience in these issues.</p>\r\n', '', 3, NULL, 0),
	(7, 4, 760, 1, 'Sunucu Bakımı ve Güvenliği', '/sunucu-bakimi-ve-guvenligi', '<p>Gen Arge, ekibi Linux işletim sistemine sahip sunucuların kurulum, bakım ve güvenliği konusuna uzman bir ekibe ve bilgiye sahiptir. Gen Arge ekibi sunucu bakımı ve güvenliği konularında aşağıdaki hizmetleri sunmaktadır.</p>\r\n\r\n<h2>Fiziksel Sunucu Bakımı</h2>\r\n\r\n<p>Firmware - BIOS - RAID - KVM Güncelleme</p>\r\n\r\n<h2>Sanallaştırma</h2>\r\n\r\n<p>VMWare vCloud - Hyper-V, SolusVM, Docker vb. sistemlerin devreye alınması, yapılandırılması ve yönetimi</p>\r\n\r\n<h2>Yedekleme</h2>\r\n\r\n<p>Veeam Backup - Shadow Copy - Clone - Cloud Backup - FTP Backup vb. yedekleme yöntemlerinin sisteme uyarlanması, devreye alınması, yapılandırılması ve yönetimi</p>\r\n\r\n<h2>Sunucu Optimizasyonu</h2>\r\n\r\n<p>Apache-Nginx-Litespeed, MySQL - MariaDB, cPanel - Plesk - CWP - VestaCP - CyberPanel vb. sistemlerin kurulumu, yapılandırılması, yönetimi ve optimizasyonu Exploit Taraması ve Port Güvenliği</p>\r\n\r\n<h2>Amazon Web Service - Google Cloud - Azure - Cloud Sunucu Yönetimi Ve Yapılandırma Hizmetleri</h2>\r\n\r\n<p>AWS hizmetlerinin kurulumu, yapılandırılması ve devreye alınması<br />\r\nGoogle Cloud hizmetlerinin kurulumu, yapılandırılması ve devreye alınması<br />\r\nCloud Sunucu Yönetimleri (Digitalocean, Vultr, Linode, Heroku vb)<br />\r\nMicrosoft Azure hizmetlerinin kurulumu, yapılandırılması ve devreye alınması</p>\r\n\r\n<h2>E-Posta Sunucu Ve Kurumsal E-Posta Çözümleri</h2>\r\n\r\n<p>VMWare vCloud - Hyper-V, SolusVM, Docker vb. sistemlerin devreye alınması, yapılandırılması ve yönetimi</p>\r\n\r\n<h2>DNS Yönetimi Ve Yapılandırmaları</h2>\r\n\r\n<p>PowerDNS, BindDNS, MyDNS vb. dns sunucularının kurulumu, yapılandırmaları ve yönetimi<br />\r\nDNS Cluster<br />\r\nCloudflare DNS Yönetimi</p>\r\n\r\n<h2>Database Yönetimi Ve Yapılandırmaları</h2>\r\n\r\n<p>AWS hizmetlerinin kurulumu, yapılandırılması ve devreye alınması<br />\r\nGoogle Cloud hizmetlerinin kurulumu, yapılandırılması ve devreye alınması<br />\r\nCloud Sunucu Yönetimleri (Digitalocean, Vultr, Linode, Heroku vb)<br />\r\nMicrosoft Azure hizmetlerinin kurulumu, yapılandırılması ve devreye alınması</p>\r\n\r\n<h2>Linux Sunucu Yönetimi Ve Yapılandırmaları ( Tüm Linux Sürümleri)</h2>\r\n\r\n<p>Web Server, DB Server, DNS Server, Mail Server, Backup Server vb. olarak yapılandırma<br />\r\nLog Yönetimi ile sunucu bazı hataların tespiti ve giderilmesi<br />\r\nPort yönetimi ile sunucu bazlı güvenlik önlemleri<br />\r\nPaket Güncellemeleri ile sunucu güncelleştirmeleri ve güvenlik önlemleri</p>\r\n\r\n<h2>Windows Sunucu Yönetimi Ve Yapılandırmaları (Tüm Windows Server Sümleri)</h2>\r\n\r\n<p>Web Server, DB Server, DNS Server, Mail Server, Backup Server vb. olarak yapılandırma<br />\r\nActive Directoy kurulumu, yapılandırması ve yönetimi<br />\r\nPort ve Paket yönetimi ile sunucu bazlı güvenlik önlemleri</p>\r\n\r\n<h2>Hardware Firewall, Web Application Firewall, Software Firewall Olarak Güvenlik Hizmetleri</h2>\r\n\r\n<p>Fortigate, Watchguard, Juniper, Citrix (Load Balancer) Firewall cihazlarının kurulumu, yapılandırmaları veyönetimi<br />\r\nFortiweb, AWS, Cloudflare, Baracuda vb. bir çok WAF sistemlerinin kurulumu yapılandırmaları ve yönetimi</p>\r\n', '', 3, NULL, 0),
	(8, 4, 616, 1, 'Server Maintenance and Security', '/server-maintenance-and-security', '<p>Gen Arge, ekibi Linux işletim sistemine sahip sunucuların kurulum, bakım ve güvenliği konusuna uzman bir ekibe ve bilgiye sahiptir. Gen Arge ekibi sunucu bakımı ve güvenliği konularında aşağıdaki hizmetleri sunmaktadır.</p>\r\n\r\n<h2>Fiziksel Sunucu Bakımı</h2>\r\n\r\n<p>Firmware - BIOS - RAID - KVM Güncelleme</p>\r\n\r\n<h2>Sanallaştırma</h2>\r\n\r\n<p>VMWare vCloud - Hyper-V, SolusVM, Docker vb. sistemlerin devreye alınması, yapılandırılması ve yönetimi</p>\r\n\r\n<h2>Yedekleme</h2>\r\n\r\n<p>Veeam Backup - Shadow Copy - Clone - Cloud Backup - FTP Backup vb. yedekleme yöntemlerinin sisteme uyarlanması, devreye alınması, yapılandırılması ve yönetimi</p>\r\n\r\n<h2>Sunucu Optimizasyonu</h2>\r\n\r\n<p>Apache-Nginx-Litespeed, MySQL - MariaDB, cPanel - Plesk - CWP - VestaCP - CyberPanel vb. sistemlerin kurulumu, yapılandırılması, yönetimi ve optimizasyonu Exploit Taraması ve Port Güvenliği</p>\r\n\r\n<h2>Amazon Web Service - Google Cloud - Azure - Cloud Server Management and Configuration Services</h2>\r\n\r\n<p>Installing, configuring and deploying AWS services<br />\r\nInstallation, configuration and commissioning of Google Cloud services<br />\r\nCloud Server Management (Digitalocean, Vultr, Linode, Heroku etc.)<br />\r\nInstallation, configuration and commissioning of Microsoft Azure services</p>\r\n\r\n<h2>E-Mail Server and Corporate E-Mail Solutions</h2>\r\n\r\n<p>VMWare vCloud - Hyper-V, SolusVM, Docker etc. system commissioning, configuration and management</p>\r\n\r\n<h2>DNS Management and Configurations</h2>\r\n\r\n<p>PowerDNS, BindDNS, MyDNS etc. setup, configuration and management of dns servers<br />\r\nDNS Cluster</p>\r\n\r\n<p>Cloudflare DNS Management</p>\r\n\r\n<h2>Database Management and Configurations</h2>\r\n\r\n<p>Installing, configuring and deploying AWS services<br />\r\nInstallation, configuration and commissioning of Google Cloud services<br />\r\nCloud Server Management (Digitalocean, Vultr, Linode, Heroku etc.)I</p>\r\n\r\n<p>nstallation, configuration and commissioning of Microsoft Azure services</p>\r\n\r\n<h2>Linux Server Management And Configurations (All Linux Versions)</h2>\r\n\r\n<p>Web Server, DB Server, DNS Server, Mail Server, Backup Server etc. configuration as<br />\r\nDetection and removal of some server errors with Log Management<br />\r\nServer-based security measures with port management</p>\r\n\r\n<p>Server updates and security measures with Package Updates</p>\r\n\r\n<h2>Windows Server Management And Configurations (All Windows Server Versions)</h2>\r\n\r\n<p>Web Server, DB Server, DNS Server, Mail Server, Backup Server etc. configuration as<br />\r\nActive Directoy setup, configuration and management</p>\r\n\r\n<p>Server-based security measures with Port and Package management</p>\r\n\r\n<h2>Security Services as Hardware Firewall, Web Application Firewall, Software Firewall</h2>\r\n\r\n<p>Installation, configuration and management of Fortigate, Watchguard, Juniper, Citrix (Load Balancer)</p>\r\n\r\n<p>Firewall devicesFortiweb, AWS, Cloudflare, Baracuda etc. Installation, configuration and management of many WAF systems</p>\r\n', '', 3, NULL, 0),
	(9, 5, 760, 1, 'CRM Yazılımları', '/crm-yazilimlari', '<p>Gen Arge, şirket içerisinde kullanmak üzere geliştirdiği CRM sistemi ve CRM teknolojileri üzerine yaptığı ARGE faaliyetleri sebebi ile CRM konusunda uzun yıllara dayanan bir deneyime sahiptir. Bu sebeple, web tabanlı bulut CRM çözümleri konusuda hizmet verebilmektedir.</p>\r\n', '', 3, NULL, 0),
	(10, 5, 616, 1, 'CRM Software', '/crm-software', '<p>Gen Arge has many years of experience in CRM, due to its R&amp;D activities on the CRM system and CRM technologies it has developed for use within the company. For this reason, it can provide services in web-based cloud CRM solutions.</p>\r\n', '', 3, NULL, 0),
	(11, 6, 760, 1, 'İş Süreci Yönetim Yazılımları', '/is-sureci-yonetim-yazilimlari', '<p>Gen Arge, şirket içerisinde kullanmak üzere geliştirdiği iş süreci yönetim yazılımları sayesinde, proje yönetimi, ön muhasebe, süreç yönetimi gibi süreçler ve bu süreçlere ait yönetim yazılımlarının geliştirilmesi konusunda tecrübe sahibi olmuştur. Bu sebeple iş süreçlerinin analiz edilmesi ve verimlilik artışı sağlayacak yazılımların geliştirilmesi konusunda tecrübe sahibidir.</p>\r\n', '', 3, NULL, 0),
	(12, 6, 616, 1, 'Business Process Management Software', '/business-process-management-software', '<p>Thanks to the business process management software developed for use within the company, Gen Arge has gained experience in the development of processes such as project management, pre-accounting, process management and management software for these processes. For this reason, he has experience in analyzing business processes and developing software that will increase productivity.</p>\r\n', '', 3, NULL, 0),
	(13, 7, 760, 1, 'Blockchain Teknolojileri', '/blockchain-teknolojileri', '<p>Blockchain, geleceği değiştirecek teknolojilerden biridir. Blockchain, yani blok zinciri, isminden de anlaşılacağı üzere, içinde verilerin olduğu blokların daha sonra değiştirelemeyecek, şekilde birbirine bağlanarak oluşturdukları veri bloklarıdır. Bu bloklardan oluşan verinin değiştirilememesi ve de bir çok farklı noktadan doğrulanarak, doğruluğunun konsensüs ile kesinleşmesi özellikleri ile klasik veri yığınlarından farklılık gösterir.</p>\r\n\r\n<p>Blockchain hayatımıza kripto paralarla girmiş olsa da, kullanım olarak farklı bir çok sektörde kullanılma potansiyeli vardır ve bu konuda bir çok çalışma mevcuttur.</p>\r\n\r\n<p>Gen Arge ekibi, kripto paralar ve blockchain teknolojileri üzerine, ARGE çalışmaları yapmaktadır. 2017 sonlarında başlattığı bitcore üzerinde çalışan kripto para projesi Folm Universe projesini halen devam ettirmektedir.</p>\r\n', '', 3, NULL, 0),
	(14, 7, 616, 1, 'Blockchain Technologies', '/blockchain-technologies', '<p>Blockchain is one of the technologies that will change the future. Blockchain, as the name suggests, is a block of data that blocks with data are linked together in such a way that they cannot be changed later. It differs from classical data stacks with the features that the data consisting of these blocks cannot be changed and that it is verified from many different points and its accuracy is confirmed by consensus.</p>\r\n\r\n<p>Although blockchain has entered our lives with cryptocurrencies, it has the potential to be used in many different sectors and there are many studies on this subject.</p>\r\n\r\n<p>Gen Arge team conducts R&amp;D studies on cryptocurrencies and blockchain technologies. The crypto money project Folm Universe, which started on bitcore, started in late 2017, is still continuing.</p>\r\n', '', 3, NULL, 0),
	(17, 9, 760, 1, 'MediaPress', '/mediapress', '<p>MediaPress, başta MediaClick için geliştirilen, güçlü bir CMS (içerik yönetim yazılımı) dir. Yaptığı işi karmaşık kurgulara sahip ve büyük veri setlerine sahip web sitelerinin içeriklerini yönetmek olarak özetleyebiliriz.</p>\r\n\r\n<p>Gen ARGE ekibi olarak, MediaPress’i tasarlarken felsefemiz şu oldu :</p>\r\n\r\n<p>Hali hazırda kullanılan bir çok CMS, ön yüz kodları ile bütünleşiktir ve tasarımda çeşitli kısıtlar getirmektedir. MediaPress, web projelerinde her türlü tasarıma entegre olabilmeli ve tasarım kısıtı getirmemelidir.<br />\r\nMediaPress, modüler yapıda olmalıdır ve kolayca yeni modüller eklenebilmelidir.<br />\r\nMediaPress, açık kaynak kodlu ve müşteri tarafından geliştirilebilir olmalı, gerek hosting gerekse geliştirme yönü ile geliştiriciye bağımlı olmamalıdır.<br />\r\nMediaPress, endüstri standardı olarak kabul görmüş standart özellikleri modül olarak sunarak, geliştirme süreçlerini kısaltmalı ve hataları azaltmalıdır.</p>\r\n', '', 3, NULL, 0),
	(18, 9, 616, 1, 'MediaPress', '/mediapress', '<p>MediaPress is a strong CMS (content management software) developed for MediaClick. We can summarize the business as the complex fiction and managing the content of websites with large data sets.</p>\r\n\r\n<p>As a Gene R &amp; D team, our philosophy was following MediaPress:</p>\r\n\r\n<p>A very commonly used CMS is integrated with front-face codes and brings a variety of restrictions in the design. MediaPress should be integrated into all kinds of design in web projects and not to bring the design constraint.<br />\r\nMediaPress must be modular structure and can be easily added to new modules.<br />\r\nMediaPress must be open source and be developed by the customer, it should not be dependent on the developer with both hosting and development direction.<br />\r\nMediaPress should abbreviate the development processes and reduce the development processes by offering the standard features of industry as a module.</p>\r\n', '', 3, NULL, 0),
	(19, 10, 760, 1, 'Folm Universe (Blockchain Project)', '/folm-universe-blockchain-project', '<p>Folm, ödeme, depolama ve servis sözleşmelerini içeren dört katmanlı bir kripto paradır.</p>\r\n\r\n<p>Bu katmanlar şunlardır :</p>\r\n\r\n<h2>Folm Coin</h2>\r\n\r\n<p>Folm, blockchain üzerine kurulu açık kaynaklı, merkezi olmayan bir kripto para birimidir.</p>\r\n\r\n<h2>Folm Services</h2>\r\n\r\n<p>FolmServices, gelecekte geliştirilecek olanlar da dahil olmak üzere tüm FolmServices için bir altyapı çerçevesi olmayı amaçlamaktadır.</p>\r\n\r\n<h2>FolmBank</h2>\r\n\r\n<p>FolmBank, gelecekte geliştirilecek olanlar da dahil olmak üzere tüm FolmServices için bir altyapı çerçevesidir.</p>\r\n\r\n<h2>FolmApps</h2>\r\n\r\n<p>Folm servisleri ve Folmcoin için desktop ve mobil araçlar setidir.</p>\r\n\r\n<p>Daha fazla detay için, <a href="http://folm.io" target="_blank">folm.io</a> adresini ziyaret ediniz.</p>\r\n', '', 3, NULL, 0),
	(20, 10, 616, 1, 'Folm Universe (Blockchain Project)', '/folm-universe-blockchain-project', '<p>Folm is a four-tier cryptocurrency that includes payment, storage, and service contracts.</p>\r\n\r\n<p>These layers are:</p>\r\n\r\n<h2>Folm Coin</h2>\r\n\r\n<p>Folm is an open source, decentralized cryptocurrency built on blockchain.</p>\r\n\r\n<h2>Folm Services</h2>\r\n\r\n<p>FolmServices aims to be an infrastructure framework for all FolmServices, including future development.</p>\r\n\r\n<h2>FolmBank</h2>\r\n\r\n<p>FolmBank is an infrastructure framework for all FolmServices, including those to be developed in the future.</p>\r\n\r\n<h2>FolmApps</h2>\r\n\r\n<p>Folm servisleri ve Folmcoin için desktop ve mobil araçlar setidir.</p>\r\n\r\n<p>Daha fazla detay için, <a href="http://folm.io" target="_blank">folm.io</a> adresini ziyaret ediniz.</p>\r\n', '', 3, NULL, 0),
	(21, 11, 760, 1, 'Eğitmen.tv (Start-Up)', '/egitmentv-start-up', '<p>Eğitmen.TV, öğrenmek istediğiniz her konudaki bilgiyi, bizzat o konunun uzmanından birebir ve canlı olarak öğrenebileceğiniz bir eğitim platformudur. Eğitmen.TV, bu platformda sizi konunun eğitmeni ile buluşturur, güvenli bir ödeme döngüsünü işletir ve kendi online eğitim altyapısında eğitimin gerçekleşmesini sağlar.</p>\r\n\r\n<p>Projeyi incelemek için <a href="https://www.egitmen.tv/" target="_blank">www.egitmen.tv</a> yi ziyaret ediniz.</p>\r\n', '', 3, NULL, 0),
	(22, 11, 616, 1, 'Eğitmen.tv (Start-Up)', '/egitmentv-start-up', '<p>Trainer.TV is an educational platform that you can learn from all the topic you want to learn, by that subject\'s expert. Trainer.TV brings you on this platform with the instructor of the subject, operates a secure payment cycle and allows training in their online educational infrastructure.</p>\r\n\r\n<p>Visit <a href="https://www.egitmen.tv/" target="_blank">www.egitmen.tv</a> to review the project.</p>\r\n', '', 3, NULL, 0);
/*!40000 ALTER TABLE `page_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.page_detail_extras
CREATE TABLE IF NOT EXISTS `page_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int NOT NULL,
  `page_detail_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_detail_extras_page_id_index` (`page_id`),
  KEY `page_detail_extras_page_detail_id_index` (`page_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.page_detail_extras: ~37 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `page_detail_extras`;
/*!40000 ALTER TABLE `page_detail_extras` DISABLE KEYS */;
INSERT INTO `page_detail_extras` (`id`, `page_id`, `page_detail_id`, `key`, `value`, `created_at`, `updated_at`) VALUES
	(11, 0, 3, 'slogan1', 'Esnek ve Ölçeklenebilir', '2021-03-16 12:30:20', '2021-03-16 12:30:20'),
	(12, 0, 3, 'summary', 'CMS Çözümleri', '2021-03-16 12:30:20', '2021-03-16 12:30:20'),
	(13, 0, 4, 'slogan1', 'Flexible and Scalable', '2021-03-16 12:30:20', '2021-03-16 12:30:20'),
	(14, 0, 4, 'summary', 'CMS Solutions', '2021-03-16 12:30:20', '2021-03-16 12:30:20'),
	(19, 0, 1, 'slogan1', 'Siz Hayal Edin', '2021-03-16 12:44:19', '2021-03-16 12:44:19'),
	(20, 0, 1, 'summary', 'Biz Kodlayalım', '2021-03-16 12:44:19', '2021-03-16 12:44:19'),
	(21, 0, 2, 'slogan1', 'You Imagine', '2021-03-16 12:44:19', '2021-03-16 12:44:19'),
	(22, 0, 2, 'summary', 'We Code', '2021-03-16 12:44:19', '2021-03-16 12:44:19'),
	(23, 0, 5, 'slogan1', 'Bir Fikriniz Varsa', '2021-03-16 13:22:18', '2021-03-16 13:22:18'),
	(24, 0, 5, 'summary', 'Biz Buradayız', '2021-03-16 13:22:18', '2021-03-16 13:22:18'),
	(25, 0, 6, 'slogan1', 'If You Have An Idea', '2021-03-16 13:22:18', '2021-03-16 13:22:18'),
	(26, 0, 6, 'summary', 'We are here', '2021-03-16 13:22:18', '2021-03-16 13:22:18'),
	(27, 0, 7, 'slogan1', 'Sistem ve Sunucu', '2021-03-16 13:27:21', '2021-03-16 13:27:21'),
	(28, 0, 7, 'summary', 'Yönetimi', '2021-03-16 13:27:21', '2021-03-16 13:27:21'),
	(29, 0, 8, 'slogan1', 'System and Server', '2021-03-16 13:27:21', '2021-03-16 13:27:21'),
	(30, 0, 8, 'summary', 'Management', '2021-03-16 13:27:21', '2021-03-16 13:27:21'),
	(31, 0, 9, 'slogan1', 'Bulut CRM', '2021-03-16 13:28:43', '2021-03-16 13:28:43'),
	(32, 0, 9, 'summary', 'Çözümleri', '2021-03-16 13:28:43', '2021-03-16 13:28:43'),
	(33, 0, 10, 'slogan1', 'Cloud CRM', '2021-03-16 13:28:43', '2021-03-16 13:28:43'),
	(34, 0, 10, 'summary', 'Solutions', '2021-03-16 13:28:43', '2021-03-16 13:28:43'),
	(35, 0, 11, 'slogan1', 'İş Süreçleriniz', '2021-03-16 13:30:17', '2021-03-16 13:30:17'),
	(36, 0, 11, 'summary', 'Kontrol Altında', '2021-03-16 13:30:17', '2021-03-16 13:30:17'),
	(37, 0, 12, 'slogan1', 'Your Business Processes', '2021-03-16 13:30:17', '2021-03-16 13:30:17'),
	(38, 0, 12, 'summary', 'Under Control', '2021-03-16 13:30:17', '2021-03-16 13:30:17'),
	(39, 0, 13, 'slogan1', 'Welcome to', '2021-03-16 13:32:14', '2021-03-16 13:32:14'),
	(40, 0, 13, 'summary', 'Folm Universe', '2021-03-16 13:32:14', '2021-03-16 13:32:14'),
	(41, 0, 14, 'slogan1', 'Welcome to', '2021-03-16 13:32:14', '2021-03-16 13:32:14'),
	(42, 0, 14, 'summary', 'Folm Universe', '2021-03-16 13:32:14', '2021-03-16 13:32:14'),
	(43, 0, 17, 'slogan1', 'Esnek ve Ölçeklenebilir', '2021-03-16 13:57:17', '2021-03-16 13:57:17'),
	(44, 0, 17, 'summary', 'CMS', '2021-03-16 13:57:17', '2021-03-16 13:57:17'),
	(45, 0, 18, 'slogan1', 'Flexible and Scalable', '2021-03-16 13:57:17', '2021-03-16 13:57:17'),
	(46, 0, 18, 'summary', 'CMS', '2021-03-16 13:57:17', '2021-03-16 13:57:17'),
	(47, 0, 19, 'slogan1', 'Kriptopara Projesi', '2021-03-16 14:03:10', '2021-03-16 14:03:10'),
	(48, 0, 19, 'summary', 'FolmCoin', '2021-03-16 14:03:10', '2021-03-16 14:03:10'),
	(49, 0, 20, 'slogan1', 'Kriptopara Projesi', '2021-03-16 14:03:10', '2021-03-16 14:03:10'),
	(50, 0, 20, 'summary', 'FolmCoin', '2021-03-16 14:03:10', '2021-03-16 14:03:10'),
	(51, 0, 21, 'slogan1', 'Canlı, Birebir Eğitim', '2021-03-16 14:05:30', '2021-03-16 14:05:30'),
	(52, 0, 21, 'summary', 'Egitmen.tv', '2021-03-16 14:05:30', '2021-03-16 14:05:30'),
	(53, 0, 22, 'slogan1', 'Live, One to One Education', '2021-03-16 14:05:30', '2021-03-16 14:05:30'),
	(54, 0, 22, 'summary', 'Egitmen.tv', '2021-03-16 14:05:30', '2021-03-16 14:05:30');
/*!40000 ALTER TABLE `page_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.page_extras
CREATE TABLE IF NOT EXISTS `page_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `page_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `page_extras_page_id_index` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.page_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `page_extras`;
/*!40000 ALTER TABLE `page_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.page_property
CREATE TABLE IF NOT EXISTS `page_property` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `language_id` int unsigned DEFAULT NULL,
  `country_group_id` int unsigned DEFAULT NULL,
  `property_id` int unsigned DEFAULT NULL,
  `page_id` int unsigned DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.page_property: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `page_property`;
/*!40000 ALTER TABLE `page_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `page_property` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.panel_language_parts
CREATE TABLE IF NOT EXISTS `panel_language_parts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int NOT NULL,
  `language_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.panel_language_parts: ~95 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `panel_language_parts`;
/*!40000 ALTER TABLE `panel_language_parts` DISABLE KEYS */;
INSERT INTO `panel_language_parts` (`id`, `website_id`, `language_id`, `key`, `value`) VALUES
	(1, 1, 760, 'sitemap->5->cover', 'Varsayılan Kapak Görseli'),
	(2, 1, 760, 'sitemap->5->tab->genel', 'Genel'),
	(3, 1, 760, 'sitemap->5->name', 'Sayfa Yapısı Başlığı'),
	(4, 1, 760, 'sitemap->5->detail', 'Detay Metni'),
	(5, 1, 760, 'sitemap->5->tab->gorseller', 'Görseller'),
	(6, 1, 760, 'sitemap->5->tab->seo', 'SEO'),
	(7, 1, 760, 'sitemap->5->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(8, 1, 760, 'sitemap->5->manuel', 'Manuel'),
	(9, 1, 760, 'sitemap->5->otomatik-seo', 'Otomatik SEO'),
	(10, 1, 760, 'sitemap->5->tab->yayinla', 'Yayınla'),
	(11, 1, 760, 'sitemap->5->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(12, 1, 760, 'sitemap->5->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(13, 1, 760, 'sitemap->5->parola-ile', 'Parola ile'),
	(14, 1, 760, 'sitemap->eyJpdiI6ImlHb1dLZVZiQ01FVk9ySXNUQ1hmTlE9PSIsInZhbHVlIjoiTExxRVdUdnI3VGhicnp6SUhYalFuSTU1a0lvbGZIXC9YUmN1Q0h0dkk3SDU0SGQ5MGk1dlhVSUdDYWJyWEJ0WCszT1B6Q2FMRTZKNkpUZHVQU2xUdEo5QjRhS2hZek', 'Accordion Pane'),
	(15, 1, 760, 'sitemap->5->subtitle', 'Alt Başlık'),
	(16, 1, 760, 'sitemap->eyJpdiI6IlZnczBjcUg1eExFbjM3clV5WVA1Z1E9PSIsInZhbHVlIjoidzV0d2twaDZOOVAyVGk2ek9VSGRMdnJGNDEzRXdmMk9rSVdCRTlGS3Fxb2QxNE9pZHQ4dUlaeWs2Uks2OFNuemx6OWl2eUliSUNmRTIyQ3FVVlh2QU5ySnNKQnViNn', 'Accordion Pane'),
	(17, 1, 760, 'sitemap->6->cover', 'Varsayılan Kapak Görseli'),
	(18, 1, 760, 'sitemap->6->tab->genel', 'Genel'),
	(19, 1, 760, 'sitemap->6->name', 'Sayfa Yapısı Başlığı'),
	(20, 1, 760, 'sitemap->6->detail', 'Detay Metni'),
	(21, 1, 760, 'sitemap->6->tab->gorseller', 'Görseller'),
	(22, 1, 760, 'sitemap->6->tab->seo', 'SEO'),
	(23, 1, 760, 'sitemap->6->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(24, 1, 760, 'sitemap->6->manuel', 'Manuel'),
	(25, 1, 760, 'sitemap->6->otomatik-seo', 'Otomatik SEO'),
	(26, 1, 760, 'sitemap->6->tab->yayinla', 'Yayınla'),
	(27, 1, 760, 'sitemap->6->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(28, 1, 760, 'sitemap->6->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(29, 1, 760, 'sitemap->6->parola-ile', 'Parola ile'),
	(30, 1, 760, 'sitemap->eyJpdiI6IlVLY2xTNExQdDByc2ZGMFI3SVprWnc9PSIsInZhbHVlIjoiT2RXU1FsWFdlVHhIUnZQRkNadGdHMUlcL2g3U3VcL0V4bFwva0RPbkp1YVRVMlAyWWFGYmlxdmJOakxGV2ZQTXhXQmt1aW1hSVdZa2NjRXcrNmZrdFRFcGRHbWhDV3', 'Accordion Pane'),
	(31, 1, 760, 'sitemap->2->cover', 'Varsayılan Kapak Görseli'),
	(32, 1, 760, 'sitemap->2->tab->genel', 'Genel'),
	(33, 1, 760, 'sitemap->2->name', 'Sayfa Yapısı Başlığı'),
	(34, 1, 760, 'sitemap->2->detail', 'Detay Metni'),
	(35, 1, 760, 'sitemap->2->tab->gorseller', 'Görseller'),
	(36, 1, 760, 'sitemap->2->tab->seo', 'SEO'),
	(37, 1, 760, 'sitemap->2->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(38, 1, 760, 'sitemap->2->manuel', 'Manuel'),
	(39, 1, 760, 'sitemap->2->otomatik-seo', 'Otomatik SEO'),
	(40, 1, 760, 'sitemap->2->tab->yayinla', 'Yayınla'),
	(41, 1, 760, 'sitemap->2->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(42, 1, 760, 'sitemap->2->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(43, 1, 760, 'sitemap->2->parola-ile', 'Parola ile'),
	(44, 1, 760, 'sitemap->7->page->cover', 'Varsayılan Kapak Fotoğrafı'),
	(45, 1, 760, 'sitemap->7->page->tab->yayin-durumu', 'Yayın Durumu'),
	(46, 1, 760, 'sitemap->7->page->tab->genel', 'Genel'),
	(47, 1, 760, 'sitemap->7->page->order', 'Sıralama'),
	(48, 1, 760, 'sitemap->7->page->name', 'Başlık'),
	(49, 1, 760, 'sitemap->7->page->summary', 'Detay Slogan'),
	(50, 1, 760, 'sitemap->7->page->detail', 'Detay Yazısı'),
	(51, 1, 760, 'sitemap->7->page->tab->fotograflar', 'Fotoğraflar'),
	(52, 1, 760, 'sitemap->7->page->tab->seo', 'SEO'),
	(53, 1, 760, 'sitemap->7->page->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(54, 1, 760, 'sitemap->7->page->manuel', 'Manuel'),
	(55, 1, 760, 'sitemap->7->page->otomatik-seo', 'Otomatik SEO'),
	(56, 1, 760, 'sitemap->7->page->tab->yayinla', 'Yayınla'),
	(57, 1, 760, 'sitemap->7->page->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(58, 1, 760, 'sitemap->7->page->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(59, 1, 760, 'sitemap->7->page->parola-ile', 'Parola ile'),
	(60, 1, 760, 'sitemap->eyJpdiI6IktLTytrQ2ZrcFF6Z09HaXVaTnh6RUE9PSIsInZhbHVlIjoidDI0TytUNmhVNmNITXJ2VTFBc3d5QWxSM2pYdGloZmg2KzZpK0Joc3BvXC9nVjNkazdEdDRMODFaWitFM05KOTNrMVp2WXRrcHBhRmE4dDYrcGFReDMzYTl1MEdpcX', 'Accordion Pane'),
	(61, 1, 760, 'sitemap->7->page->slogan1', 'Üst Slogan'),
	(62, 1, 760, 'sitemap->eyJpdiI6IkNyaXMweE04T1RybHdSakJcL2ZmS253PT0iLCJ2YWx1ZSI6ImtjcDRZeUZqY0dmbmZkV25MQkhzQndWaU1FOEZSaU9cL2ZlKzNxazlicCs0Z0kwWnltNG9xdk0wSVR6YkNtWkpKVXdhelN0cVlvdUdFdTBrRFVTck5WamtkaW85Uj', 'Accordion Pane'),
	(63, 1, 760, 'sitemap->eyJpdiI6ImxaSEsydnJsbUYzc0VBcEhRNFwvTXRBPT0iLCJ2YWx1ZSI6IjdkU3RrQmd2Tm9HVUdURzkramoxOFZnbkJsQTVJVnk2QU1hT2lwZUt6d1lrYXJ3Mmd0cUhQXC9HT0NpZ3dMZGJGeFM3R2V1QmtyS2hTV09wZGxJVmRPTWhPMFlPUD', 'Accordion Pane'),
	(64, 1, 760, 'sitemap->9->page->cover', 'Varsayılan Kapak Fotoğrafı'),
	(65, 1, 760, 'sitemap->9->page->tab->yayin-durumu', 'Yayın Durumu'),
	(66, 1, 760, 'sitemap->9->page->tab->genel', 'Genel'),
	(67, 1, 760, 'sitemap->9->page->order', 'Sıralama'),
	(68, 1, 760, 'sitemap->9->page->name', 'Başlık'),
	(69, 1, 760, 'sitemap->9->page->summary', 'Detay Slogan'),
	(70, 1, 760, 'sitemap->9->page->detail', 'Detay Yazısı'),
	(71, 1, 760, 'sitemap->9->page->tab->fotograflar', 'Fotoğraflar'),
	(72, 1, 760, 'sitemap->9->page->tab->seo', 'SEO'),
	(73, 1, 760, 'sitemap->9->page->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(74, 1, 760, 'sitemap->9->page->manuel', 'Manuel'),
	(75, 1, 760, 'sitemap->9->page->otomatik-seo', 'Otomatik SEO'),
	(76, 1, 760, 'sitemap->9->page->tab->yayinla', 'Yayınla'),
	(77, 1, 760, 'sitemap->9->page->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(78, 1, 760, 'sitemap->9->page->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(79, 1, 760, 'sitemap->9->page->parola-ile', 'Parola ile'),
	(80, 1, 760, 'sitemap->eyJpdiI6Im1oWlA0bGlBZHN2cnRqZjNmYk5WMHc9PSIsInZhbHVlIjoidFBSQ2srOUY2Y2srTlZ0Qzd4SStTOGs4Y1RIQ2JDa0tzUzFjWmpya3pibXpKS1ptRUFCMGErVTZoTWdcL0ZQek41N0xoVWREVmZ6amViNWo0THdPYWg3MjV3MXlBSj', 'Accordion Pane'),
	(81, 1, 760, 'sitemap->eyJpdiI6Im1Oa05lb3lCU1lqdEYwR01Vbjc2cnc9PSIsInZhbHVlIjoiRGVCSnVCMkhENVwvSFNraHdwYXR1emZDc29PODJuYVdqZmdjdmprSHFsVjlLWVZESzRFTzdkaEw3d2hxYnBSaktxd2F3VThnclBCTzJGeFd1WHRNYzR6WUFpR21vZX', 'Accordion Pane'),
	(82, 1, 760, 'sitemap->9->page->slogan1', 'Üst Slogan'),
	(83, 1, 760, 'sitemap->eyJpdiI6IlRNUnIrWE9hWUFIY0F1ZkRKVzhsaGc9PSIsInZhbHVlIjoicXFhTDNCc2h0blBRUVFYb2VDSE8zSlc4a01aNFpGdDNTRDVwM2xtSDZxVnZ3OWVlUHFMZ3pVY0dpK1o3MCtBZHdaTTlpWFNxSGZjN3hMMUIwRFJwWDdLcFlaOXlKQ0', 'Accordion Pane'),
	(84, 1, 760, 'sitemap->eyJpdiI6Im1oWlA0bGlBZHN2cnRqZjNmYk5WMHc9PSIsInZhbHVlIjoidFBSQ2srOUY2Y2srTlZ0Qzd4SStTOGs4Y1RIQ2JDa0tzUzFjWmpya3pibXpKS1ptRUFCMGErVTZoTWdcL0ZQek41N0xoVWREVmZ6amViNWo0THdPYWg3MjV3MXlBSj', 'Accordion Pane'),
	(85, 1, 760, 'sitemap->10->cover', 'Varsayılan Kapak Görseli'),
	(86, 1, 760, 'sitemap->10->tab->genel', 'Genel'),
	(87, 1, 760, 'sitemap->10->name', 'Sayfa Yapısı Başlığı'),
	(88, 1, 760, 'sitemap->10->detail', 'Detay Metni'),
	(89, 1, 760, 'sitemap->10->tab->gorseller', 'Görseller'),
	(90, 1, 760, 'sitemap->10->tab->seo', 'SEO'),
	(91, 1, 760, 'sitemap->10->search_text', 'Aptal Arama <small>Burada yazacağınız metin sayfada gözükmez, sadece site içi aramalarda dikkate alınır.</small>'),
	(92, 1, 760, 'sitemap->10->manuel', 'Manuel'),
	(93, 1, 760, 'sitemap->10->otomatik-seo', 'Otomatik SEO'),
	(94, 1, 760, 'sitemap->10->tab->yayinla', 'Yayınla'),
	(95, 1, 760, 'sitemap->10->tab->kisitlamali-sayfa', 'Kısıtlamalı Sayfa '),
	(96, 1, 760, 'sitemap->10->kullanici-rolune-gore', 'Kullanıcı rolüne göre'),
	(97, 1, 760, 'sitemap->10->parola-ile', 'Parola ile');
/*!40000 ALTER TABLE `panel_language_parts` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.panel_menus
CREATE TABLE IF NOT EXISTS `panel_menus` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.panel_menus: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `panel_menus`;
/*!40000 ALTER TABLE `panel_menus` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_menus` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.panel_menu_details
CREATE TABLE IF NOT EXISTS `panel_menu_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int unsigned DEFAULT NULL,
  `sitemap_id` int unsigned DEFAULT NULL,
  `parent` int unsigned DEFAULT NULL,
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `depth` int unsigned DEFAULT NULL,
  `draft` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.panel_menu_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `panel_menu_details`;
/*!40000 ALTER TABLE `panel_menu_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `panel_menu_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.password_resets: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `password_resets`;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.permissions
CREATE TABLE IF NOT EXISTS `permissions` (
  `ability_id` int unsigned NOT NULL,
  `entity_id` int unsigned NOT NULL,
  `entity_type` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forbidden` tinyint(1) NOT NULL DEFAULT '0',
  `scope` int DEFAULT NULL,
  KEY `permissions_entity_index` (`entity_id`,`entity_type`,`scope`),
  KEY `permissions_ability_id_index` (`ability_id`),
  KEY `permissions_scope_index` (`scope`),
  CONSTRAINT `permissions_ability_id_foreign` FOREIGN KEY (`ability_id`) REFERENCES `abilities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.permissions: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `permissions`;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` (`ability_id`, `entity_id`, `entity_type`, `forbidden`, `scope`) VALUES
	(1, 2, 'Mediapress\\Modules\\Auth\\Models\\Role', 0, NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.popups
CREATE TABLE IF NOT EXISTS `popups` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` tinyint DEFAULT NULL,
  `website_id` int unsigned NOT NULL,
  `order` int unsigned DEFAULT NULL,
  `status` tinyint unsigned NOT NULL,
  `devices` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_date_type` tinyint unsigned NOT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `display_time_type` tinyint unsigned NOT NULL,
  `display_time` smallint unsigned DEFAULT NULL,
  `type` tinyint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_display` tinyint unsigned NOT NULL,
  `content_only_image` tinyint DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `impressions` tinyint DEFAULT NULL,
  `impression_count` int unsigned DEFAULT NULL,
  `show_page` tinyint unsigned NOT NULL,
  `conditions` tinyint unsigned NOT NULL,
  `condition_time` smallint unsigned DEFAULT NULL,
  `condition_pixel` smallint unsigned DEFAULT NULL,
  `font` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `border_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `border_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `padding_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `full_bg` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `button_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `border_radius` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_animation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title_space` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_font_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content_animation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btn_font_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `btn_position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `counter_font_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_css` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.popups: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `popups`;
/*!40000 ALTER TABLE `popups` DISABLE KEYS */;
/*!40000 ALTER TABLE `popups` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.popup_custom_urls
CREATE TABLE IF NOT EXISTS `popup_custom_urls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `popup_id` int unsigned NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `popup_custom_urls_popup_id_index` (`popup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.popup_custom_urls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `popup_custom_urls`;
/*!40000 ALTER TABLE `popup_custom_urls` DISABLE KEYS */;
/*!40000 ALTER TABLE `popup_custom_urls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.popup_languages
CREATE TABLE IF NOT EXISTS `popup_languages` (
  `popup_id` int unsigned NOT NULL,
  `country_group_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  KEY `popup_languages_popup_id_index` (`popup_id`),
  KEY `popup_languages_country_group_id_index` (`country_group_id`),
  KEY `popup_languages_language_id_index` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.popup_languages: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `popup_languages`;
/*!40000 ALTER TABLE `popup_languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `popup_languages` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.popup_urls
CREATE TABLE IF NOT EXISTS `popup_urls` (
  `popup_id` int unsigned NOT NULL,
  `url_id` int unsigned NOT NULL,
  KEY `popup_urls_popup_id_index` (`popup_id`),
  KEY `popup_urls_url_id_index` (`url_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.popup_urls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `popup_urls`;
/*!40000 ALTER TABLE `popup_urls` DISABLE KEYS */;
/*!40000 ALTER TABLE `popup_urls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.properties
CREATE TABLE IF NOT EXISTS `properties` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int unsigned NOT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `lft` int unsigned DEFAULT NULL,
  `rgt` int unsigned DEFAULT NULL,
  `depth` int unsigned DEFAULT NULL,
  `property_id` int unsigned DEFAULT NULL,
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `cdec_1` decimal(20,4) DEFAULT NULL,
  `cdec_2` decimal(20,4) DEFAULT NULL,
  `cdec_3` decimal(20,4) DEFAULT NULL,
  `cdec_4` decimal(20,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `properties_sitemap_id_index` (`sitemap_id`),
  KEY `properties_property_id_index` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.properties: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `properties`;
/*!40000 ALTER TABLE `properties` DISABLE KEYS */;
/*!40000 ALTER TABLE `properties` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.property_details
CREATE TABLE IF NOT EXISTS `property_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int unsigned NOT NULL,
  `country_group_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL DEFAULT '3',
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `property_details_property_id_index` (`property_id`),
  KEY `property_details_country_group_id_index` (`country_group_id`),
  KEY `property_details_language_id_index` (`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.property_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `property_details`;
/*!40000 ALTER TABLE `property_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.property_detail_extras
CREATE TABLE IF NOT EXISTS `property_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `property_detail_id` int unsigned DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_detail_extras_property_detail_id_index` (`property_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.property_detail_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `property_detail_extras`;
/*!40000 ALTER TABLE `property_detail_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.property_extras
CREATE TABLE IF NOT EXISTS `property_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `property_id` int unsigned DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `property_extras_property_id_index` (`property_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.property_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `property_extras`;
/*!40000 ALTER TABLE `property_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.property_page
CREATE TABLE IF NOT EXISTS `property_page` (
  `property_id` int unsigned NOT NULL,
  `page_id` int unsigned NOT NULL,
  KEY `property_page_property_id_index` (`property_id`),
  KEY `property_page_page_id_index` (`page_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.property_page: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `property_page`;
/*!40000 ALTER TABLE `property_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `property_page` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.redirect_urls
CREATE TABLE IF NOT EXISTS `redirect_urls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `redirect_urls_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.redirect_urls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `redirect_urls`;
/*!40000 ALTER TABLE `redirect_urls` DISABLE KEYS */;
/*!40000 ALTER TABLE `redirect_urls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` int unsigned DEFAULT NULL,
  `scope` int DEFAULT NULL,
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`,`scope`),
  KEY `roles_scope_index` (`scope`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.roles: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `title`, `level`, `scope`, `admin_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'SuperMCAdmin', 'Mediaclick Yönetici', NULL, NULL, NULL, 3, NULL, NULL, NULL),
	(2, 'Moderator', 'Moderatör', NULL, NULL, NULL, 3, NULL, NULL, NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.scenes
CREATE TABLE IF NOT EXISTS `scenes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `slider_id` int unsigned NOT NULL,
  `time` int DEFAULT NULL,
  `order` int NOT NULL DEFAULT '1',
  `status` tinyint NOT NULL DEFAULT '1',
  `admin_id` tinyint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.scenes: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `scenes`;
/*!40000 ALTER TABLE `scenes` DISABLE KEYS */;
INSERT INTO `scenes` (`id`, `slider_id`, `time`, `order`, `status`, `admin_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, NULL, 1, 1, 1, '2021-03-16 09:30:32', '2021-03-16 09:30:32', NULL);
/*!40000 ALTER TABLE `scenes` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.scene_details
CREATE TABLE IF NOT EXISTS `scene_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `scene_id` int unsigned NOT NULL,
  `language_id` int DEFAULT NULL,
  `country_group_id` int NOT NULL DEFAULT '1',
  `texts` text COLLATE utf8mb4_unicode_ci,
  `buttons` text COLLATE utf8mb4_unicode_ci,
  `files` text COLLATE utf8mb4_unicode_ci,
  `url` text COLLATE utf8mb4_unicode_ci,
  `video_url` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.scene_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `scene_details`;
/*!40000 ALTER TABLE `scene_details` DISABLE KEYS */;
INSERT INTO `scene_details` (`id`, `scene_id`, `language_id`, `country_group_id`, `texts`, `buttons`, `files`, `url`, `video_url`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 760, 1, '{"1":{"desktop":"Web Tasar\\u0131m ve Web Tabanl\\u0131","tablet":"Web Tasar\\u0131m ve Web Tabanl\\u0131","mobile":"Web Tasar\\u0131m ve Web Tabanl\\u0131"},"2":{"desktop":"yaz\\u0131l\\u0131m firmas\\u0131","tablet":"yaz\\u0131l\\u0131m firmas\\u0131","mobile":"yaz\\u0131l\\u0131m firmas\\u0131"}}', NULL, '{"desktop":"[3]","tablet":"[]","mobile":"[]"}', '{"text":{"type":"_self","text":null},"holder":"text"}', NULL, '2021-03-16 09:30:32', '2021-03-16 09:30:32', NULL),
	(2, 1, 616, 1, '{"1":{"desktop":"Web Design and Web Based","tablet":"Web Design and Web Based","mobile":"Web Design and Web Based"},"2":{"desktop":"software company","tablet":"software company","mobile":"software company"}}', NULL, '{"desktop":"[3]","tablet":"[]","mobile":"[]"}', '{"text":{"type":"_self","text":null},"holder":"text"}', NULL, '2021-03-16 09:30:32', '2021-03-16 09:30:32', NULL);
/*!40000 ALTER TABLE `scene_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.scripts
CREATE TABLE IF NOT EXISTS `scripts` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int NOT NULL,
  `append_head` longtext COLLATE utf8mb4_unicode_ci,
  `prepend_body` longtext COLLATE utf8mb4_unicode_ci,
  `append_body` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.scripts: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `scripts`;
/*!40000 ALTER TABLE `scripts` DISABLE KEYS */;
/*!40000 ALTER TABLE `scripts` ENABLE KEYS */;

-- görünüm yapısı dökülüyor genarge.search_content
-- VIEW bağımlılık sorunlarını çözmek için geçici tablolar oluşturuluyor
CREATE TABLE `search_content` (
	`model_id` INT(10) UNSIGNED NOT NULL,
	`model_type` VARCHAR(42) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`language_id` INT(10) UNSIGNED NULL,
	`detail_name` VARCHAR(191) NULL COLLATE 'utf8mb4_unicode_ci',
	`detail_detail` LONGTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`detail_search_text` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`url_id` INT(10) UNSIGNED NULL,
	`cvar_1` VARCHAR(300) NULL COLLATE 'utf8mb4_unicode_ci',
	`cvar_2` VARCHAR(300) NULL COLLATE 'utf8mb4_unicode_ci',
	`ctex_1` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`ctex_2` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`cint_1` INT(10) NULL,
	`cint_2` VARCHAR(191) NULL COLLATE 'utf8mb4_unicode_ci',
	`cdat_1` DATE NULL,
	`cdat_2` DATE NULL,
	`cdec_1` DECIMAL(20,4) NULL,
	`cdec_2` DECIMAL(20,4) NULL,
	`detail_key` VARCHAR(191) NULL COLLATE 'utf8mb4_unicode_ci',
	`detail_value` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci',
	`key` VARCHAR(191) NULL COLLATE 'utf8mb4_unicode_ci',
	`value` MEDIUMTEXT NULL COLLATE 'utf8mb4_unicode_ci'
) ENGINE=MyISAM;

-- tablo yapısı dökülüyor genarge.seo_contents
CREATE TABLE IF NOT EXISTS `seo_contents` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `url_id` int NOT NULL,
  `h1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `h2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.seo_contents: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `seo_contents`;
/*!40000 ALTER TABLE `seo_contents` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_contents` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.seo_keywords
CREATE TABLE IF NOT EXISTS `seo_keywords` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `url_id` int NOT NULL,
  `keyword` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` tinyint NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.seo_keywords: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `seo_keywords`;
/*!40000 ALTER TABLE `seo_keywords` DISABLE KEYS */;
/*!40000 ALTER TABLE `seo_keywords` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.settings
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.settings: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `settings`;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.setting_sun
CREATE TABLE IF NOT EXISTS `setting_sun` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `vtype` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'string',
  `params` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.setting_sun: ~8 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `setting_sun`;
/*!40000 ALTER TABLE `setting_sun` DISABLE KEYS */;
INSERT INTO `setting_sun` (`id`, `website_id`, `group`, `title`, `key`, `value`, `vtype`, `params`) VALUES
	(1, NULL, 'E-Posta Yönetimi', 'Mailchimp API Key', 'heraldist.mailchimp.apikey', 'a7376c4ec8bf6dd27d24266bd01808df-us18', 'string', NULL),
	(2, NULL, 'E-Posta Yönetimi', 'Gönderici E-Posta', 'heraldist.mpmailler.fromemailaddress', 'default@default.com', 'string', NULL),
	(3, NULL, 'Varsayılan Gönderici Adı', 'Gönderici E-Posta', 'heraldist.mpmailler.fromname', 'test', 'string', NULL),
	(4, NULL, 'Analytics', 'Yandex Metrica Cache', 'mpcore.analytics.yandex.cache', '600', 'string', NULL),
	(5, NULL, 'Analytics', 'Yandex Metrica Counter ID (Sayaç ID)', 'mpcore.analytics.yandex.counter_id', '49873495', 'string', NULL),
	(6, NULL, 'Analytics', 'Yandex Metrica Token', 'mpcore.analytics.yandex.token', 'AQAAAAAo4lphAAUjimZh6CfDoEkevy2m8jNLwt0', 'string', NULL),
	(7, NULL, 'Analytics', 'Bing API Key', 'mpcore.analytics.bing.apikey', '5d8fb140135c40f28a09042f7fe81bff', 'string', NULL),
	(8, NULL, 'Analytics', 'Bing API website URL', 'mpcore.analytics.bing.api_website_url', 'http://test.com', 'string', NULL),
	(9, '1', 'Logo', NULL, 'logo.desktop_logo', '1', 'string', NULL),
	(10, '1', 'Logo', NULL, 'logo.favicon', '2', 'string', NULL);
/*!40000 ALTER TABLE `setting_sun` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemaps
CREATE TABLE IF NOT EXISTS `sitemaps` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_type_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `feature_tag` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `urlStatus` tinyint NOT NULL DEFAULT '1',
  `detailPage` tinyint(1) NOT NULL DEFAULT '0',
  `autoOrder` tinyint NOT NULL DEFAULT '1',
  `subPageOrder` tinyint NOT NULL DEFAULT '1',
  `reservedUrl` tinyint NOT NULL DEFAULT '1',
  `searchable` tinyint(1) NOT NULL DEFAULT '0',
  `category` tinyint(1) NOT NULL DEFAULT '0',
  `criteria` tinyint(1) NOT NULL DEFAULT '0',
  `property` tinyint(1) NOT NULL DEFAULT '0',
  `filesystem` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sfilesystem` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_in_panel_menu` tinyint(1) NOT NULL DEFAULT '0',
  `featured_in_panel_menu` tinyint(1) NOT NULL DEFAULT '0',
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `custom` int DEFAULT NULL,
  `ctex_1` text COLLATE utf8mb4_unicode_ci,
  `ctex_2` text COLLATE utf8mb4_unicode_ci,
  `cvar_1` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_2` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_3` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_4` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cvar_5` varchar(300) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_1` int DEFAULT NULL,
  `cint_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_4` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cint_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cdat_1` date DEFAULT NULL,
  `cdat_2` date DEFAULT NULL,
  `cdec_1` decimal(20,4) DEFAULT NULL,
  `cdec_2` decimal(20,4) DEFAULT NULL,
  `cdec_3` decimal(20,4) DEFAULT NULL,
  `cdec_4` decimal(20,4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allowed_role_id` int DEFAULT NULL,
  `sitemap_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemaps: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemaps`;
/*!40000 ALTER TABLE `sitemaps` DISABLE KEYS */;
INSERT INTO `sitemaps` (`id`, `sitemap_type_id`, `feature_tag`, `urlStatus`, `detailPage`, `autoOrder`, `subPageOrder`, `reservedUrl`, `searchable`, `category`, `criteria`, `property`, `filesystem`, `sfilesystem`, `show_in_panel_menu`, `featured_in_panel_menu`, `admin_id`, `status`, `custom`, `ctex_1`, `ctex_2`, `cvar_1`, `cvar_2`, `cvar_3`, `cvar_4`, `cvar_5`, `cint_1`, `cint_2`, `cint_3`, `cint_4`, `cint_5`, `cdat_1`, `cdat_2`, `cdec_1`, `cdec_2`, `cdec_3`, `cdec_4`, `created_at`, `updated_at`, `deleted_at`, `password`, `allowed_role_id`, `sitemap_id`) VALUES
	(2, '1', 'homepage', 1, 1, 0, 0, 0, 0, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:24:32', '2021-03-16 09:24:48', NULL, NULL, NULL, NULL),
	(5, '3', NULL, 1, 1, 0, 0, 0, 1, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:35:52', '2021-03-16 09:43:28', NULL, '', 0, NULL),
	(6, '4', NULL, 1, 1, 0, 0, 0, 1, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:08:29', '2021-03-16 10:43:19', NULL, '', 0, NULL),
	(7, '5', NULL, 1, 1, 1, 0, 1, 1, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:21:24', '2021-03-16 12:22:07', NULL, NULL, NULL, NULL),
	(9, '6', NULL, 1, 1, 1, 0, 1, 1, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:52:22', '2021-03-16 13:52:45', NULL, NULL, NULL, NULL),
	(10, '7', NULL, 1, 1, 0, 0, 0, 1, 0, 0, 0, 'uploads', 'public', 1, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:27:27', '2021-03-16 14:44:29', NULL, '', 0, NULL);
/*!40000 ALTER TABLE `sitemaps` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_details
CREATE TABLE IF NOT EXISTS `sitemap_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  `country_group_id` int unsigned DEFAULT NULL,
  `website_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `status` int NOT NULL DEFAULT '3',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `search_text` text COLLATE utf8mb4_unicode_ci,
  `meta_templates` text COLLATE utf8mb4_unicode_ci,
  `manual_meta` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `sitemap_details_sitemap_id_index` (`sitemap_id`),
  KEY `sitemap_details_language_id_index` (`language_id`),
  KEY `sitemap_details_country_group_id_index` (`country_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_details`;
/*!40000 ALTER TABLE `sitemap_details` DISABLE KEYS */;
INSERT INTO `sitemap_details` (`id`, `sitemap_id`, `language_id`, `country_group_id`, `website_id`, `name`, `slug`, `category_slug`, `active`, `status`, `deleted_at`, `detail`, `search_text`, `meta_templates`, `manual_meta`) VALUES
	(1, 2, 760, 1, 1, 'Ana Sayfa', '/', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(2, 2, 616, 1, 1, 'Home Page', '/', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(3, 5, 760, 1, 1, 'Biz Kimiz', '/biz-kimiz', '/', 1, 1, NULL, '<p>Gen Arge A.Ş. Manisa Celal Bayar Üniversitesi Teknokent bünyesinde kurulmuş bir yazılım şirketidir. Türkiye’nin öncü dijital ajanslarından MediaClick’in yazılım alt yapısını kuran ve desteğini veren, web tabanlı yazılım teknolojilerine ve blockchain teknolojilerine odaklanmış bir yazılım firmasıdır.</p>\r\n\r\n<p>Kurulduğu günden bugüne, ARGE odaklı düşünen, yeni nesil teknolojileri araştırıp, yazılımlarında bu teknolojiler kullanarak, dünya standartlarında yazılımlar üretmektedir.</p>\r\n\r\n<p>Gen Arge’nin vizyonu, odaklandığı ve geliştirdiği ürünleri sadece ülkemizde değil dünyada kullanılan ürünler haline getirmek ve uluslararası bir başarı hikayesi yazmaktır.&nbsp;</p>\r\n', '', '[]', 0),
	(4, 5, 616, 1, 1, 'Who are We', '/who-are-we', '/', 1, 1, NULL, '<p>Gen Arge A.Ş. Manisa Celal is a software company established under Bayar University Teknokent. Turkey\'s leading digital agencies in establishing the infrastructure that mediaclick software and support, is a software company focused on web-based software technologies and their blockcha technology.</p>\r\n\r\n<p>Since the day it was founded, it has been producing world-class software by researching new generation technologies, thinking about R&amp;D, and using these technologies in its software.</p>\r\n\r\n<p>The vision of Gen Arge is to turn the products it focuses and develops into products that are used not only in our country but in the world and to write an international success story.</p>\r\n', '', '[]', 0),
	(5, 6, 760, 1, 1, 'Odak Teknolojiler', '/odak-teknolojiler', '/', 1, 1, NULL, '', '', '[]', 0),
	(6, 6, 616, 1, 1, 'Focus Technologies', '/focus-technologies', '/', 1, 1, NULL, '', '', '[]', 0),
	(7, 7, 760, 1, 1, 'Neler Yapıyoruz', '/neler-yapiyoruz', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(8, 7, 616, 1, 1, 'What We Do', '/what-we-do', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(9, 9, 760, 1, 1, 'Projeler', '/projeler', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(10, 9, 616, 1, 1, 'Projects', '/projects', '/', 1, 1, NULL, '', NULL, '[]', 0),
	(11, 10, 760, 1, 1, 'İletişim', '/iletisim', '/', 1, 1, NULL, '<p><strong>Vergi Bilgileri</strong></p>\r\n\r\n<p>Mesir VD. 3910325521<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Banka Hesap Numaraları</strong></p>\r\n\r\n<p>QNB FinansBank / Enpara Şubesi (03663)</p>\r\n\r\n<p>IBAN : TR43 0011 1000 0000 0092 4860 33 (TL)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Adres</strong></p>\r\n\r\n<p>Muradiye Mah.Celal Bayar Üniversitesi Kampüsü, Küme Evleri, Teknoloji Geliştirme (Bölgesi İçi) Merkezi No:22 / B206 Yunusemre / MANİSA</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Telefon</strong></p>\r\n\r\n<p><a href="tel:0850 811 51 21">0850 811 51 21</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>E-Posta</strong></p>\r\n\r\n<p><a href="mailto:info@genarge.com.tr">info@genarge.com.tr</a></p>\r\n', '', '[]', 0),
	(12, 10, 616, 1, 1, 'Contact', '/contact', '/', 1, 1, NULL, '<p><strong>Tax Information</strong></p>\r\n\r\n<p>Mesir VD. 3910325521</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Bank Account Numbers</strong></p>\r\n\r\n<p>QNB FinansBank / Enpara Şubesi (03663)</p>\r\n\r\n<p>IBAN : TR43 0011 1000 0000 0092 4860 33 (TL)</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Address</strong></p>\r\n\r\n<p>Muradiye Mah.Celal Bayar Üniversitesi Kampüsü, Küme Evleri, Teknoloji Geliştirme (Bölgesi İçi) Merkezi No:22 / B206 Yunusemre / MANİSA</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Telephone</strong></p>\r\n\r\n<p><a href="tel:0850 811 51 21">0850 811 51 21</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>E-Mail</strong></p>\r\n\r\n<p><a href="mailto:info@genarge.com.tr">info@genarge.com.tr</a></p>\r\n', '', '[]', 0);
/*!40000 ALTER TABLE `sitemap_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_detail_extras
CREATE TABLE IF NOT EXISTS `sitemap_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_detail_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sitemap_detail_extras_sitemap_detail_id_index` (`sitemap_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_detail_extras: ~2 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_detail_extras`;
/*!40000 ALTER TABLE `sitemap_detail_extras` DISABLE KEYS */;
INSERT INTO `sitemap_detail_extras` (`id`, `sitemap_detail_id`, `key`, `value`) VALUES
	(3, 3, 'subtitle', 'Gen Arge'),
	(4, 4, 'subtitle', 'Gen Arge');
/*!40000 ALTER TABLE `sitemap_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_extras
CREATE TABLE IF NOT EXISTS `sitemap_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sitemap_extras_sitemap_id_index` (`sitemap_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_extras`;
/*!40000 ALTER TABLE `sitemap_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemap_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_types
CREATE TABLE IF NOT EXISTS `sitemap_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `external_controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `external_required` tinyint unsigned NOT NULL DEFAULT '0',
  `sitemap_type_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `website_id` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_types: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_types`;
/*!40000 ALTER TABLE `sitemap_types` DISABLE KEYS */;
INSERT INTO `sitemap_types` (`id`, `name`, `controller`, `external_controller`, `external_required`, `sitemap_type_type`, `created_at`, `updated_at`, `deleted_at`, `website_id`) VALUES
	(1, 'Homepage', '', NULL, 0, 'static', '2021-03-16 09:24:47', '2021-03-16 09:24:47', NULL, 1),
	(2, 'UserStatic', '', NULL, 0, 'static', '2021-03-16 09:34:38', '2021-03-16 09:34:38', NULL, 1),
	(3, 'BizKimiz', '', NULL, 0, 'static', '2021-03-16 09:36:10', '2021-03-16 09:36:10', NULL, 1),
	(4, 'OdakTeknolojiler', '', NULL, 0, 'static', '2021-03-16 10:08:59', '2021-03-16 10:08:59', NULL, 1),
	(5, 'NelerYapiyoruz', '', NULL, 0, 'dynamic', '2021-03-16 12:22:07', '2021-03-16 12:22:07', NULL, 1),
	(6, 'Projeler', '', NULL, 0, 'dynamic', '2021-03-16 13:52:45', '2021-03-16 13:52:45', NULL, 1),
	(7, 'Contact', '', NULL, 0, 'static', '2021-03-16 14:27:48', '2021-03-16 14:27:48', NULL, 1);
/*!40000 ALTER TABLE `sitemap_types` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_website
CREATE TABLE IF NOT EXISTS `sitemap_website` (
  `website_id` int unsigned NOT NULL,
  `sitemap_id` int unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_website: ~5 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_website`;
/*!40000 ALTER TABLE `sitemap_website` DISABLE KEYS */;
INSERT INTO `sitemap_website` (`website_id`, `sitemap_id`) VALUES
	(1, 2),
	(1, 5),
	(1, 6),
	(1, 7),
	(1, 9),
	(1, 10);
/*!40000 ALTER TABLE `sitemap_website` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_xmls
CREATE TABLE IF NOT EXISTS `sitemap_xmls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Title to display',
  `filename` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Base filename for this sitemap_xml entry',
  `status` tinyint unsigned NOT NULL DEFAULT '2',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'custom',
  `split` tinyint NOT NULL DEFAULT '1',
  `urls_per_page` int unsigned DEFAULT '10000',
  `website_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `renew_freq` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'weekly',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_xmls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_xmls`;
/*!40000 ALTER TABLE `sitemap_xmls` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemap_xmls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sitemap_xml_blocks
CREATE TABLE IF NOT EXISTS `sitemap_xml_blocks` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `sitemap_xml_id` int NOT NULL,
  `sitemap_id` int NOT NULL,
  `include_detail` tinyint DEFAULT NULL,
  `detail_priority` decimal(3,2) DEFAULT NULL,
  `detail_change_frequency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `include_categories` tinyint DEFAULT NULL,
  `categories_priority` decimal(3,2) DEFAULT NULL,
  `categories_change_frequency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `include_pages` tinyint DEFAULT NULL,
  `pages_priority` decimal(3,2) DEFAULT NULL,
  `pages_change_frequency` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sitemap_xml_blocks: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sitemap_xml_blocks`;
/*!40000 ALTER TABLE `sitemap_xml_blocks` DISABLE KEYS */;
/*!40000 ALTER TABLE `sitemap_xml_blocks` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.sliders
CREATE TABLE IF NOT EXISTS `sliders` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'image',
  `text` int NOT NULL DEFAULT '0',
  `button` int NOT NULL DEFAULT '0',
  `screen` int NOT NULL DEFAULT '1',
  `width` int DEFAULT NULL,
  `height` int DEFAULT NULL,
  `admin_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.sliders: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `sliders`;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` (`id`, `website_id`, `name`, `type`, `text`, `button`, `screen`, `width`, `height`, `admin_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, 'main_slider', 'image', 2, 0, 3, 1400, 800, 1, '2021-03-16 09:29:15', '2021-03-16 09:29:15', NULL);
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.slots
CREATE TABLE IF NOT EXISTS `slots` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `slot` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.slots: ~6 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `slots`;
/*!40000 ALTER TABLE `slots` DISABLE KEYS */;
INSERT INTO `slots` (`id`, `slot`, `description`) VALUES
	(1, 'Core', 'Core Slot'),
	(2, 'Content', 'Content manager'),
	(3, 'Entity', 'User and entity manager'),
	(4, 'Messaging', 'Form and message manager'),
	(5, 'Auth', 'Auth manager'),
	(6, 'TagManager', 'Tag Manager');
/*!40000 ALTER TABLE `slots` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.social_media
CREATE TABLE IF NOT EXISTS `social_media` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `order` int unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `admin_id` int DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.social_media: ~4 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `social_media`;
/*!40000 ALTER TABLE `social_media` DISABLE KEYS */;
INSERT INTO `social_media` (`id`, `website_id`, `order`, `name`, `link`, `icon`, `group_id`, `lang_id`, `status`, `admin_id`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, 'facebook', '{"1":{"760":"https:\\/\\/www.facebook.com\\/MediaClick\\/","616":"https:\\/\\/www.facebook.com\\/MediaClick\\/"}}', NULL, '1', '760', '1', 1, '2021-03-16 14:47:53', '2021-03-16 14:47:53'),
	(2, 1, 2, 'twitter', '{"1":{"760":"https:\\/\\/twitter.com\\/mediaclicktr","616":"https:\\/\\/twitter.com\\/mediaclicktr"}}', NULL, '1', '760', '1', 1, '2021-03-16 14:48:11', '2021-03-16 14:48:11'),
	(3, 1, 3, 'youtube', '{"1":{"760":"https:\\/\\/www.youtube.com\\/channel\\/UCSxLWHELy1yx3TvUBxzxo0A","616":"https:\\/\\/www.youtube.com\\/channel\\/UCSxLWHELy1yx3TvUBxzxo0A"}}', NULL, '1', '760', '1', 1, '2021-03-16 14:48:32', '2021-03-16 14:48:32'),
	(4, 1, 4, 'instagram', '{"1":{"760":"https:\\/\\/www.instagram.com\\/mediaclicktr\\/","616":"https:\\/\\/www.instagram.com\\/mediaclicktr\\/"}}', NULL, '1', '760', '1', 1, '2021-03-16 14:48:50', '2021-03-16 14:48:50');
/*!40000 ALTER TABLE `social_media` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.urls
CREATE TABLE IF NOT EXISTS `urls` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `urls_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.urls: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `urls`;
/*!40000 ALTER TABLE `urls` DISABLE KEYS */;
INSERT INTO `urls` (`id`, `website_id`, `url`, `model_type`, `model_id`, `type`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 1, '/tr', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 1, 'original', '2021-03-16 09:24:48', '2021-03-16 09:24:48', NULL),
	(2, 1, '/en', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 2, 'original', '2021-03-16 09:24:48', '2021-03-16 09:24:48', NULL),
	(3, 1, '/tr/biz-kimiz', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 3, 'original', '2021-03-16 09:36:10', '2021-03-16 09:36:10', NULL),
	(4, 1, '/en/who-are-we', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 4, 'original', '2021-03-16 09:36:10', '2021-03-16 09:36:10', NULL),
	(5, 1, '/tr/odak-teknolojiler', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 5, 'original', '2021-03-16 10:08:59', '2021-03-16 10:08:59', NULL),
	(6, 1, '/en/focus-technologies', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 6, 'original', '2021-03-16 10:08:59', '2021-03-16 10:08:59', NULL),
	(7, 1, '/tr/neler-yapiyoruz', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 7, 'original', '2021-03-16 12:22:07', '2021-03-16 12:22:07', NULL),
	(8, 1, '/en/what-we-do', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 8, 'original', '2021-03-16 12:22:07', '2021-03-16 12:22:07', NULL),
	(9, 1, '/tr/neler-yapiyoruz/ozel-web-tabanli-yazilim-gelistirme', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 1, 'original', '2021-03-16 12:23:25', '2021-03-16 12:23:58', NULL),
	(10, 1, '/en/what-we-do/custom-web-based-software-development', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 2, 'original', '2021-03-16 12:23:25', '2021-03-16 12:23:58', NULL),
	(11, 1, '/tr/neler-yapiyoruz/web-tasarim-projeleri', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 3, 'original', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL),
	(12, 1, '/en/what-we-do/web-design-projects', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 4, 'original', '2021-03-16 12:28:28', '2021-03-16 12:30:20', NULL),
	(13, 1, '/tr/neler-yapiyoruz/start-up-proje-danismanligi', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 5, 'original', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL),
	(14, 1, '/en/what-we-do/start-up-project-consultancy', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 6, 'original', '2021-03-16 13:20:53', '2021-03-16 13:22:18', NULL),
	(15, 1, '/tr/neler-yapiyoruz/sunucu-bakimi-ve-guvenligi', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 7, 'original', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL),
	(16, 1, '/en/what-we-do/server-maintenance-and-security', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 8, 'original', '2021-03-16 13:22:34', '2021-03-16 13:27:21', NULL),
	(17, 1, '/tr/neler-yapiyoruz/crm-yazilimlari', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 9, 'original', '2021-03-16 13:27:35', '2021-03-16 13:28:43', NULL),
	(18, 1, '/en/what-we-do/crm-software', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 10, 'original', '2021-03-16 13:27:35', '2021-03-16 13:28:43', NULL),
	(19, 1, '/tr/neler-yapiyoruz/is-sureci-yonetim-yazilimlari', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 11, 'original', '2021-03-16 13:29:02', '2021-03-16 13:30:17', NULL),
	(20, 1, '/en/what-we-do/business-process-management-software', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 12, 'original', '2021-03-16 13:29:02', '2021-03-16 13:30:17', NULL),
	(21, 1, '/tr/neler-yapiyoruz/blockchain-teknolojileri', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 13, 'original', '2021-03-16 13:30:57', '2021-03-16 13:32:14', NULL),
	(22, 1, '/en/what-we-do/blockchain-technologies', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 14, 'original', '2021-03-16 13:30:57', '2021-03-16 13:32:14', NULL),
	(23, 1, '/tr/projeler', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 9, 'original', '2021-03-16 13:52:45', '2021-03-16 13:52:45', NULL),
	(24, 1, '/en/projects', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 10, 'original', '2021-03-16 13:52:45', '2021-03-16 13:52:45', NULL),
	(27, 1, '/tr/projeler/mediapress', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 17, 'original', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL),
	(28, 1, '/en/projects/mediapress', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 18, 'original', '2021-03-16 13:54:32', '2021-03-16 13:57:17', NULL),
	(29, 1, '/tr/projeler/folm-universe-blockchain-project', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 19, 'original', '2021-03-16 13:59:04', '2021-03-16 14:03:10', NULL),
	(30, 1, '/en/projects/folm-universe-blockchain-project', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 20, 'original', '2021-03-16 13:59:04', '2021-03-16 14:03:10', NULL),
	(31, 1, '/tr/projeler/egitmentv-start-up', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 21, 'original', '2021-03-16 14:03:37', '2021-03-16 14:05:30', NULL),
	(32, 1, '/en/projects/egitmentv-start-up', 'Mediapress\\Modules\\Content\\Models\\PageDetail', 22, 'original', '2021-03-16 14:03:37', '2021-03-16 14:05:30', NULL),
	(33, 1, '/tr/iletisim', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 11, 'original', '2021-03-16 14:27:49', '2021-03-16 14:27:49', NULL),
	(34, 1, '/en/contact', 'Mediapress\\Modules\\Content\\Models\\SitemapDetail', 12, 'original', '2021-03-16 14:27:49', '2021-03-16 14:27:49', NULL);
/*!40000 ALTER TABLE `urls` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.useraction_logs
CREATE TABLE IF NOT EXISTS `useraction_logs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `user_id` int unsigned NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `module_id` int DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_id` int DEFAULT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `context_type` enum('default','success','warning','danger','info') COLLATE utf8mb4_unicode_ci NOT NULL,
  `context_icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `priority` int DEFAULT NULL,
  `parent_id` int DEFAULT NULL,
  `scope_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope_2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scope_3` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.useraction_logs: ~59 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `useraction_logs`;
/*!40000 ALTER TABLE `useraction_logs` DISABLE KEYS */;
INSERT INTO `useraction_logs` (`id`, `website_id`, `user_id`, `ip`, `module_id`, `controller`, `action`, `model_id`, `model_type`, `context_type`, `context_icon`, `content`, `priority`, `parent_id`, `scope_1`, `scope_2`, `scope_3`, `created_at`, `updated_at`) VALUES
	(1, 1, 1, '127.0.0.1', 1, 'Mediapress\\Modules\\Auth\\Controllers\\PanelLoginController', 'login', 1, 'Mediapress\\Modules\\Auth\\Models\\Admin', 'info', 'fa-sign-in', '\'\' sitesinde \'Serkan Iskender\' adlı kullanıcı giriş yaptı.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:23:13', '2021-03-16 09:23:13'),
	(2, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\WebsiteController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Website', 'warning', 'fa-edit', '\'\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'websites\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:23:42', '2021-03-16 09:23:42'),
	(3, 1, 1, '127.0.0.1', 12, 'Mediapress\\Modules\\Setting\\Controllers\\SettingsController', 'logo', 1, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'1\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:23:55', '2021-03-16 09:23:55'),
	(4, 1, 1, '127.0.0.1', 12, 'Mediapress\\Modules\\Setting\\Controllers\\SettingsController', 'logo', 2, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'2\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:23:55', '2021-03-16 09:23:55'),
	(5, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 1, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'1\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:24:02', '2021-03-16 09:24:02'),
	(6, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\WebsiteController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Website', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'websites\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:24:27', '2021-03-16 09:24:27'),
	(7, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 2, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'2\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:24:32', '2021-03-16 09:24:32'),
	(8, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 2, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'2\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:24:48', '2021-03-16 09:24:48'),
	(9, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 1, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'1\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:26:07', '2021-03-16 09:26:07'),
	(10, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 2, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'2\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:26:22', '2021-03-16 09:26:22'),
	(11, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 3, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'3\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:26:33', '2021-03-16 09:26:33'),
	(12, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 4, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'4\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:26:47', '2021-03-16 09:26:47'),
	(13, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 5, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'5\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:27:01', '2021-03-16 09:27:01'),
	(14, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 3, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'3\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:29:28', '2021-03-16 09:29:28'),
	(15, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 5, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'5\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:35:52', '2021-03-16 09:35:52'),
	(16, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 5, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'5\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:36:10', '2021-03-16 09:36:10'),
	(17, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 4, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'4\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:39:56', '2021-03-16 09:39:56'),
	(18, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 6, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'6\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 09:46:29', '2021-03-16 09:46:29'),
	(19, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 6, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'6\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:08:29', '2021-03-16 10:08:29'),
	(20, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 6, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'6\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:08:59', '2021-03-16 10:08:59'),
	(21, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 7, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'7\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:09:22', '2021-03-16 10:09:22'),
	(22, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 5, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'5\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:24:56', '2021-03-16 10:24:56'),
	(23, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 6, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'6\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:24:57', '2021-03-16 10:24:57'),
	(24, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 7, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'7\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:25:00', '2021-03-16 10:25:00'),
	(25, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 8, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'8\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:35', '2021-03-16 10:36:35'),
	(26, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 9, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'9\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:36', '2021-03-16 10:36:36'),
	(27, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 10, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'10\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:36', '2021-03-16 10:36:36'),
	(28, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 11, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'11\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:37', '2021-03-16 10:36:37'),
	(29, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 12, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'12\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:38', '2021-03-16 10:36:38'),
	(30, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 13, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'13\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:38', '2021-03-16 10:36:38'),
	(31, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 14, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'14\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:39', '2021-03-16 10:36:39'),
	(32, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 15, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'15\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:40', '2021-03-16 10:36:40'),
	(33, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 16, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'16\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:40', '2021-03-16 10:36:40'),
	(34, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 17, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'17\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:41', '2021-03-16 10:36:41'),
	(35, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 18, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'18\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 10:36:41', '2021-03-16 10:36:41'),
	(36, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 7, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'7\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:21:24', '2021-03-16 12:21:24'),
	(37, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 7, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'7\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:22:07', '2021-03-16 12:22:07'),
	(38, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:23:58', '2021-03-16 12:23:58'),
	(39, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 19, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'19\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:26:59', '2021-03-16 12:26:59'),
	(40, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:27:13', '2021-03-16 12:27:13'),
	(41, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:28:23', '2021-03-16 12:28:23'),
	(42, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 20, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'20\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:30:08', '2021-03-16 12:30:08'),
	(43, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 2, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'2\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:30:20', '2021-03-16 12:30:20'),
	(44, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:31:45', '2021-03-16 12:31:45'),
	(45, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 1, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 12:44:19', '2021-03-16 12:44:19'),
	(46, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 21, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'21\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:22:03', '2021-03-16 13:22:03'),
	(47, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 3, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'3\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:22:18', '2021-03-16 13:22:18'),
	(48, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 22, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'22\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:27:11', '2021-03-16 13:27:11'),
	(49, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 4, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'4\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:27:21', '2021-03-16 13:27:21'),
	(50, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 23, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'23\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:28:35', '2021-03-16 13:28:35'),
	(51, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 5, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'5\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:28:43', '2021-03-16 13:28:43'),
	(52, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 24, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'24\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:30:08', '2021-03-16 13:30:08'),
	(53, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 6, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'6\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:30:17', '2021-03-16 13:30:17'),
	(54, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 25, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'25\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:32:06', '2021-03-16 13:32:06'),
	(55, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 7, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'7\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:32:14', '2021-03-16 13:32:14'),
	(56, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 8, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'8\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:34:01', '2021-03-16 13:34:01'),
	(57, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 9, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'9\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:34:27', '2021-03-16 13:34:27'),
	(58, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 10, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'10\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:34:50', '2021-03-16 13:34:50'),
	(59, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 11, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'11\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:35:10', '2021-03-16 13:35:10'),
	(60, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 12, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'12\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:35:28', '2021-03-16 13:35:28'),
	(61, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 13, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'13\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:35:46', '2021-03-16 13:35:46'),
	(62, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 14, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'14\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:36:12', '2021-03-16 13:36:12'),
	(63, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 8, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'8\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:50:59', '2021-03-16 13:50:59'),
	(64, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 9, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'9\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:52:22', '2021-03-16 13:52:22'),
	(65, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 9, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'9\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:52:45', '2021-03-16 13:52:45'),
	(66, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 26, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'26\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:57:10', '2021-03-16 13:57:10'),
	(67, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 9, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'9\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 13:57:17', '2021-03-16 13:57:17'),
	(68, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 27, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'27\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:03:03', '2021-03-16 14:03:03'),
	(69, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 10, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'10\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:03:10', '2021-03-16 14:03:10'),
	(70, 1, 1, '127.0.0.1', NULL, 'Mediapress\\FileManager\\Http\\Controllers\\UploadFileManagerController', 'uploadFromDropzone', 28, 'Mediapress\\FileManager\\Models\\MFile', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'mfiles\' tablosuna \'28\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:05:24', '2021-03-16 14:05:24'),
	(71, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\PageController', 'update', 11, 'Mediapress\\Modules\\Content\\Models\\Page', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'pages\' tablosunda, \'11\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:05:30', '2021-03-16 14:05:30'),
	(72, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 15, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'15\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:06:19', '2021-03-16 14:06:19'),
	(73, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 16, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'16\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:06:39', '2021-03-16 14:06:39'),
	(74, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsStore', 17, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'menu_details\' tablosuna \'17\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:06:57', '2021-03-16 14:06:57'),
	(75, 1, 1, '127.0.0.1', 7, 'Mediapress\\Modules\\Heraldist\\Controllers\\FormController', 'store', 1, 'Mediapress\\Modules\\Heraldist\\Models\\Form', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'forms\' tablosuna \'1\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:24:29', '2021-03-16 14:24:29'),
	(76, 1, 1, '127.0.0.1', 7, 'Mediapress\\Modules\\Heraldist\\Controllers\\FormController', 'detailsCreateOrUpdate', 1, 'Mediapress\\Modules\\Heraldist\\Models\\Form', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'forms\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:26:05', '2021-03-16 14:26:05'),
	(77, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'create', 10, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'success', 'fa-check', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı, \'sitemaps\' tablosuna \'10\' ID\'li içeriği ekledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:27:27', '2021-03-16 14:27:27'),
	(78, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\SitemapController', 'store', 10, 'Mediapress\\Modules\\Content\\Models\\Sitemap', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'sitemaps\' tablosunda, \'10\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:27:48', '2021-03-16 14:27:48'),
	(79, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 5, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'5\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 14:29:36', '2021-03-16 14:29:36'),
	(80, 1, 1, '127.0.0.1', 7, 'Mediapress\\Modules\\Heraldist\\Controllers\\FormController', 'detailsCreateOrUpdate', 1, 'Mediapress\\Modules\\Heraldist\\Models\\Form', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'forms\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 15:07:37', '2021-03-16 15:07:37'),
	(81, 1, 1, '127.0.0.1', 7, 'Mediapress\\Modules\\Heraldist\\Controllers\\FormController', 'detailsCreateOrUpdate', 1, 'Mediapress\\Modules\\Heraldist\\Models\\Form', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'forms\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 15:07:46', '2021-03-16 15:07:46'),
	(82, 1, 1, '127.0.0.1', 7, 'Mediapress\\Modules\\Heraldist\\Controllers\\FormController', 'detailsCreateOrUpdate', 1, 'Mediapress\\Modules\\Heraldist\\Models\\Form', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'forms\' tablosunda, \'1\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-16 15:08:08', '2021-03-16 15:08:08'),
	(83, 1, 1, '127.0.0.1', 1, 'Mediapress\\Modules\\Auth\\Controllers\\PanelLoginController', 'login', 1, 'Mediapress\\Modules\\Auth\\Models\\Admin', 'info', 'fa-sign-in', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı giriş yaptı.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 06:23:03', '2021-03-17 06:23:03'),
	(84, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 3, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'3\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:04:06', '2021-03-17 07:04:06'),
	(85, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 18, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'18\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:53:26', '2021-03-17 07:53:26'),
	(86, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 19, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'19\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:53:42', '2021-03-17 07:53:42'),
	(87, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 23, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'23\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:53:54', '2021-03-17 07:53:54'),
	(88, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 24, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'24\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:01', '2021-03-17 07:54:01'),
	(89, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 20, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'20\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:12', '2021-03-17 07:54:12'),
	(90, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 25, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'25\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:21', '2021-03-17 07:54:21'),
	(91, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 26, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'26\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:29', '2021-03-17 07:54:29'),
	(92, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 27, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'27\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:37', '2021-03-17 07:54:37'),
	(93, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 28, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'28\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:45', '2021-03-17 07:54:45'),
	(94, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 29, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'29\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:54:59', '2021-03-17 07:54:59'),
	(95, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 30, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'30\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:55:08', '2021-03-17 07:55:08'),
	(96, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 31, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'31\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:55:20', '2021-03-17 07:55:20'),
	(97, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 21, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'21\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:55:27', '2021-03-17 07:55:27'),
	(98, 1, 1, '127.0.0.1', 3, 'Mediapress\\Modules\\Content\\Controllers\\MenuController', 'detailsUpdate', 22, 'Mediapress\\Modules\\Content\\Models\\MenuDetail', 'warning', 'fa-edit', '\'genarge.test\' sitesinde \'Serkan Iskender\' adlı kullanıcı \'menu_details\' tablosunda, \'22\' ID\'li içeriği güncelledi.', NULL, NULL, NULL, NULL, NULL, '2021-03-17 07:55:40', '2021-03-17 07:55:40');
/*!40000 ALTER TABLE `useraction_logs` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.userlists
CREATE TABLE IF NOT EXISTS `userlists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.userlists: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `userlists`;
/*!40000 ALTER TABLE `userlists` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlists` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.userlist_pivot
CREATE TABLE IF NOT EXISTS `userlist_pivot` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `userlist_id` int NOT NULL,
  `userable_id` int NOT NULL,
  `userable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.userlist_pivot: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `userlist_pivot`;
/*!40000 ALTER TABLE `userlist_pivot` DISABLE KEYS */;
/*!40000 ALTER TABLE `userlist_pivot` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `website_id` int NOT NULL DEFAULT '1',
  `phone` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` json DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.users: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.user_activations
CREATE TABLE IF NOT EXISTS `user_activations` (
  `user_id` int unsigned NOT NULL,
  `activated` tinyint(1) NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.user_activations: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `user_activations`;
/*!40000 ALTER TABLE `user_activations` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_activations` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.user_extras
CREATE TABLE IF NOT EXISTS `user_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int unsigned NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.user_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `user_extras`;
/*!40000 ALTER TABLE `user_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.websites
CREATE TABLE IF NOT EXISTS `websites` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `type` enum('domain','folder','alias') COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` enum('internal','external') COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` int unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ssl` tinyint(1) NOT NULL DEFAULT '0',
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `website_id` int DEFAULT NULL,
  `use_deflng_code` tinyint NOT NULL DEFAULT '0',
  `regions` tinyint NOT NULL DEFAULT '0',
  `variation_template` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '{lng}/{cg}',
  `admin_id` int unsigned DEFAULT NULL,
  `status` int NOT NULL DEFAULT '3',
  `order` int unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.websites: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `websites`;
/*!40000 ALTER TABLE `websites` DISABLE KEYS */;
INSERT INTO `websites` (`id`, `type`, `target`, `sort`, `name`, `slug`, `ssl`, `default`, `website_id`, `use_deflng_code`, `regions`, `variation_template`, `admin_id`, `status`, `order`, `created_at`, `updated_at`, `deleted_at`) VALUES
	(1, 'domain', 'internal', 1, 'Gen Arge San ve Tic A.Ş.', 'genarge.test', 0, 1, NULL, 1, 1, '{cg}_{lng}', 0, 1, 1, NULL, '2021-03-16 09:24:27', NULL);
/*!40000 ALTER TABLE `websites` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.website_details
CREATE TABLE IF NOT EXISTS `website_details` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int unsigned NOT NULL,
  `language_id` int unsigned NOT NULL,
  `country_group_id` int unsigned DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_details_website_id_index` (`website_id`),
  KEY `website_details_language_id_index` (`language_id`),
  KEY `website_details_country_group_id_index` (`country_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.website_details: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `website_details`;
/*!40000 ALTER TABLE `website_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `website_details` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.website_detail_extras
CREATE TABLE IF NOT EXISTS `website_detail_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_detail_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `website_detail_extras_website_detail_id_index` (`website_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.website_detail_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `website_detail_extras`;
/*!40000 ALTER TABLE `website_detail_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `website_detail_extras` ENABLE KEYS */;

-- tablo yapısı dökülüyor genarge.website_extras
CREATE TABLE IF NOT EXISTS `website_extras` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `website_id` int NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `website_extras_website_id_index` (`website_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- genarge.website_extras: ~0 rows (yaklaşık) tablosu için veriler indiriliyor
DELETE FROM `website_extras`;
/*!40000 ALTER TABLE `website_extras` DISABLE KEYS */;
/*!40000 ALTER TABLE `website_extras` ENABLE KEYS */;

-- görünüm yapısı dökülüyor genarge.search_content
-- Geçici tablolar temizlenerek final VIEW oluşturuluyor
DROP TABLE IF EXISTS `search_content`;
CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `search_content` AS select `s`.`id` AS `model_id`,'Mediapress\\Modules\\Content\\Models\\Sitemap' AS `model_type`,`sd`.`language_id` AS `language_id`,`sd`.`name` AS `detail_name`,`sd`.`detail` AS `detail_detail`,`sd`.`search_text` AS `detail_search_text`,`su`.`id` AS `url_id`,`s`.`cvar_1` AS `cvar_1`,`s`.`cvar_2` AS `cvar_2`,`s`.`ctex_1` AS `ctex_1`,`s`.`ctex_2` AS `ctex_2`,`s`.`cint_1` AS `cint_1`,`s`.`cint_2` AS `cint_2`,`s`.`cdat_1` AS `cdat_1`,`s`.`cdat_2` AS `cdat_2`,`s`.`cdec_1` AS `cdec_1`,`s`.`cdec_2` AS `cdec_2`,`sde`.`key` AS `detail_key`,`sde`.`value` AS `detail_value`,`se`.`key` AS `key`,`se`.`value` AS `value` from ((((`sitemaps` `s` left join `sitemap_details` `sd` on((`s`.`id` = `sd`.`sitemap_id`))) left join `sitemap_detail_extras` `sde` on((`sd`.`id` = `sde`.`sitemap_detail_id`))) left join `sitemap_extras` `se` on((`s`.`id` = `se`.`sitemap_id`))) left join `urls` `su` on((`sd`.`id` = `su`.`model_id`))) where ((`su`.`type` = 'original') and (`su`.`model_type` = 'Mediapress\\Modules\\Content\\Models\\SitemapDetail') and (`s`.`searchable` = 1) and (`s`.`deleted_at` is null)) union select `p`.`id` AS `model_id`,'Mediapress\\Modules\\Content\\Models\\Page' AS `model_type`,`pd`.`language_id` AS `language_id`,`pd`.`name` AS `detail_name`,`pd`.`detail` AS `detail_detail`,`pd`.`search_text` AS `detail_search_text`,`pu`.`id` AS `url_id`,`p`.`cvar_1` AS `cvar_1`,`p`.`cvar_2` AS `cvar_2`,`p`.`ctex_1` AS `ctex_1`,`p`.`ctex_2` AS `ctex_2`,`p`.`cint_1` AS `cint_1`,`p`.`cint_2` AS `cint_2`,`p`.`cdat_1` AS `cdat_1`,`p`.`cdat_2` AS `cdat_2`,`p`.`cdec_1` AS `cdec_1`,`p`.`cdec_2` AS `cdec_2`,`pde`.`key` AS `detail_key`,`pde`.`value` AS `detail_value`,`pe`.`key` AS `key`,`pe`.`value` AS `value` from (((((`pages` `p` left join `page_details` `pd` on((`p`.`id` = `pd`.`page_id`))) left join `page_detail_extras` `pde` on((`pd`.`id` = `pde`.`page_detail_id`))) left join `page_extras` `pe` on((`p`.`id` = `pe`.`page_id`))) left join `urls` `pu` on((`pd`.`id` = `pu`.`model_id`))) left join `sitemaps` `ps` on((`ps`.`id` = `p`.`sitemap_id`))) where ((`p`.`status` = '1') and (`pu`.`type` = 'original') and (`pu`.`model_type` = 'Mediapress\\Modules\\Content\\Models\\PageDetail') and (`ps`.`searchable` = 1) and (`p`.`deleted_at` is null) and (`ps`.`deleted_at` is null)) union select `c`.`id` AS `model_id`,'Mediapress\\Modules\\Content\\Models\\Category' AS `model_type`,`cd`.`language_id` AS `language_id`,`cd`.`name` AS `detail_name`,`cd`.`detail` AS `detail_detail`,`cd`.`search_text` AS `detail_search_text`,`cu`.`id` AS `url_id`,`c`.`cvar_1` AS `cvar_1`,`c`.`cvar_2` AS `cvar_2`,`c`.`ctex_1` AS `ctex_1`,`c`.`ctex_2` AS `ctex_2`,`c`.`cint_1` AS `cint_1`,`c`.`cint_2` AS `cint_2`,`c`.`cdat_1` AS `cdat_1`,`c`.`cdat_2` AS `cdat_2`,`c`.`cdec_1` AS `cdec_1`,`c`.`cdec_2` AS `cdec_2`,`cde`.`key` AS `detail_key`,`cde`.`value` AS `detail_value`,`ce`.`key` AS `key`,`ce`.`value` AS `value` from ((((`categories` `c` left join `category_details` `cd` on((`c`.`id` = `cd`.`category_id`))) left join `category_detail_extras` `cde` on((`cd`.`id` = `cde`.`category_detail_id`))) left join `category_extras` `ce` on((`c`.`id` = `ce`.`category_id`))) left join `urls` `cu` on((`cd`.`id` = `cu`.`model_id`))) where ((`c`.`status` = '1') and (`cu`.`type` = 'original') and (`cu`.`model_type` = 'Mediapress\\Modules\\Content\\Models\\CategoryDetail') and (`c`.`deleted_at` is null));

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
